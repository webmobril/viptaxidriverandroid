package com.vip.taxivip_driver.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginPreference {

    // Sharedpref file name
    private static final String PREF_NAME = "LoginPreferences";

   private static SharedPreferences pref;

    // Editor for Shared preferences
   private static SharedPreferences.Editor editor;

    // Context
    Context _context;

    // All Shared Preferences Keys
    private static final String TAG_TOKEN = "tagtoken";

    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    private String isLoggedIn = "isLoggedIn";

    private String USER_REG_ID = "user_id";
    private String register_id = "id";
    private String OFFLINE_RESULT = "offline_result";
    private String FULL_NAME = "fullname";
    private String USER_EMAIL = "user_email";
    private String register_token = "register_token";
    private String OTP_REGISTER = "otp_register";
    private String RESEND_OTP = "Resend_otp";
    private String address = "address";
    private String officeAddress = "officeAddress";
    private String MOBILE = "mobile";
    private String latitude = "latitude";
    private String longitude = "longitude";
    private String USER_IMAGE = "userImage";
    private String UDOC_IMAGE = "docImage";
    private String UDOC_REG_IMAGE = "docRegImage";
    private String UDOC_VP_IMAGE = "docVPImage";
    private String UDOC_INS_IMAGE = "docInsImage";
    private String UDOC_CRIM_IMAGE = "docCriImage";
    private String UDOC_POLI_IMAGE = "docPolImage";
    private String _NAME = "_Name";
    private String FIRST_NAME = "firstName";
    private String LAST_NAME = "lastName";
    private String DESCRIBE_YOURSELF = "describe_yourself";
    private String LANGUAGE_KNOWN = "language_known";
    private String WHERE_FROM = "where_From";
    private String cityName = "cityName";
    private String VEHICLE_TYPE = "vehicleType";
    private String BOOKING_TYPE = "BookingType";
    private String cityId = "cityId";
    private String VERIFY_DOC = "Verify_doc";
    private String currentKm = "currentKm";
    private String BOOKING_ID = "Booking_Id";




    public LoginPreference(Context context) {
        if (pref == null) {
            pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            // editor = pref.edit();
        }
    }


    public  boolean getIsLoggedIn() {
        return pref.getBoolean(this.isLoggedIn, false);
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        editor = pref.edit();
        editor.putBoolean(this.isLoggedIn, isLoggedIn);
        editor.apply();
    }


    public void setBookingId(int BookingId) {
        editor = pref.edit();
        editor.putInt(this.BOOKING_ID, BookingId);
        editor.apply();
    }

    public int getBookingId() {
        return pref.getInt(this.BOOKING_ID, 0);
    }



    public String getFullname() {
        return pref.getString(this.FULL_NAME, "");
    }

    public void setFullname(String fullname) {
        editor = pref.edit();
        editor.putString(this.FULL_NAME, fullname);
        editor.apply();
    }

    public String getUser_email() {
        return pref.getString(this.USER_EMAIL, "");
    }

    public void setUser_email(String user_email) {
        editor = pref.edit();
        editor.putString(this.USER_EMAIL, user_email);
        editor.apply();
    }



    public void setRegisterToken(String register_token) {
        editor = pref.edit();
        editor.putString(this.register_token, register_token);
        editor.apply();
    }

    public String getRegisterToken() {
        String registerKey = pref.getString(this.register_token, "");
        return registerKey;
    }



    public void setRegister_id(int register_id) {
        editor = pref.edit();
        editor.putInt(this.register_id, register_id);
        editor.apply();
    }

    public int getRegister_id() {
        int register_id = pref.getInt(this.register_id, 1);
        return register_id;
    }


    public int getUSER_REG_ID() {
        int USER_REG_ID = pref.getInt(this.USER_REG_ID,1);
        return USER_REG_ID;
    }

    public void setUSER_REG_ID(int USER_REG_ID) {
        editor = pref.edit();
        editor.putInt(this.USER_REG_ID, USER_REG_ID);
        editor.apply();
    }


    public void setVerifyDoc(int verifyDoc) {
        editor = pref.edit();
        editor.putInt(this.VERIFY_DOC, verifyDoc);
        editor.apply();
    }

    public int getVerifyDoc() {
        int verifyDoc = pref.getInt(this.VERIFY_DOC, 1);
        return verifyDoc;
    }



    public void setOfflineResult_id(int offlineResult) {
        editor = pref.edit();
        editor.putInt(this.OFFLINE_RESULT, offlineResult);
        editor.apply();
    }

    public int getOfflineResult() {
        int register_ids = pref.getInt(this.OFFLINE_RESULT, 1);
        return register_ids;
    }

    public String getOtpRegister() {
        return pref.getString(this.OTP_REGISTER, "");
    }

    public void setOtpRegister(String otpRegister) {
        editor = pref.edit();
        editor.putString(this.OTP_REGISTER, otpRegister);
        editor.apply();
    }

    public String getResend_otp() {
        return pref.getString(this.RESEND_OTP, "");
    }

    public void setResend_otp(String Resend_otp) {
        editor = pref.edit();
        editor.putString(this.RESEND_OTP, Resend_otp);
        editor.apply();
    }


    public void setMobile(String mobile) {
        editor = pref.edit();
        editor.putString(this.MOBILE, mobile);
        editor.apply();
    }



    public String getMobile() {
        return pref.getString(this.MOBILE, "");
    }



    public void setFirstName(String firstName) {
        editor = pref.edit();
        editor.putString(this.FIRST_NAME, firstName);
        editor.apply();
    }
    public String getFirstName() {
        return pref.getString(this.FIRST_NAME, "");
    }



    public void setVehicleType(String vehicleType) {
        editor = pref.edit();
        editor.putString(this.VEHICLE_TYPE, vehicleType);
        editor.apply();
    }
    public String getVehicleType() {
        return pref.getString(this.VEHICLE_TYPE, "");
    }



    public void setName(String name) {
        editor = pref.edit();
        editor.putString(this._NAME, name);
        editor.apply();
    }

    public String getName() {
        return pref.getString(this._NAME, "");
    }


    public void setLastName(String lastName) {
        editor = pref.edit();
        editor.putString(this.LAST_NAME, lastName);
        editor.apply();
    }
    public String getLastName() {
        return pref.getString(this.LAST_NAME, "");
    }



    public void setDescribeYourself(String describeYourself) {
        editor = pref.edit();
        editor.putString(this.DESCRIBE_YOURSELF, describeYourself);
        editor.apply();
    }
    public String getDescribeYourself() {
        return pref.getString(this.DESCRIBE_YOURSELF, "");
    }

    public void setLanguageKnown(String languageKnown) {
        editor = pref.edit();
        editor.putString(this.LANGUAGE_KNOWN, languageKnown);
        editor.apply();
    }
    public String getLanguageKnown() {
        return pref.getString(this.LANGUAGE_KNOWN, "");
    }



    public void setWhereFrom(String whereFrom) {
        editor = pref.edit();
        editor.putString(this.WHERE_FROM, whereFrom);
        editor.apply();
    }
    public String getWhereFrom() {
        return pref.getString(this.WHERE_FROM, "");
    }



    public void setCityName(String whereFrom) {
        editor = pref.edit();
        editor.putString(this.cityName, cityName);
        editor.apply();
    }
    public String getCityName() {
        return pref.getString(this.cityName, "");
    }


    public void setUserImage(String userImage) {
        editor = pref.edit();
        editor.putString(this.USER_IMAGE, userImage);
        editor.apply();
    }

    public String getUserImage() {
        return pref.getString(this.USER_IMAGE, "");
    }


    public void setDocumentImage(String userImage) {
        editor = pref.edit();
        editor.putString(this.UDOC_IMAGE, userImage);
        editor.commit();
    }

    public String getDocumentImage() {
        return pref.getString(this.UDOC_IMAGE, "");
    }

    public void setDocumentRegImage(String userImage) {
        editor = pref.edit();
        editor.putString(this.UDOC_REG_IMAGE, userImage);
        editor.apply();
    }

    public String getDocumentRegImage() {
        return pref.getString(this.UDOC_REG_IMAGE, "");
    }


    public void setDocumentVpImage(String userImage) {
        editor = pref.edit();
        editor.putString(this.UDOC_VP_IMAGE, userImage);
        editor.apply();
    }

    public String getDocumentVpImage() {
        return pref.getString(this.UDOC_VP_IMAGE, "");
    }

    public void setDocumentInsImage(String userImage) {
        editor = pref.edit();
        editor.putString(this.UDOC_INS_IMAGE, userImage);
        editor.apply();
    }

    public String getDocumentInsImage() {
        return pref.getString(this.UDOC_INS_IMAGE, "");
    }


    public void setDocumentCrimImage(String userImage) {
        editor = pref.edit();
        editor.putString(this.UDOC_CRIM_IMAGE, userImage);
        editor.apply();
    }

    public String getDocumentCrimImage() {
        return pref.getString(this.UDOC_CRIM_IMAGE, "");
    }


    public void setDocumentPolicImage(String userImage) {
        editor = pref.edit();
        editor.putString(this.UDOC_POLI_IMAGE, userImage);
        editor.apply();
    }

    public String getDocumentPolicImage() {
        return pref.getString(this.UDOC_POLI_IMAGE, "");
    }




    public void saveDeviceToken(String token){
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(TAG_TOKEN, token);
            editor.apply();
        }catch (Exception e){e.printStackTrace();}

        // return true;
    }

    //this method will fetch the device token from shared preferences
    public String getDeviceToken(){
        return  pref.getString(TAG_TOKEN, "a");
    }



    public void setLatitude(String latitude) {
        editor = pref.edit();
        editor.putString(this.latitude, latitude);
        editor.apply();
    }

    public String getLatitude() {
        return pref.getString(this.latitude, "");
    }


    public void setLongitude(String longitude) {
        editor = pref.edit();
        editor.putString(this.longitude, longitude);
        editor.apply();
    }

    public String getLongitude() {
        return pref.getString(this.longitude, "");
    }


    public float getCurrentKm() {
        return pref.getFloat(this.currentKm, 0f);
    }

    public void setCurrentKm(float currentKm) {
        editor = pref.edit();
        editor.putFloat(this.currentKm, currentKm);
        editor.apply();
    }

    public void setBookingType(String role) {
        editor = pref.edit();
        editor.putString(this.BOOKING_TYPE, role);
        editor.apply();
    }

    public String getBookingType() {
        return pref.getString(this.BOOKING_TYPE, "");
    }



    public  void clearSharedPreferences(Context context)
    {
        editor = pref.edit();
        editor.clear().apply();

    }


}
