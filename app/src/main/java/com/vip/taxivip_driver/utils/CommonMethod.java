package com.vip.taxivip_driver.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.fragments.home_view.HomeFragment;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommonMethod {


    public  static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showAlert(String message, Activity context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> {

                });
        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void exitPopup(Context mContext){
        new AlertDialog.Builder(mContext)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, (arg0, arg1) -> {
                    Intent launchNextActivity = new Intent(Intent.ACTION_MAIN);
                    launchNextActivity.addCategory(Intent.CATEGORY_HOME);
                    launchNextActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(launchNextActivity);
                }).create().show();
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static void hideKeyboard(Activity context) {

        if (context != null) {
            View view = context.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }


    public static void loadImageResized(Context mContext, String image, ImageView imageView) {
        Glide.with(mContext)
                .load(image)
                .placeholder(R.drawable.user_img)
                .error(R.drawable.user_img)
                .skipMemoryCache(true) //2
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);
    }

    public static void loadImageCarType(Context mContext, String image, ImageView imageView) {
        Glide.with(mContext)
                .load(image)
                .placeholder(R.drawable.car_type)
                .error(R.drawable.car_type)
                .skipMemoryCache(true) //2
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);
    }



    // callActivity with clear all activity instance
    public static void callActivity(Context mContext, Class cls){
        Intent intent = new Intent(mContext, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
    }


    //callOnly Activity
    public static void callOnlyActivity(Context mContext, Class cls){
        Intent intent = new Intent(mContext, cls);
        mContext.startActivity(intent);
    }

    public static double roundTwoDecimals(double doublePrice) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(doublePrice));
    }

    // set RecyclerView
    public static void setRecyclerView(Context context, RecyclerView recyclerView) {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);
    }


    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }



    /**
     * Transact fragment
     */
    public static void setFragment(Fragment fragment, boolean removeStack, FragmentActivity activity, int mContainer) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ftTransaction = fragmentManager.beginTransaction();
        if (removeStack) {
            int size = fragmentManager.getBackStackEntryCount();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            ftTransaction.replace(mContainer, fragment);
        } else {
            ftTransaction.replace(mContainer, fragment);
            ftTransaction.addToBackStack(null);
        }
        ftTransaction.replace(mContainer, fragment).commit();
    }

    public static void setSimpleFragment(Fragment fragment,FragmentActivity activity,int fromContainer){
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(fromContainer, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    public static void showToast(Context context, String Messsage) {
        Toast.makeText(context, Messsage, Toast.LENGTH_SHORT).show();
    }


    public static boolean isMyServiceRunning(Context context,Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        assert manager != null;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }





    public static void getLocationDistance(String from, com.google.maps.model.LatLng origin, com.google.maps.model.LatLng destination) {
        try {
            // - Perform the actual request
            DirectionsResult directionsResult = null;
            // - We need a context to access the API
            GeoApiContext geoApiContext = new GeoApiContext.Builder()
                    .apiKey("AIzaSyBYeaKLivuTjFXawkDIKVDYQZhz3zSizJk")
                    .build();

            directionsResult = DirectionsApi.newRequest(geoApiContext)
                    .mode(TravelMode.DRIVING)
                    .origin(origin)
                    .destination(destination)
                    .await();

            if (directionsResult != null) {
                DirectionsRoute route = directionsResult.routes[0];
                if (route != null) {
                    DirectionsLeg leg = route.legs[0];

                    double dist = leg.distance.inMeters;
                    double distinkm = dist / 1000;

                    AppData.getInstance().setDistance(String.valueOf(distinkm));
                    AppData.getInstance().setTime(leg.duration.humanReadable);

                    Log.e("Time--", AppData.getInstance().getTime());

                   /* if (from.equals("Home")) {
                        HomeFragment.homeFragment.setTimeDistance();
                    } else {
                        UserLocationFragment.userLocationFragment.setDistance(String.valueOf(distinkm));
                    }*/
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCurrentTime(){
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String currentTime = sdf.format(new Date());
        return currentTime;
    }


    public static ProgressDialog showProgressDialog(Context context, String message){
        ProgressDialog m_Dialog = new ProgressDialog(context);
        m_Dialog.setMessage(message);
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(false);
        m_Dialog.show();
        return m_Dialog;
    }

}
