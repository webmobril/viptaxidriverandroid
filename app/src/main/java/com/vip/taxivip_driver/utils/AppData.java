package com.vip.taxivip_driver.utils;

import com.google.android.gms.maps.model.LatLng;
import com.vip.taxivip_driver.models.EndTripResponse;
import com.vip.taxivip_driver.models.NotificationResponse;
import com.vip.taxivip_driver.models.SignInDataResponse;

public class AppData {
    public static final AppData ourInstance = new AppData();
    private SignInDataResponse.Result loginResonse;
    private boolean isfromNotification;
    private NotificationResponse.Result notificationData;
    private String distance = "1";
    private boolean fromOutSide;
    private NotificationResponse cancelNotification;
    private String time;
    private String FROM;
    private EndTripResponse.Result endTripResponse;
    private boolean isOngoing;
    private LatLng latLng;

    public static AppData getInstance() {
        return ourInstance;
    }

    public AppData() {
    }

    public void setLoginResonse(SignInDataResponse.Result loginResonse) {
        this.loginResonse = loginResonse;
    }

    public SignInDataResponse.Result getLoginResonse() {
        return loginResonse;
    }

    public void setIsfromNotification(boolean isfromNotification) {
        this.isfromNotification = isfromNotification;
    }

    public boolean getIsfromNotification() {
        return isfromNotification;
    }

    public void setNotificationData(NotificationResponse.Result notificationData) {
        this.notificationData = notificationData;
    }

    public NotificationResponse.Result getNotificationData() {
        return notificationData;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDistance() {
        return distance;
    }




    public void setFromOutSide(boolean fromOutSide) {
        this.fromOutSide = fromOutSide;
    }

    public boolean getFromOutSide() {
        return fromOutSide;
    }

    public void setCancelNotification(NotificationResponse cancelNotification) {
        this.cancelNotification = cancelNotification;
    }

    public NotificationResponse getCancelNotification() {
        return cancelNotification;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setFROM(String from) {
        this.FROM = from;
    }

    public String getFROM() {
        return FROM;
    }

    public void setEndTripResponse(EndTripResponse.Result endTripResponse) {
        this.endTripResponse = endTripResponse;
    }

    public EndTripResponse.Result getEndTripResponse() {
        return endTripResponse;
    }

    public void setIsOngoing(boolean isOngoing) {
        this.isOngoing = isOngoing;
    }

    public boolean getIsOngoing() {
        return isOngoing;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public LatLng getLatLng() {
        return latLng;
    }
}
