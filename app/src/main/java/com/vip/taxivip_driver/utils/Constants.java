package com.vip.taxivip_driver.utils;

public class Constants {

    public static final String BASE_URL = "https://www.webmobril.org/dev/taxivip/api/";
    public static final String IMAGE_URL = "https://www.webmobril.org/dev/taxivip/";
    public static final String HTML_URL = "https://www.webmobril.org";
    public static final String WEB_URL = "http://webmobril.org/dev/taxivip/blog_detail/";

    public final static String PUBNUB_PUBLISH_KEY = "pub-c-be4a9e47-ba04-4c30-9c17-35ed33bc686a";
    public final static String PUBNUB_SUBSCRIBE_KEY = "sub-c-6ffebde2-bb5e-11e9-a581-ba8cef5fd74e";
    public final static String PUBNUB_CHANNEL_NAME = "VIPTaxiDriver";
    public static final long NOTIFICATION_CANCEL_TIME = 15000;

    public static final String TAG_LOCATION = "MyLocationService";
    public static final String LOCATION_RECEIVER = "LOCATION RECEIVER";
    public static final int CAMERA_REQUEST = 0;
    public static final int GALLERY_IMAGE = 1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 5;
    public static final int MY_PERMISSIONS_REQUEST_LOC = 30;
    public static final String TAG_SIGNIN = "SignInActivity";
    public static final String TYPE = "TYPE";
    public static final String TYPE_VALUE = "TYPE_VALUE";
    public static final String MOBILE_NO = "Mobile No";
    public static final String COUNTRY_CODE = "country_code";
    public static final String OTP = "OTP";
    public static final String SIGNIN = "SIGNIN";
    public static final String REGISTER = "REGISTER";
    public static final String SIGNUP = "SIGNUP";
    public static final String DOCUMENT = "DOCUMENT";
    public static final String HELP = "HELP";
    public static final String ACCOUNT_STATUS = "ACCOUNT_STATUS";
    public static final String CREATE_ACCOUNT = "CREATE_ACCOUNT";
    public static final String FORGOT_PASSWORDS = "FORGOT_PASSWORDS";
    public static final String CARDS = "CARDS";
    public static final String DRIVER_DOCUMENTS_IMG = "driver_documents_img";
    public static final String HOME_SCREEN = "home_screen";
    public static final String VEHICLE_MODEL = "vehicle_model";
    public static final String VEHICLE_BRAND = "vehicle_brand";
    public static final String VEHICLE_YEAR = "vehicle_year";
    public static final String VEHICLE_COLOR = "vehicle_color";
    public static final String HISTORY = "History";

    public static final String LOCATION_STATUS_FILTER = "com.vip.taxivip_driver";
    public static final String CANCEl_BOOKING = "Booking Cancelled";
    public static final String NEW_BOOKING = "New Booking Request";
    public static final String DESTINATION_CHANGED = "Destination Changed";

    public static final String BOOKING_ACCEPT = "BOOKING_ACCEPT";
    public static final String REACHED_LOCATION = "REACHED_LOCATION";
    public static final String START_TRIP = "START_TRIP";



}
