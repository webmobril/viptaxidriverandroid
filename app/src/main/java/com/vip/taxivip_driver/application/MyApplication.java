package com.vip.taxivip_driver.application;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.vip.taxivip_driver.services.MyLocationService;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.FontsOverride;


public class MyApplication extends Application implements Application.ActivityLifecycleCallbacks{

    private static MyApplication singleton;
    public static MyApplication application;

    private int activityReferences = 0;
    private boolean isActivityChangingConfigurations = false;


    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        application = MyApplication.this;

        registerActivityLifecycleCallbacks(this);

        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/Nunito Light.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/Nunito Light.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/Nunito Light.ttf");


    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, Bundle savedInstanceState) {

        if (!CommonMethod.isMyServiceRunning(getApplicationContext(), MyLocationService.class)) {
            startService(new Intent(getApplicationContext(), MyLocationService.class));
        }
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        try {
            if (++activityReferences == 1 && !isActivityChangingConfigurations) {

                if (!CommonMethod.isMyServiceRunning(getApplicationContext(), MyLocationService.class)) {
                    startService(new Intent(getApplicationContext(), MyLocationService.class));
                }
                Log.e("MyApplication--", "App Enters Foreground");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        startService(new Intent(getApplicationContext(), MyLocationService.class));

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {


    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }
    public static MyApplication getContext() {
        return application;
    }

    public static MyApplication getInstance() {

            if (singleton== null) {
                synchronized(MyApplication.class) {
                    if (singleton == null)
                        singleton = new MyApplication();
                }
            }
            // Return the instance
            return singleton;

    }
}
