package com.vip.taxivip_driver.requestclass;

public class AcceptBookingRequest {
    private int driver_id;

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    private int booking_id;
}
