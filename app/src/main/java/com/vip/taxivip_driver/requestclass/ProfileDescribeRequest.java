package com.vip.taxivip_driver.requestclass;

public class ProfileDescribeRequest {

    private int driver_id;
    private String describe_yourself;
    private String homeaddress;

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }

    public String getDescribe_yourself() {
        return describe_yourself;
    }

    public void setDescribe_yourself(String describe_yourself) {
        this.describe_yourself = describe_yourself;
    }

    public String getHomeaddress() {
        return homeaddress;
    }

    public void setHomeaddress(String homeaddress) {
        this.homeaddress = homeaddress;
    }


}
