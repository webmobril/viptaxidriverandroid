package com.vip.taxivip_driver.requestclass;

public class VehicleUploadRequest {

    private int user_id;
    private String car_name;
    private String model_number;
    private String car_year;
    private String car_color;
    private int viehcle_type_id;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public String getModel_number() {
        return model_number;
    }

    public void setModel_number(String model_number) {
        this.model_number = model_number;
    }

    public String getCar_year() {
        return car_year;
    }

    public void setCar_year(String car_year) {
        this.car_year = car_year;
    }

    public String getCar_color() {
        return car_color;
    }

    public void setCar_color(String car_color) {
        this.car_color = car_color;
    }

    public int getViehcle_type_id() {
        return viehcle_type_id;
    }

    public void setViehcle_type_id(int viehcle_type_id) {
        this.viehcle_type_id = viehcle_type_id;
    }


}
