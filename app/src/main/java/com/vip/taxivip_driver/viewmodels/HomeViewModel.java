package com.vip.taxivip_driver.viewmodels;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.vip.taxivip_driver.models.UpdateProfileResponse;
import com.vip.taxivip_driver.reposotory.HomeRespsotory;
import com.vip.taxivip_driver.requestclass.SetDestinationRequest;

public class HomeViewModel extends ViewModel {

    private static HomeRespsotory homeRespsotory;

    public HomeViewModel() {
        homeRespsotory =  HomeRespsotory.getInstance();
    }



    public MutableLiveData<UpdateProfileResponse> postDestinationViewModel( SetDestinationRequest setDestinationRequest) {
        return homeRespsotory.postSetDetinationApi(setDestinationRequest);
    }


}
