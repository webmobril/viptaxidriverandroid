package com.vip.taxivip_driver.viewmodels;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.vip.taxivip_driver.models.DocumentResponse;
import com.vip.taxivip_driver.models.LanguageResponse;
import com.vip.taxivip_driver.models.UpdateProfileResponse;
import com.vip.taxivip_driver.models.VehicleTypeResponse;
import com.vip.taxivip_driver.reposotory.ProfileReposotory;
import com.vip.taxivip_driver.requestclass.ProfileDescribeRequest;
import com.vip.taxivip_driver.requestclass.VehicleUploadRequest;

public class ProfileViewModel  extends ViewModel {
    private ProfileReposotory profileReposotory;

    public ProfileViewModel() {
        profileReposotory =  ProfileReposotory.getInstance();
    }


    public MutableLiveData<DocumentResponse> postVehicleModelMutable(VehicleUploadRequest vehicleUploadRequest){

       return profileReposotory.postVehicleModelUpdate(vehicleUploadRequest);
    }


    public MutableLiveData<UpdateProfileResponse> postUpdateProfileDescribeViewModel(ProfileDescribeRequest profileDescribeRequest){

        return profileReposotory.postUpdateProfileDescribe(profileDescribeRequest);
    }


    public MutableLiveData<UpdateProfileResponse> postUpdateAddLanguageViewModel(int driverId,  String language_known){

        return profileReposotory.postUpdateProfileLanguage(driverId,language_known);
    }


    public MutableLiveData<LanguageResponse> getLanguageViewModel(){

        return profileReposotory.getLanguageResponse();
    }


     public MutableLiveData<VehicleTypeResponse> getVehicleTypeViewModel(){

        return profileReposotory.getVehicleTypeReposotory();
     }
}
