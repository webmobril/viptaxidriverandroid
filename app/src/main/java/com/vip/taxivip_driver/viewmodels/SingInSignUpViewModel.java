package com.vip.taxivip_driver.viewmodels;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vip.taxivip_driver.models.CityMessageResponse;
import com.vip.taxivip_driver.models.CountryResult;
import com.vip.taxivip_driver.models.OtpVerifiedResponse;
import com.vip.taxivip_driver.models.ProfileImageResponse;
import com.vip.taxivip_driver.models.ResendOTPResponse;
import com.vip.taxivip_driver.models.SignInDataResponse;
import com.vip.taxivip_driver.models.SignupResponse;
import com.vip.taxivip_driver.models.StateMessgeResponse;
import com.vip.taxivip_driver.models.UpdateProfileResponse;
import com.vip.taxivip_driver.reposotory.SignInSignUpReposotory;
import com.vip.taxivip_driver.requestclass.OtpVerifyRequest;
import com.vip.taxivip_driver.requestclass.SingInRequest;
import com.vip.taxivip_driver.requestclass.SingupRequest;
import com.vip.taxivip_driver.requestclass.UpdateProfileRequest;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class SingInSignUpViewModel extends ViewModel {
    private SignInSignUpReposotory signupReposotry;
    private MutableLiveData<SignInDataResponse> signInDataResponseMutableLiveData;

    public SingInSignUpViewModel() {
        signupReposotry =  SignInSignUpReposotory.getInstance();

    }

    public MutableLiveData<SignInDataResponse> getSignIn(SingInRequest singInRequest) {

        return signupReposotry.getSignInMutableLiveData(singInRequest);

    }



    public MutableLiveData<SignupResponse> getSignupResponse(SingupRequest singupRequest) {
        return signupReposotry.getMutableLiveData(singupRequest);

    }


    public MutableLiveData<List<StateMessgeResponse>> getStateResponse(){
        return signupReposotry.getMutableListState();
    }


    public MutableLiveData<List<CityMessageResponse>> getCityResponse(int stateId){
        return signupReposotry.getMutableListCity(stateId);
    }


    public MutableLiveData<List<CountryResult>> getCountryResponse(){
        return signupReposotry.getMutableListCountry();
    }


    public MutableLiveData<UpdateProfileResponse> postUpdateProfileViewModel(UpdateProfileRequest updateProfileRequest){

        return signupReposotry.postUpdateProfile(updateProfileRequest);
    }


    public MutableLiveData<UpdateProfileResponse> postGetProfileViewModel(int driverI){

        return signupReposotry.postgetProfile(driverI);
    }

    public MutableLiveData<ProfileImageResponse> postProfileImageViewModel(RequestBody userId , MultipartBody.Part image_body){

        return signupReposotry.postUpdateProfileImage(userId ,image_body);
    }



    public LiveData<OtpVerifiedResponse> getOTP(OtpVerifyRequest otpVerifyRequest) {

        return signupReposotry.getOtpVerifyMutableLiveData(otpVerifyRequest);
    }

    public LiveData<ResendOTPResponse> getResendOTP(int user_id) {

        return signupReposotry.getResendOTP(user_id);
    }

}
