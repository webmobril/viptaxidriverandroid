package com.vip.taxivip_driver.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vip.taxivip_driver.models.ForgetPasswordResponse;
import com.vip.taxivip_driver.models.ResetPasswordResponse;
import com.vip.taxivip_driver.reposotory.ForgetPasswordRespository;
import com.vip.taxivip_driver.requestclass.ForgetPasswordRequest;
import com.vip.taxivip_driver.requestclass.ResetPasswordRequest;

public class ForgetResetPasswordViewModel extends ViewModel {

    private ForgetPasswordRespository forgetPasswordRespository;

    public ForgetResetPasswordViewModel() {
        forgetPasswordRespository = ForgetPasswordRespository.getInstance();
    }

    public MutableLiveData<ForgetPasswordResponse> getForgetResponse(ForgetPasswordRequest forgetPasswordRequest) {
        return forgetPasswordRespository.getMutableLiveData(forgetPasswordRequest);
    }

    public MutableLiveData<ResetPasswordResponse> getResetPasswordResponse(ResetPasswordRequest resetPasswordRequest) {
        return forgetPasswordRespository.getMutableLiveDataResetPass(resetPasswordRequest);

    }
}
