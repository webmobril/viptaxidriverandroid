package com.vip.taxivip_driver.viewmodels;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.vip.taxivip_driver.models.AboutUsResponse;
import com.vip.taxivip_driver.models.ContactUsResponse;
import com.vip.taxivip_driver.models.GetHelpResponse;
import com.vip.taxivip_driver.models.LogoutResponse;
import com.vip.taxivip_driver.models.TermsConditionResponse;
import com.vip.taxivip_driver.reposotory.AccountReposotory;
import com.vip.taxivip_driver.requestclass.ContactAdminRequest;

public class AccountViewModel extends ViewModel {

   private AccountReposotory AccountViewModel;

    public AccountViewModel() {

        AccountViewModel =  AccountReposotory.getInstance();
    }
    public MutableLiveData<LogoutResponse> putLogOutResponse(int userID) {
        return AccountViewModel.postLogoutResponse(userID);
    }

    public MutableLiveData<ContactUsResponse> getContactUsViewModel() {
        return AccountViewModel.getContactUs();
    }

    public MutableLiveData<GetHelpResponse> getHelpResponseMutableLiveData(ContactAdminRequest contactAdminRequest) {
        return AccountViewModel.getHelpResponseMutableLive(contactAdminRequest);
    }

    public MutableLiveData<AboutUsResponse> getAboutUSViewModel(int language) {

        return AccountViewModel.getAboutUsApi(language);
    }

    public MutableLiveData<TermsConditionResponse> getTermsConditionViewModel(int language) {
        return AccountViewModel.getTermsConditionReposotory(language);

    }
    public MutableLiveData<TermsConditionResponse> getPrivacyPolicyViewModel(int language) {

        return AccountViewModel.getPrivacyPolicyReposotory(language);
    }

}
