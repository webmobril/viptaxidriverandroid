package com.vip.taxivip_driver.viewmodels;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.vip.taxivip_driver.models.AcceptBookingResponse;
import com.vip.taxivip_driver.models.BookingDetailsResponse;
import com.vip.taxivip_driver.models.EndTripResponse;
import com.vip.taxivip_driver.models.OfflineResponse;
import com.vip.taxivip_driver.models.ReachLocationResponse;
import com.vip.taxivip_driver.models.SendReviewResponse;
import com.vip.taxivip_driver.models.TripHistoryResponse;
import com.vip.taxivip_driver.reposotory.BookingReposotory;
import com.vip.taxivip_driver.requestclass.AcceptBookingRequest;
import com.vip.taxivip_driver.requestclass.EndTripRequest;
import com.vip.taxivip_driver.requestclass.ReviewRequest;
import com.vip.taxivip_driver.requestclass.StartTripRequest;

public class BookingViewModels extends ViewModel {
    private  BookingReposotory bookingReposotory;

    public BookingViewModels() {
        bookingReposotory = BookingReposotory.getInstance();
    }

    // online offline ViewModel
    public MutableLiveData<OfflineResponse> postOfflineResponse(int driverId) {
        return bookingReposotory.postOfflineDriverApi(driverId);
    }

    // Accept Request ViewModel
    public MutableLiveData<AcceptBookingResponse> postAcceptRequestViewModel(AcceptBookingRequest acceptBookingRequest) {
        return bookingReposotory.acceptBookingRequestReposotory(acceptBookingRequest);
    }
     // Reach Location Request ViewModel
    public MutableLiveData<ReachLocationResponse> postReactLocationViewModel(AcceptBookingRequest acceptBookingRequest){

        return bookingReposotory.reachLocationReposotory(acceptBookingRequest);
    }

    // Start Trip Request ViewModel

    public MutableLiveData<AcceptBookingResponse> postStartTripRequestViewModel(StartTripRequest startTripRequest) {
        return bookingReposotory.startTripReposotory(startTripRequest);
    }

    // End Trip Request viewModel
    public MutableLiveData<EndTripResponse> postEndTripViewModel(EndTripRequest endTripRequest) {

        return bookingReposotory.endTripResponseReposotory(endTripRequest);
    }

    // Booking History ViewModel
    public MutableLiveData<TripHistoryResponse> getTripHistoryViewModel(int driverId) {

        return bookingReposotory.postTripHistoryReposotory(driverId);
    }

    // Send Reviews ViewModel
    public MutableLiveData<SendReviewResponse> postReviewViewModel(ReviewRequest reviewRequest) {
        return bookingReposotory.sendReviewResponseReposotory(reviewRequest);
    }

     // Get Booking Details
    public MutableLiveData<BookingDetailsResponse> getBookingDetailsViewModel(int bookingId){
        return bookingReposotory.getBookingDetailsReposotory(bookingId);
    }
}
