package com.vip.taxivip_driver.network;


import java.io.IOException;

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage(){

        return "No Internet Connection";
    }
}
