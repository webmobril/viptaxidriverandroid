package com.vip.taxivip_driver.network;

import com.vip.taxivip_driver.models.AboutUsResponse;
import com.vip.taxivip_driver.models.AcceptBookingResponse;
import com.vip.taxivip_driver.models.BookingDetailsResponse;
import com.vip.taxivip_driver.models.CityResponse;
import com.vip.taxivip_driver.models.ContactUsResponse;
import com.vip.taxivip_driver.models.CountryResponse;
import com.vip.taxivip_driver.models.DocumentResponse;
import com.vip.taxivip_driver.models.EndTripResponse;
import com.vip.taxivip_driver.models.ForgetPasswordResponse;
import com.vip.taxivip_driver.models.GetHelpResponse;
import com.vip.taxivip_driver.models.LanguageResponse;
import com.vip.taxivip_driver.models.LogoutResponse;
import com.vip.taxivip_driver.models.OfflineResponse;
import com.vip.taxivip_driver.models.OtpVerifiedResponse;
import com.vip.taxivip_driver.models.ProfileImageResponse;
import com.vip.taxivip_driver.models.ReachLocationResponse;
import com.vip.taxivip_driver.models.ResendOTPResponse;
import com.vip.taxivip_driver.models.ResetPasswordResponse;
import com.vip.taxivip_driver.models.SendReviewResponse;
import com.vip.taxivip_driver.models.SignInDataResponse;
import com.vip.taxivip_driver.models.SignupResponse;
import com.vip.taxivip_driver.models.StateResponse;
import com.vip.taxivip_driver.models.TermsConditionResponse;
import com.vip.taxivip_driver.models.TripHistoryResponse;
import com.vip.taxivip_driver.models.UpdateProfileResponse;
import com.vip.taxivip_driver.models.VehicleTypeResponse;
import com.vip.taxivip_driver.requestclass.AcceptBookingRequest;
import com.vip.taxivip_driver.requestclass.ContactAdminRequest;
import com.vip.taxivip_driver.requestclass.EndTripRequest;
import com.vip.taxivip_driver.requestclass.ForgetPasswordRequest;
import com.vip.taxivip_driver.requestclass.OtpVerifyRequest;
import com.vip.taxivip_driver.requestclass.ProfileDescribeRequest;
import com.vip.taxivip_driver.requestclass.ResetPasswordRequest;
import com.vip.taxivip_driver.requestclass.ReviewRequest;
import com.vip.taxivip_driver.requestclass.SetDestinationRequest;
import com.vip.taxivip_driver.requestclass.SingInRequest;
import com.vip.taxivip_driver.requestclass.SingupRequest;
import com.vip.taxivip_driver.requestclass.StartTripRequest;
import com.vip.taxivip_driver.requestclass.UpdateProfileRequest;
import com.vip.taxivip_driver.requestclass.VehicleUploadRequest;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RestApiService {


    @POST("api_signup")
    Call<SignupResponse> postSignUp(@Body SingupRequest singupRequest);


    @POST("api_login_driver")
    Call<SignInDataResponse> postLoginRequest(@Body SingInRequest singInRequest);


    @FormUrlEncoded
    @POST("api_resend_otp")
    Call<ResendOTPResponse> postresendOTp(@Field("user_id") int user_id);


    /*@FormUrlEncoded
    @POST("api_verify_otp")
    Call<OtpVerifiedResponse> postVerifyOtp(@Field("mobile") String mobileOrEmail,
                                            @Field("otp") String otp,
                                            @Field("roles_id") int roles_id,
                                            @Field("country_code") String country_code);*/

    @POST("api_verify_otp")
    Call<OtpVerifiedResponse> postVerifyOtp(@Body OtpVerifyRequest otpVerifyRequest);


    /*@FormUrlEncoded
    @POST("api_forgot_password")
    Call<ForgetPasswordResponse> postForgetPassword(@Field("mobile") String mobile,
                                                    @Field("new_password") String newPassword,
                                                    @Field("confirm_password") String confirmPassword,
                                                    @Field("roles_id") int roles_id,
                                                    @Field("country_code") String country_code);*/

    @POST("api_forgot_password")
    Call<ForgetPasswordResponse> postForgetPassword(@Body ForgetPasswordRequest forgetPasswordRequest);


   /* @FormUrlEncoded
    @POST("api_reset_password")
    Call<ResetPasswordResponse> postResetPassword(@Field("user_id") int user_id,
                                                  @Field("old_password") String old_password,
                                                  @Field("new_password") String newPassword,
                                                  @Field("confirm_password") String confirmPassword);*/

    @POST("api_reset_password")
    Call<ResetPasswordResponse> postResetPassword(@Body ResetPasswordRequest resetPasswordRequest);


    @FormUrlEncoded
    @POST("api_logout")
    Call<LogoutResponse> postLogOut(@Field("user_id") int userId);

    @GET("api_state")
    Call<StateResponse> getStateList();


    @GET("api_country")
    Call<CountryResponse> getCountryList();


    @GET("api_language")
    Call<LanguageResponse> getLanguageApi();


    @GET("api_city")
    Call<CityResponse> getCityList(@Query("state_id") int stateId);

    //Edit Profile

    @POST("api_update_driver_profile")
    Call<UpdateProfileResponse> postUpateProfile(@Body UpdateProfileRequest updateProfileRequest);


    /*@FormUrlEncoded
    @POST("api_update_driver_profile")
    Call<UpdateProfileResponse> postUpateProfileDescribe(@Field("driver_id") int driverId,
                                                         @Field("describe_yourself") String describe_yourself,
                                                         @Field("homeaddress") String homeaddress);*/

    @POST("api_update_driver_profile")
    Call<UpdateProfileResponse> postUpateProfileDescribe(@Body ProfileDescribeRequest profileDescribeRequest);


    @FormUrlEncoded
    @POST("api_update_driver_profile")
    Call<UpdateProfileResponse> postUpateProfileLanguage(@Field("driver_id") int driverId,
                                                         @Field("language_known") String language_known);


    // Get Profile
    @FormUrlEncoded
    @POST("api_get_profile_driver")
    Call<UpdateProfileResponse> postGetProfile(@Field("user_id") int userId);


    @FormUrlEncoded
    @Multipart
    @POST("api_document_edit")
    Call<DocumentResponse> getDocumentEdit(@Part("user_id") RequestBody user_id,
                                           @Part MultipartBody.Part driver_edit_pancard_image,
                                           @Part MultipartBody.Part driver_edit_license_image,
                                           @Part MultipartBody.Part car_edit_insurance_image,
                                           @Part MultipartBody.Part registration_edit_certificate_image,
                                           @Part MultipartBody.Part permit_edit_image,
                                           @Part("model_number") RequestBody model_number,
                                           @Part("car_name") RequestBody car_name,
                                           @Part("car_year") RequestBody car_year,
                                           @Part("car_color") RequestBody car_color);

    @Multipart
    @POST("api_document_upload")
    Call<DocumentResponse> getDocumentUpload(@Part("user_id") RequestBody user_id,
                                             @Part MultipartBody.Part driver_license_image,
                                             @Part MultipartBody.Part vehicleCertificate_body,
                                             @Part MultipartBody.Part vehicle_permit_image,
                                             @Part MultipartBody.Part carInsurance_body,
                                             @Part MultipartBody.Part criminal_record_image,
                                             @Part MultipartBody.Part police_recrod_image);


    @POST("api_document_upload")
    Call<DocumentResponse> postVehicleDetailsUpload(@Body VehicleUploadRequest vehicleUploadRequest);

    @Multipart
    @POST("api_update_profile_image")
    Call<ProfileImageResponse> postUploadImage(@Part("user_id") RequestBody user_id,
                                               @Part MultipartBody.Part profile_image);


    @FormUrlEncoded
    @POST("api_offline_online")
    Call<OfflineResponse> postOfflineDriver(@Field("user_id") int userId);


    @GET("api_viehcle_type")
    Call<VehicleTypeResponse> getVehicleType();


    @GET("api_aboutus")
    Call<AboutUsResponse> postAboutUsDetails(@Query("language") int language);

    @GET("api_terms_and_conditions_driver")
    Call<TermsConditionResponse> postTermsConditione(@Query("language") int language);

    @GET("api_privacy_policy")
    Call<TermsConditionResponse> postPrivacyPolicy(@Query("language") int language);


  /*  @FormUrlEncoded
    @POST("setDestination")
    Call<UpdateProfileResponse> postsetDestination(@Field("user_id") int user_id,
                                                   @Field("location") String location,
                                                   @Field("lat") String lat,
                                                   @Field("long") String lng);*/

    @POST("setDestination")
    Call<UpdateProfileResponse> postSetDestination(@Body SetDestinationRequest setDestinationRequest);

    @GET("contactUsInfo")
    Call<ContactUsResponse> getContactUsInfo();


    @GET("api_terms_and_conditions_driver")
    Call<CityResponse> getTermsConditions(@Query("language") int language);


    @POST("contactAdmin")
    Call<GetHelpResponse> postGetHelp(@Body ContactAdminRequest contactAdminRequest);

    // User Review Api

    @POST("sendReview")
    Call<SendReviewResponse> postSendReviewApi(@Body ReviewRequest reviewRequest);

    // Trip History Api
    @GET("tripHistoryDriver")
    Call<TripHistoryResponse> getTripHistory(@Query("driver_id") int driver_id);


    // Booking Accept Api
    @POST("acceptBooking")
    Call<AcceptBookingResponse> postAcceptBookingRequest(@Body AcceptBookingRequest acceptBookingRequest);

    // Start Trip Api
    @POST("startTrip")
    Call<AcceptBookingResponse> postStartTripRequest(@Body StartTripRequest startTripRequest);

    // Reach Location Api
    @POST("reachLocation")
    Call<ReachLocationResponse> reactLocationRequest(@Body AcceptBookingRequest acceptBookingRequest);


    // End Trip Request Api
    @POST("endTrip")
    Call<EndTripResponse> postEndTripApi(@Body EndTripRequest endTripRequest);

    //https://www.webmobril.org/dev/taxivip/api/api_booking_details

    // Get Booking Details
    @GET("api_booking_details")
    Call<BookingDetailsResponse> getBookingDetailsApi(@Query("booking_id") int booking_id);


}

