package com.vip.taxivip_driver.network;


import android.app.Application;
import android.content.Context;

import com.vip.taxivip_driver.application.MyApplication;
import com.vip.taxivip_driver.utils.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitInstanceNew {

    private static Retrofit retrofit = null;
    public synchronized static RestApiService getApiService() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            OkHttpClient.Builder oktHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(new NetworkConnectionInterceptor(MyApplication.getContext()));
            // Adding NetworkConnectionInterceptor with okHttpClientBuilder.
            oktHttpClient.addInterceptor(interceptor);
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(oktHttpClient.build())
                    .build();

        }
        return retrofit.create(RestApiService.class);

    }
}
