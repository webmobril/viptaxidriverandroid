package com.vip.taxivip_driver.views.activities.welcom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.views.fragments.signup_views.SignUp_fragment;
import com.vip.taxivip_driver.utils.CommonMethod;


public class WelcomeActivity extends AppCompatActivity {
    private Button btn_signin, btn_register;
    private LoginPreference loginPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initView();
        loginPreference = new LoginPreference(getApplicationContext());

        if (loginPreference.getDeviceToken().equals("a")) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(WelcomeActivity.this, instanceIdResult -> {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);
                loginPreference.saveDeviceToken(newToken);

            });
        }
    }

    private void initView() {
        btn_register = findViewById(R.id.btn_register);
        btn_signin = findViewById(R.id.btn_sign_in);
        btn_register.setOnClickListener(view -> {
            //SignUp_fragment fragment =new SignUp_fragment();
            //CallFragment(fragment);
            CommonMethod.setSimpleFragment(new SignUp_fragment(),WelcomeActivity.this,R.id.frame_singup);
        });
        btn_signin.setOnClickListener(view -> {
            CommonMethod.callOnlyActivity(WelcomeActivity.this, SignInActivity.class);
        });


    }

    /*public void CallFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_singup, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }*/


    public void onBackPressed(){
        if(getSupportFragmentManager().getBackStackEntryCount() >0){
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }


}
