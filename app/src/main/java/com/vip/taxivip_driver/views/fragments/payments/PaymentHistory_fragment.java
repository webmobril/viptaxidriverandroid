package com.vip.taxivip_driver.views.fragments.payments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.models.TripHistoryResponse;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.BookingViewModels;
import com.vip.taxivip_driver.views.adapter.PaymentHistoryAdapter;

import java.util.List;
import java.util.Objects;

public class PaymentHistory_fragment extends Fragment implements View.OnClickListener {

    private TextView tvToolbarTitle;
    private PaymentHistoryAdapter paymentHistoryAdapter;
    private RecyclerView driver_payment_recycler;
    private ImageView iv_Back, editProfileImg;
    private BookingViewModels bookingViewModels;
    private LoginPreference loginPreference;
    private int driverId;

    public PaymentHistory_fragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookingViewModels = new ViewModelProvider(this).get(BookingViewModels.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.driver_payment_fragment, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        editProfileImg = view.findViewById(R.id.editProfileImg);
        tvToolbarTitle = view.findViewById(R.id.tvToolbarTitle);
        driver_payment_recycler = view.findViewById(R.id.recycler_driver_payment);
        editProfileImg.setVisibility(View.GONE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(R.string.payments);
        iv_Back.setOnClickListener(this);

        // calling trip history api
        paymentHistoryApi(driverId);
    }


    private void paymentHistoryApi(int driverId) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        bookingViewModels.getTripHistoryViewModel(driverId).observe(this, tripHistoryResponse -> {
            myDialog.dismiss();
            if (tripHistoryResponse != null && tripHistoryResponse.getError() != null && !tripHistoryResponse.getError()) {
                List<TripHistoryResponse.Result> tripHistoryResponseList = tripHistoryResponse.getResult();
                prepareRecyclerView(tripHistoryResponseList);
            } else if (tripHistoryResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(), tripHistoryResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());

            }
        });
    }


    private void prepareRecyclerView(List<TripHistoryResponse.Result> resultList) {
        paymentHistoryAdapter = new PaymentHistoryAdapter(getActivity(), resultList);
        CommonMethod.setRecyclerView(getActivity(), driver_payment_recycler);
        driver_payment_recycler.setAdapter(paymentHistoryAdapter);
        paymentHistoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_Back) {
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
    }
}

