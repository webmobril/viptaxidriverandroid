package com.vip.taxivip_driver.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.fragments.profiles_fragments.AddLanguageFragment;
import com.vip.taxivip_driver.models.LanguageResponse;

import java.util.ArrayList;
import java.util.List;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageViewHolder> {

    private List<LanguageResponse.Result> resultList;
    private Context mContext;
    int index = -1;
    public int mSelectedItem = -1;

    MyclickListnerLearning myAdapterlistener;
    private ArrayList<String> addRemoveArrylist = new ArrayList<>();

    public interface MyclickListnerLearning  {
        void onViewClick(View view, ArrayList<String> stringArrayList);
    }

    public LanguageAdapter(Context mContext , List<LanguageResponse.Result> resultList , AddLanguageFragment myAdapterlistener){
        this.mContext = mContext;
        this.resultList = resultList;
        this.myAdapterlistener = myAdapterlistener;

    }

    @NonNull
    @Override
    public LanguageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_adapter,parent,false);
        return new LanguageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageViewHolder holder, int position) {

        if (resultList.size() >0){
            holder.languageText.setText(resultList.get(position).getName());
        }
         resultList.get(position).setChecked(position== mSelectedItem);
            holder.imageViewIcTick.setTag(position);




        holder.linearLayoutId.setOnClickListener(v -> {
            index = position;

            if (holder.imageViewIcTick.getVisibility() == View.VISIBLE) {
                holder.imageViewIcTick.setVisibility(View.INVISIBLE);
                addRemoveArrylist.remove(resultList.get(position).getName());
                myAdapterlistener.onViewClick(v, addRemoveArrylist);
            }
            else {
                addRemoveArrylist.add(resultList.get(position).getName());
                myAdapterlistener.onViewClick(v, addRemoveArrylist);
                holder.imageViewIcTick.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }

    public class LanguageViewHolder extends RecyclerView.ViewHolder {
        private TextView languageText;
        private RelativeLayout linearLayoutId;
        private ImageView imageViewIcTick;
        public LanguageViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayoutId = itemView.findViewById(R.id.linearLayoutId);
            languageText = itemView.findViewById(R.id.languageText);
            imageViewIcTick = itemView.findViewById(R.id.imageViewIcTick);
            /*languageText.setOnClickListener(v -> {
                int posIndex = getAdapterPosition();
                myAdapterlistener.onViewClick(v, resultList.get(posIndex).getName());
            });*/
        }
    }
}
