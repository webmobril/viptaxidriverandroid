package com.vip.taxivip_driver.views.activities.welcom;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import com.hbb20.CountryCodePicker;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.SingInRequest;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;
import com.vip.taxivip_driver.views.activities.ContainerActivity;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.views.fragments.signup_views.ForgetPassword_fragment;
import com.vip.taxivip_driver.views.fragments.signup_views.OtpFragment;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;

public  class SignInActivity extends AppCompatActivity implements View.OnClickListener {
        private Button btn_signin;
        private ImageView iv_Back;
        private TextView txt_forgot;
        private EditText ed_phone_number, ed_password;
        private SingInSignUpViewModel signInViewModel;
        private LoginPreference loginPreference;
        String password, phone;
        String deviceToken;
        private FrameLayout layout_container;
        private CountryCodePicker ccp;
        private String countryCode;


    @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.signin_activity);
            loginPreference = new LoginPreference(this);
            deviceToken = loginPreference.getDeviceToken();
            signInViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
            initView();
        }

        private void initView() {

         layout_container = findViewById(R.id.layout_container);
            iv_Back= findViewById(R.id.iv_Back);
            txt_forgot = findViewById(R.id.txt_forgot_password);
            ed_password = findViewById(R.id.ed_password);
            ed_phone_number = findViewById(R.id.ed_phone_number);
            btn_signin = findViewById(R.id.btn_signin);
            ccp =  findViewById(R.id.ccp);
            txt_forgot.setOnClickListener(this);
            btn_signin.setOnClickListener(this);
            iv_Back.setOnClickListener(this);
            countryCode = ccp.getDefaultCountryCode();

            ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                @Override
                public void onCountrySelected() {
                    countryCode = ccp.getSelectedCountryCode();
                    Log.e(this.getClass().getName(),"SELECTED_COUNTRY" + countryCode);
                }
            });
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.iv_Back:
                    customBackPress();
                    break;
                case R.id.btn_signin:
                    password = ed_password.getText().toString().trim();
                    phone = ed_phone_number.getText().toString().trim();
                        if (isValidated(phone, password)) {
                            btn_signin.setClickable(false);
                            SingInRequest singInRequest = new SingInRequest();
                            singInRequest.setUsername(phone);
                            singInRequest.setPassword(password);
                            singInRequest.setCountry_code(countryCode);
                            singInRequest.setDevice_type("1");
                            singInRequest.setDevice_token(deviceToken);
                            postSignInCall(singInRequest);
                        }
                    break;

                case R.id.txt_forgot_password:
                    layout_container.clearFocus();
                    ed_phone_number.setError(null);
                    ed_phone_number.clearFocus();
                    ed_password.setError(null);
                    ed_password.clearFocus();
                    CommonMethod.setSimpleFragment(new ForgetPassword_fragment(),SignInActivity.this,R.id.layout_container);
                    break;
            }
        }


        private void postSignInCall(SingInRequest singInRequest) {
            ProgressDialog myDialog = CommonMethod.showProgressDialog(this, getString(R.string.please_wait));
            signInViewModel.getSignIn(singInRequest).observe(this, signInDataResponse -> {
                myDialog.dismiss();
                btn_signin.setClickable(true);
                if (signInDataResponse .getError()!= null && !signInDataResponse.getError()) {
                    Log.e(Constants.TAG_SIGNIN, "LoginResponse" + signInDataResponse.getMessage());
                    loginPreference.setIsLoggedIn(true);
                    loginPreference.setRegisterToken(signInDataResponse.getResult().getAuthToken());
                    loginPreference.setRegister_id(signInDataResponse.getResult().getId());
                    loginPreference.setUserImage(signInDataResponse.getResult().getUserImage());
                    loginPreference.setName(signInDataResponse.getResult().getName());
                    loginPreference.setDescribeYourself(signInDataResponse.getResult().getDescribeYourself());
                    loginPreference.setLanguageKnown(signInDataResponse.getResult().getLanguageKnown());
                    loginPreference.setWhereFrom(signInDataResponse.getResult().getHomeaddress());
                    loginPreference.setVerifyDoc(signInDataResponse.getResult().getVerify());
                      CommonMethod.callActivity(SignInActivity.this, ContainerActivity.class);

                } else if (signInDataResponse.getMessage() != null &&
                        signInDataResponse.getMessage().equals("Please Verify Your Mobile Number")){
                    loginPreference.setRegister_id(signInDataResponse.getResult().getId());
                    OtpFragment otp_fragment = new OtpFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.MOBILE_NO, phone);
                    bundle.putString(Constants.COUNTRY_CODE, countryCode);
                    bundle.putString(Constants.OTP, "signin");
                    otp_fragment.setArguments(bundle);
                    CommonMethod.setSimpleFragment(new OtpFragment(),SignInActivity.this,R.id.layout_container);

                }else if (signInDataResponse.getMessage() != null){
                    CommonMethod.showToast(SignInActivity.this,signInDataResponse.getMessage());

                } else {
                    CommonMethod.showAlert(getString(R.string.internet_connection),this);
                }

            });
        }

        private boolean isValidated(String phone, String password) {
            String Regex = "[^\\d]";
            String PhoneDigits = phone.replaceAll(Regex, "");
            if (TextUtils.isEmpty(phone)) {
                ed_phone_number.setError(getString(R.string.enter_phone_no));
                ed_phone_number.requestFocus();
                return false;
            } else if ((PhoneDigits.length() < 7) || (PhoneDigits.length() > 15)) {
                ed_phone_number.setError(getString(R.string.enter_valid_mobile_no));
                ed_phone_number.requestFocus();
                return false;
            } else if (TextUtils.isEmpty(password)) {
                ed_password.setError(getString(R.string.password_required),null);
                ed_password.requestFocus();
                return false;
            }
            return true;
        }


    @Override
    protected void onPause() {
        super.onPause();
        layout_container.clearFocus();
        ed_phone_number.setError(null);
        ed_phone_number.clearFocus();
        ed_password.setError(null);
        ed_password.clearFocus();
    }
        public void onBackPressed(){
            if(getSupportFragmentManager().getBackStackEntryCount() <= 1){
                super.onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }

        void customBackPress(){
            if(getSupportFragmentManager().getBackStackEntryCount() <= 1){
                super.onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack();
            }

        }
    }