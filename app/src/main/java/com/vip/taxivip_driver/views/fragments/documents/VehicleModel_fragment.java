package com.vip.taxivip_driver.views.fragments.documents;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.views.fragments.accounts_.PrivacyPolicy_fragment;
import com.vip.taxivip_driver.views.fragments.accounts_.TermsCondition_fragment;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.ProfileViewModel;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;

import java.util.Objects;


public class VehicleModel_fragment extends Fragment implements View.OnClickListener {
    private EditText et_brand,et_model,et_year,et_color;
    private TextView privacyPolicyTerms;
    ProfileViewModel profileViewModel;
    private Button btn_continue;
    private LoginPreference loginPreference;
    private int driverId;
    private ImageView iv_Back;
    private String frameType;
    private SingInSignUpViewModel signupViewModel;
    private ProgressBar post_progressbar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            frameType = bundle.getString(Constants.HOME_SCREEN);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vehicle_model_fragment,container,false);
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        signupViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId= loginPreference.getRegister_id();

        initViews(view);
        return view;
    }

    private void initViews(View view) {
        privacyPolicyTerms = view.findViewById(R.id.privacyPolicyTerms);
        post_progressbar = view.findViewById(R.id.post_progressbar);
        iv_Back= view.findViewById(R.id.iv_Back);
        et_brand = view.findViewById(R.id.et_brand);
        et_model = view.findViewById(R.id.et_model);
        et_year = view.findViewById(R.id.et_year);
        et_color = view.findViewById(R.id.et_color);
        btn_continue = view.findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
        iv_Back.setOnClickListener(this);

        customTextView(privacyPolicyTerms);
        postGetProfile(driverId);

    }


    private void customTextView(TextView view) {
       // By proceeding, i agree to Taxi VIP Terms &amp; Conditions and acknowledge that I have read the Privacy Policy.

        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "I agree to Taxi VIP");
        spanTxt.append(" Term & Conditions");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.sky_blue));
            }

            @Override
            public void onClick(View widget) {
                TermsCondition_fragment fragment = new TermsCondition_fragment();
                CallFragment(fragment);
                //startActivity(new Intent(getApplicationContext(),PrivacyPolicyActivity.class));
            }
        }, spanTxt.length() - "Term & Conditions".length(), spanTxt.length(), 0);
        spanTxt.append(" and acknowledge that I have read the ");
        spanTxt.setSpan(new ForegroundColorSpan(Color.BLACK), 62, spanTxt.length(), 0);
        spanTxt.append(" Privacy Policy");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.sky_blue));
            }
            @Override
            public void onClick(View widget) {

                PrivacyPolicy_fragment fragment = new PrivacyPolicy_fragment();
                CallFragment(fragment);

                //startActivity(new Intent(getApplicationContext(), TermsAndConditionsActivity.class));
            }
        }, spanTxt.length() - " Privacy Policy".length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }




    private void postGetProfile(int driverId){
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        signupViewModel.postGetProfileViewModel(driverId).observe(this, updateProfileResponse -> {
            myDialog.dismiss();
            if (updateProfileResponse.getError()!= null && !updateProfileResponse.getError()){
                Log.e(this.getClass().getName(),"Get_ProfileModelResponse=" + updateProfileResponse.getError());

                if (updateProfileResponse.getResult().getDocuments() != null) {

                    et_brand.setText(updateProfileResponse.getResult().getDocuments().getCarName());
                    if (!updateProfileResponse.getResult().getDocuments().getModelNumber().equals("0")) {
                        et_model.setText(updateProfileResponse.getResult().getDocuments().getModelNumber());
                    }
                    et_year.setText(updateProfileResponse.getResult().getDocuments().getCarYear());
                    et_color.setText(updateProfileResponse.getResult().getDocuments().getCarColor());

                }

            }else if (updateProfileResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(),updateProfileResponse.getMessage());
            } else {
                CommonMethod.showAlert("Internet Connectivity Failure", getActivity());
            }
        });

    }




    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_continue){
            String brandCar = et_brand.getText().toString();
            String carModel = et_model.getText().toString();
            String carYear = et_year.getText().toString();
            String carColor = et_color.getText().toString();

            //if (CommonMethod.isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
                if (checkValidation(brandCar, carModel, carYear,carColor)) {
                    //VehicleModelUploadApi(driverId, brandCar,carModel,carYear,carColor);

                    VehicleTypeFragment fragment = new VehicleTypeFragment();
                    Bundle args = new Bundle();
                    args.putString(Constants.HOME_SCREEN,frameType);
                    args.putString(Constants.VEHICLE_MODEL,carModel);
                    args.putString(Constants.VEHICLE_BRAND,brandCar);
                    args.putString(Constants.VEHICLE_YEAR, carYear);
                    args.putString(Constants.VEHICLE_COLOR,carColor);
                    fragment .setArguments(args);
                    CallFragment(fragment);
                }
          /*  } else {
                CommonMethod.showAlert("Internet Connectivity Failure", getActivity());
            }*/



        } else if (v.getId() == R.id.iv_Back){
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
    }





    private boolean checkValidation(String m_brand , String m_model , String m_year,String mColor) {

        if (TextUtils.isEmpty(m_brand)) {
            et_brand.requestFocus();
            et_brand.setError(getString(R.string.enter_brand));
            return false;
        } else if (TextUtils.isEmpty(m_model)) {
            et_model.requestFocus();
            et_model.setError(getString(R.string.enter_model));
            return false;
        } else if (TextUtils.isEmpty(m_year)) {
            et_year.requestFocus();
            et_year.setError(getString(R.string.please_enter_year));
            return false;
        } else if (TextUtils.isEmpty(mColor)) {
            et_color.requestFocus();
            et_color.setError(getString(R.string.please_enter_color));
            return false;
        }
        return true;
    }


    public void CallFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if  (frameType != null && frameType.equals("first_Signup")){
            fragmentTransaction.replace(R.id.fragment_container, fragment).addToBackStack(null);
        }else {
            fragmentTransaction.replace(R.id.frame_about, fragment).addToBackStack(null);
        }

        fragmentTransaction.commit();
    }

}
