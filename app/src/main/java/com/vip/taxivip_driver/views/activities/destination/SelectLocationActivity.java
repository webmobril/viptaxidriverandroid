package com.vip.taxivip_driver.views.activities.destination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.adapter.SearchedAddressAdapter;
import com.vip.taxivip_driver.interfaces.GetAddress;
import com.vip.taxivip_driver.models.SearchAddressModel;
import com.google.android.gms.common.api.ApiException;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectLocationActivity extends AppCompatActivity implements GetAddress {

    EditText etHome, etDestination;
    SelectLocationActivity context;
    RecyclerView search_list;
    ImageView back,clearImg;
    private TextView mSearchResult;
    private StringBuilder mResult;
    String TAG = "SelectLocationActivity";
    SearchedAddressAdapter searchedAddressAdapter;
    ArrayList<SearchAddressModel> searchAddressModelArrayList = new ArrayList<>();
    RelativeLayout RlHome, RlWork, RlSaved;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_destination);
        context = SelectLocationActivity.this;

        initViews();
    }

    private void initViews() {
        etDestination = findViewById(R.id.search_et);
        search_list = findViewById(R.id.list_search);
        back = findViewById(R.id.iv_Back);
        clearImg = findViewById(R.id.clearImg);
        String placeId = getString(R.string.google_maps_key);

        // mSearchResult = findViewById(R.id.searchResult);
        //RlSaved = findViewById(R.id.RlSaved);
        //RlHome = findViewById(R.id.RlHome);
        //RlWork = findViewById(R.id.RlWork);

        //LoginPreferences LoginPref = LoginPreferences.getActiveInstance(context);
        //String address = CommonMethod.getCompleteAddressString(context, LoginPref.getCurrentLatitude(), LoginPref.getCurrentLongitude());
       // etHome.setText(address);

        back.setOnClickListener(view -> finish());

        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            com.google.android.libraries.places.api.Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        }
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);


        etDestination.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

                FetchPlaceRequest request1 = FetchPlaceRequest.newInstance(placeId, placeFields);


                AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
                FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                        .setCountry("AU")
                        .setSessionToken(token)
                        .setQuery(etDestination.getText().toString())
                        .build();


                placesClient.findAutocompletePredictions(request).addOnSuccessListener(response -> {
                    mResult = new StringBuilder();
                    searchAddressModelArrayList.clear();
                    for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                        SearchAddressModel searchAddressModel = new SearchAddressModel();
                        searchAddressModel.setAddress(prediction.getFullText(null).toString());
                        searchAddressModelArrayList.add(searchAddressModel);
                        Log.e(this.getClass().getName(),"lat_lng" + prediction.getPlaceTypes());
                       // LatLng qLoc = response.getLatLng();

                    }
                    if (searchAddressModelArrayList.size() > 0) {
                        setAdapter(searchAddressModelArrayList);
                    }
                }).addOnFailureListener((exception) -> {
                    if (exception instanceof ApiException) {
                        ApiException apiException = (ApiException) exception;
                        Log.e(TAG, "Place not found: " + apiException.getStatusCode());
                    }
                });
            }
        });
    }

    private void setAdapter(ArrayList<SearchAddressModel> searchAddressModelArrayList) {
        Log.e(TAG, "searchAddressModelArrayList --> " + searchAddressModelArrayList.size());
        searchedAddressAdapter = new SearchedAddressAdapter(context, searchAddressModelArrayList, this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(SelectLocationActivity.this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        search_list.setLayoutManager(layoutManager);
        search_list.setAdapter(searchedAddressAdapter);
    }
    @Override
    public void onClickChange(String address) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("SelectAddress", address);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
