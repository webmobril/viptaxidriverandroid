package com.vip.taxivip_driver.views.fragments.profiles_fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.UpdateProfileRequest;
import com.vip.taxivip_driver.views.adapter.PlacesAutoCompleteAdapter;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.models.CountryMessage;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;


public class EditInfo_fragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private EditText ed_first_name, ed_last_name, ed_phone_number, ed_email;
    private Spinner spinner_country;
    private SingInSignUpViewModel signupViewModel;
    private List<CountryMessage> countryMessageList;
    private ProgressBar post_progressbar;
    private Button btn_continue;
    private LoginPreference loginPreference;
    private int driverId;
    private int countryID;
    private ImageView iv_Back, imageUpload, imageViewProfileEdit;
    private AutoCompleteTextView cityAutocomplete;
    private File update_destination;
    private Bitmap update_bitmap;
    private String update_imgPath;
    private String update_imageString;
    private Context mContext;


    public EditInfo_fragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signupViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_info_fragment, container, false);

        initViews(view);
        postGetProfile(driverId);
        return view;
    }

    private void initViews(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        imageUpload = view.findViewById(R.id.imageUpload);
        imageViewProfileEdit = view.findViewById(R.id.imageViewProfileEdit);
        ed_first_name = view.findViewById(R.id.ed_first_name);
        ed_last_name = view.findViewById(R.id.ed_last_name);
        ed_phone_number = view.findViewById(R.id.ed_phone_number);
        ed_email = view.findViewById(R.id.ed_email);
        cityAutocomplete = view.findViewById(R.id.cityAutocomplete);
        //  spinner_country = view.findViewById(R.id.spinner_country);
        post_progressbar = view.findViewById(R.id.post_progressbar);
        btn_continue = view.findViewById(R.id.btn_continue);
        iv_Back.setOnClickListener(this);
        btn_continue.setOnClickListener(this);
        imageViewProfileEdit.setOnClickListener(this);

        cityAutocomplete.setOnItemClickListener(this);
        cityAutocomplete.setAdapter(new PlacesAutoCompleteAdapter(getActivity(), R.layout.autocomplete_list_item));



        /*try {
            loadCountryData();
        }catch (Exception e){e.printStackTrace();}
*/
        /*spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String state=   spinner_country.getItemAtPosition(spinner_country.getSelectedItemPosition()).toString();
                countryID=countryMessageList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String str = (String) parent.getItemAtPosition(position);
        CommonMethod.showToast(getActivity(), str);

    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_continue) {
            String firstName = ed_first_name.getText().toString();
            String lastName = ed_last_name.getText().toString();
            String email = ed_email.getText().toString();
            String country = cityAutocomplete.getText().toString();
            String state = cityAutocomplete.getText().toString();
            UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();
            updateProfileRequest.setDriver_id(driverId);
            updateProfileRequest.setFirstname(firstName);
            updateProfileRequest.setLastname(lastName);
            updateProfileRequest.setEmail(email);
            updateProfileRequest.setHomeaddress(state);
            updateProfileRequest.setCity(country);

                updateProfilePost(updateProfileRequest);

        } else if (v.getId() == R.id.iv_Back) {
            getActivity().onBackPressed();
        } else if (v.getId() == R.id.imageViewProfileEdit) {
            CaptureImage();
        }
    }


    private void CaptureImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA}, 1);
            }
        } else {
            selectImage();
        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("msg", "Permission callback called-------");
        // Initialize the map with both permissions
        // Fill with actual results from user
        if (requestCode == Constants.REQUEST_ID_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<>();
            perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for  permissions

                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ) {
                    Log.e("msg", "All Permissions granted");
                } else {
                    Log.e("msg", "Some permissions are not granted ask again ");
                    if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showDialogOK(getResources().getString(R.string.permission_req),
                                (dialog, which) -> {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            // proceed with logic by disabling the related features or quit the app.
                                            break;
                                    }
                                });
                    }
                    //permission is denied (and never ask again is  checked)
                    //shouldShowRequestPermissionRationale will return false
                    else {
                        CommonMethod.showToast(getActivity(), getString(R.string.enable_permission));
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.Okay), okListener)
                .setNegativeButton(getResources().getString(R.string.Okay), okListener)
                .create()
                .show();
    }

    private boolean checkAndRequestPermissions() {
        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[0]), Constants.REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void postGetProfile(int driverId) {
        post_progressbar.setVisibility(View.VISIBLE);
        signupViewModel.postGetProfileViewModel(driverId).observe(this, updateProfileResponse -> {
            post_progressbar.setVisibility(View.GONE);
            if (updateProfileResponse.getError() != null && !updateProfileResponse.getError()) {
                Log.e(this.getClass().getName(), "Get_Profile_Response=" + updateProfileResponse.getMessage());
                if (updateProfileResponse.getResult() != null) {
                    if (updateProfileResponse.getResult().getFirstName() != null) {
                        ed_first_name.setText(updateProfileResponse.getResult().getFirstName());
                    }
                    if (updateProfileResponse.getResult().getLastName() != null) {
                        ed_last_name.setText(updateProfileResponse.getResult().getLastName());
                    }
                    ed_phone_number.setText(updateProfileResponse.getResult().getMobile());
                    ed_email.setText(updateProfileResponse.getResult().getEmail());
                    cityAutocomplete.setText(updateProfileResponse.getResult().getCityId());
                    loginPreference.setUserImage(updateProfileResponse.getResult().getUserImage());
                    String imgUrl = Constants.IMAGE_URL + updateProfileResponse.getResult().getUserImage();
                    CommonMethod.loadImageResized(getActivity(), imgUrl, imageUpload);
                }
            } else if (updateProfileResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(), updateProfileResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
            }
        });

    }


    private void updateProfilePost(UpdateProfileRequest updateProfileRequest) {
        post_progressbar.setVisibility(View.VISIBLE);
        signupViewModel.postUpdateProfileViewModel(updateProfileRequest)
                .observe(this, updateProfileResponse -> {
                    post_progressbar.setVisibility(View.GONE);
                    if (updateProfileResponse.getError() != null && !updateProfileResponse.getError()) {
                        CommonMethod.showToast(getActivity(), updateProfileResponse.getMessage());
                        // Toast.makeText(getActivity(), updateProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                        //CommonMethod.callActivty(getActivity(), Profile_Activity.class);

                    } else if (updateProfileResponse.getMessage() != null){
                        CommonMethod.showToast(getActivity(), updateProfileResponse.getMessage());
                    } else {
                        CommonMethod.showAlert(getString(R.string.internet_connection),getActivity());
                    }

                });
    }


    /* upload profile pic */

    private void uploadImage(File profileImgFile) {
        post_progressbar.setVisibility(View.VISIBLE);
        RequestBody profileImage = RequestBody.create(MediaType.parse("multipart/form-data"), profileImgFile);
        MultipartBody.Part image_body = MultipartBody.Part.createFormData("profile_image", profileImgFile.getName(), profileImage);
        RequestBody userIdBody = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(driverId));

        signupViewModel.postProfileImageViewModel(userIdBody, image_body).observe(this, profileImageResponse -> {
            post_progressbar.setVisibility(View.GONE);
            String message = null;
            if (profileImageResponse.getError() != null && !profileImageResponse.getError()) {
                message = profileImageResponse.getMessage();
                loginPreference.setUserImage(profileImageResponse.getResult().getUserImage());
                CommonMethod.loadImageResized(getActivity(),
                        Constants.IMAGE_URL + profileImageResponse.getResult().getUserImage(), imageUpload);
                // Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                CommonMethod.showToast(getActivity(), message);
                getActivity().onBackPressed();

            } else {
                CommonMethod.showToast(getActivity(), message);
                //Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void loadCountryData() {
        signupViewModel.getCountryResponse().observe(this, countryResponse -> {
            //Log.e(this.getClass().getName(),"STATE_RESPONSE=" + countryResponse.size());
            if (countryResponse != null && countryResponse.size() > 0) {
                countryMessageList = new ArrayList<>();
                for (int i = 0; i < countryResponse.size(); i++) {
                    CountryMessage stateMessage = new CountryMessage();
                    String name = countryResponse.get(i).getName();
                    int id = countryResponse.get(i).getId();
                    stateMessage.setName(name);
                    stateMessage.setId(id);
                    countryMessageList.add(stateMessage);

                }
                ArrayAdapter<CountryMessage> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryMessageList);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                spinner_country.setAdapter(spinnerArrayAdapter);
                spinnerArrayAdapter.notifyDataSetChanged();
            }


        });
    }


    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, Constants.CAMERA_REQUEST);

            } else if (options[item].equals("Choose From Gallery")) {
                Intent intentGalley = new Intent(Intent.ACTION_PICK);
                intentGalley.setType("image/*");
                startActivityForResult(intentGalley, Constants.GALLERY_IMAGE);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK && data != null) {
            try {
                update_bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                update_bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
                byte[] imageBytes = bytes.toByteArray();
                update_imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                update_destination = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                        "Camera" + "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    update_destination.createNewFile();
                    fo = new FileOutputStream(update_destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                update_imgPath = update_destination.getAbsolutePath();
                imageUpload.setImageBitmap(update_bitmap);
                uploadImage(update_destination);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.GALLERY_IMAGE && resultCode == RESULT_OK && data != null) {

            Uri selectedImage = data.getData();
            try {
                update_bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                update_bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
                Log.e("Activity", "Pick from Gallery::>>> ");
                update_imgPath = getRealPathFromURI(selectedImage);
                byte[] imageBytes = bytes.toByteArray();
                update_imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                update_destination = new File(update_imgPath);
                imageUpload.setImageBitmap(update_bitmap);
                uploadImage(update_destination);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


}
