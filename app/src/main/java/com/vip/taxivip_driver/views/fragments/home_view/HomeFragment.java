package com.vip.taxivip_driver.views.fragments.home_view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.models.NotificationResponse;
import com.vip.taxivip_driver.requestclass.AcceptBookingRequest;
import com.vip.taxivip_driver.requestclass.EndTripRequest;
import com.vip.taxivip_driver.requestclass.StartTripRequest;
import com.vip.taxivip_driver.services.MyLocationService;
import com.vip.taxivip_driver.utils.AppData;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.BookingViewModels;
import com.vip.taxivip_driver.views.activities.destination.NewGooglePlaceSearch;
import com.vip.taxivip_driver.views.activities.profile.Profile_Activity;
import com.vip.taxivip_driver.views.fragments.SecurityEnterPasswordFragment;
import com.vip.taxivip_driver.views.fragments.booking_fragment.UserRatingFragment;
import com.vip.taxivip_driver.views.fragments.documents.Document_Fragment;
import com.vip.taxivip_driver.views.fragments.payments.PaymentHistory_fragment;

import java.util.Objects;

import static com.vip.taxivip_driver.utils.CommonMethod.isLocationEnabled;


public class HomeFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnCameraMoveListener, View.OnClickListener {

    private SupportMapFragment supportMapFragment;
    private GoogleMap googleMap;
    private static View view;
    private Context mContext;
    private Activity activity;
    private ImageView driver_icon_profile, search_icon, go_online_offline, go_offline_img;
    private TextView moneyTextView, txtOfflineOnline;
    private BookingViewModels bookingViewModels;
    private LoginPreference loginPreference;
    private int driverId;
//    private ProgressBar post_progressbar;
    private int verifyAccountDoc;
    private Marker myMarker;
    private LocationManager manager;
    private LatLng latitudeLng;
    private RelativeLayout changeOnOffLayout;
    private NotificationResponse.Result notificationData = AppData.getInstance().getNotificationData();
    // public static HomeFragment homeFragment;


    private IntentFilter location_filter = new IntentFilter(Constants.LOCATION_RECEIVER);
    private BroadcastReceiver locationReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            double currentlat = intent.getDoubleExtra("lat", 0.0);
            double currentlng = intent.getDoubleExtra("lng", 0.0);
            latitudeLng = new LatLng(currentlat, currentlng);
            if (googleMap != null) {
                setCurrentLocation(new LatLng(currentlat, currentlng));
            }
        }
    };
    private AlertDialog alertDialog;
    private Animation slide_down,slide_up;

    /*Reach Location Layout*/
    private LinearLayout ll_01_contentLayer_show;
    private Button btn_01_reachLocation;
    private ImageView img01UserImg,img01Call;
    private TextView txt01UserName,sourceAddressText,kmMeterText,currentTimeText;
    private RatingBar rat01UserRating;


    /*Start Ride Views*/
    private LinearLayout ll_02_contentLayer_start_ride;
    private Button btn_02_accept;
    private TextView sourceAddressText02,txt02UserName,currentTimeText02,kmText02;
    private ImageView img2Call,img02User;
    private RatingBar rat02UserRating;

    /*End Trip Layout*/
    private LinearLayout ll_04_contentLayer_end_trip;
    private Button btn_04_endTrip;
    private ProgressDialog myDialog;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        Log.e(this.getClass().getName(), "onAttach");

        mContext.startService(new Intent(getActivity(), MyLocationService.class));
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(this.getClass().getName(), "onCreate");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        bookingViewModels = new ViewModelProvider(this).get(BookingViewModels.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
        //  homeFragment = HomeFragment.this;

        setUpMapIfNeeded();
        initViews(view);
        return view;
    }


    private void initViews(View view) {
        changeOnOffLayout = view.findViewById(R.id.changeOnOffLayout);
        txtOfflineOnline = view.findViewById(R.id.txtOfflineOnline);
        go_online_offline = view.findViewById(R.id.go_online_offline);
        go_offline_img = view.findViewById(R.id.go_offline_img);
        search_icon = view.findViewById(R.id.search_icon);
        moneyTextView = view.findViewById(R.id.moneyTextView);
        driver_icon_profile = view.findViewById(R.id.driver_icon_profile);
        moneyTextView.setOnClickListener(this);
        driver_icon_profile.setOnClickListener(this);
        search_icon.setOnClickListener(this);
        go_online_offline.setOnClickListener(this);
        go_offline_img.setOnClickListener(this);

        ll_01_contentLayer_show = view.findViewById(R.id.ll_01_contentLayer_show);
        btn_01_reachLocation = view.findViewById(R.id.btn_01_reachLocation);
        btn_01_reachLocation.setOnClickListener(this);
        img01UserImg = view.findViewById(R.id.img01UserImg);
        img01Call = view.findViewById(R.id.img01Call);
        txt01UserName = view.findViewById(R.id.txt01UserName);
        sourceAddressText = view.findViewById(R.id.sourceAddressText);
        rat01UserRating = view.findViewById(R.id.rat01UserRating);
        kmMeterText =view.findViewById(R.id.kmMeterText);
        currentTimeText = view.findViewById(R.id.currentTimeText);

        // start Ride Layout
        ll_02_contentLayer_start_ride = view.findViewById(R.id.ll_02_contentLayer_start_ride);
        btn_02_accept = view.findViewById(R.id.btn_02_accept);
        btn_02_accept.setOnClickListener(this);

        // End Trip Layout
        ll_04_contentLayer_end_trip = view.findViewById(R.id.ll_04_contentLayer_end_trip);
        btn_04_endTrip = view.findViewById(R.id.btn_04_endTrip);
        btn_04_endTrip.setOnClickListener(this);

        verifyAccountDoc = loginPreference.getVerifyDoc();
        CommonMethod.loadImageResized(getActivity(),
                Constants.IMAGE_URL + loginPreference.getUserImage(), driver_icon_profile);


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Really Exit?")
                        .setMessage("Are you sure you want to exit?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, (arg0, arg1) -> {
                            Intent a = new Intent(Intent.ACTION_MAIN);
                            a.addCategory(Intent.CATEGORY_HOME);
                            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(a);
                        }).create().show();


                return true;
            }
            return false;
        });

        view.findViewById(R.id.my_location).setOnClickListener(v -> {
            if (!isLocationEnabled(mContext)) {
                CommonMethod.showAlert(getString(R.string.gps_alert), getActivity());
            }
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latitudeLng, 17f));
        });

        try {
            manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            } else {
                supportMapFragment.getMapAsync(this);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (AppData.getInstance().getIsfromNotification()) {
            if (AppData.getInstance().getFROM().equals(Constants.NEW_BOOKING)) {
                showPickupRequest();
            } /*else if (AppData.getInstance().getFROM().equals(Constants.DESTINATION_CHANGED)) {
                CommonMethod.setFragment(new UserLocationFragment(), true, MainActivity.activity, R.id.container);
            }*/
        }

        int resultOfflineOnlnie = loginPreference.getOfflineResult();
        if (resultOfflineOnlnie == 0) {
            go_online_offline.setVisibility(View.GONE);
            go_offline_img.setVisibility(View.VISIBLE);
            txtOfflineOnline.setText(getString(R.string.you_are_offline_now));

        } else {
            go_online_offline.setVisibility(View.VISIBLE);
            go_offline_img.setVisibility(View.GONE);
            txtOfflineOnline.setText(getString(R.string.you_are_online_now));
        }

        //Load animation
        slide_down = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
    }

    private void setUpMapIfNeeded() {
        // if (googleMap == null) {
        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        supportMapFragment.getMapAsync(this);
        // }
        //if (googleMap != null) {
        // setupMap();
        // }
    }

    @SuppressWarnings("MissingPermission")
    private void setupMap() {
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(false);
        googleMap.setOnCameraMoveListener(this);

    }



    public void clearVisibility() {

        if (ll_01_contentLayer_show.getVisibility() == View.VISIBLE) {
            ll_01_contentLayer_show.startAnimation(slide_down);
        } else if (ll_02_contentLayer_start_ride.getVisibility() == View.VISIBLE) {
            ll_02_contentLayer_start_ride.startAnimation(slide_down);
        } /*else if (ll_03_contentLayer_service_flow.getVisibility() == View.VISIBLE) {
            //ll_03_contentLayer_service_flow.startAnimation(slide_down);
        } else if (ll_04_contentLayer_payment.getVisibility() == View.VISIBLE) {
            ll_04_contentLayer_payment.startAnimation(slide_down);
        } else if (ll_04_contentLayer_payment.getVisibility() == View.VISIBLE) {
            ll_04_contentLayer_payment.startAnimation(slide_down);
        } else if (ll_05_contentLayer_feedback.getVisibility() == View.VISIBLE) {
            ll_05_contentLayer_feedback.startAnimation(slide_down);
        }*/

        ll_01_contentLayer_show.setVisibility(View.GONE);
        ll_02_contentLayer_start_ride.setVisibility(View.GONE);
        /*ll_03_contentLayer_service_flow.setVisibility(View.GONE);
        ll_04_contentLayer_payment.setVisibility(View.GONE);
        ll_05_contentLayer_feedback.setVisibility(View.GONE);*/
        changeOnOffLayout.setVisibility(View.GONE);
        changeOnOffLayout.setVisibility(View.GONE);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        try {
            // Customise the styling of the BASE_URL map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            mContext, R.raw.style_json));

            if (!success) {
                Log.e("Map:Style", "Style parsing failed.");
            } else {
                Log.e("Map:Style", "Style Applied.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("Map:Style", "Can't find style. Error: ", e);
        }
        // setUserLocation();
        googleMap.setPadding(30, 0, 10, 10);

        if (isLocationEnabled(mContext)) {
            manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            if (latitudeLng != null) {
                Log.e("TAG", "GPS is on");

                googleMap.addMarker(new MarkerOptions().position(latitudeLng).title("Your Current Location"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latitudeLng, 14));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            }
        } else {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        setupMap();

        // googleMap.setMyLocationEnabled(false);

    }


    @SuppressLint("MissingPermission")
    private void setCurrentLocation(final LatLng latLng) {
        if (myMarker != null) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
            Location newLoc = new Location("new_location");
            newLoc.setLatitude(latLng.latitude);
            newLoc.setLongitude(latLng.longitude);

            Location oldLocation = new Location("old_location");
            oldLocation.setLatitude(myMarker.getPosition().latitude);
            oldLocation.setLongitude(myMarker.getPosition().longitude);

            float bearing = oldLocation.bearingTo(newLoc);
            Log.e("bearing", "--" + bearing);

            final double distance = oldLocation.distanceTo(newLoc);
            Log.e("Distance--", ":" + distance);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));

            if (/*bearing != 0.0*/distance > 3) {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
                //animateCar(latLng, bearing);
            } else {
                myMarker.setRotation(myMarker.getRotation());
            }
            boolean contains = googleMap.getProjection().getVisibleRegion().latLngBounds.contains(latLng);
            if (!contains) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            }


        } else {
            myMarker = googleMap.addMarker(new MarkerOptions().position(latLng).title("Current Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon)));
            myMarker.setFlat(true);

        }

    }


    @Override
    public void onResume() {
        supportMapFragment.onLowMemory();
        mContext.registerReceiver(locationReciever, location_filter);
        super.onResume();
        if (verifyAccountDoc == 0) {
            DOCUMENT_FRAGMENT();
        } else if (loginPreference.getBookingType().equals(Constants.BOOKING_ACCEPT)){
           /* if (ll_01_contentLayer_show.getVisibility() == View.GONE) {
                ll_01_contentLayer_show.startAnimation(slide_up);
            }
            ll_01_contentLayer_show.setVisibility(View.VISIBLE);*/
            getBookingDetailsApi(loginPreference.getBookingId());

        } else if (loginPreference.getBookingType().equals(Constants.START_TRIP)){
            StartTripRequest startTripRequest = new StartTripRequest();
            startTripRequest.setBooking_id(loginPreference.getBookingId());
            startTripRequest.setDriver_id(driverId);
            startTripRequest.setStart_time(CommonMethod.getCurrentTime());
            startTripRequest(startTripRequest);

        } else if (loginPreference.getBookingType().equals(Constants.REACHED_LOCATION)){
            AcceptBookingRequest reachLocationRequest = new AcceptBookingRequest();
            reachLocationRequest.setBooking_id(loginPreference.getBookingId());
            reachLocationRequest.setDriver_id(driverId);
            reactLocationRequest(reachLocationRequest);
        }
    }

    @Override
    public void onStop() {
        try {
            if (locationReciever != null) {
                mContext.unregisterReceiver(locationReciever);
                locationReciever = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onStop();
    }


    @Override
    public void onLowMemory() {
        supportMapFragment.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onPause() {
        supportMapFragment.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        //supportMapFragment.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onCameraMove() {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.moneyTextView:
                //  UserRatingFragment fragment = new UserRatingFragment();
                // CallFragment(fragment);
                showCustomMoneyDialog(v);
                break;

            case R.id.go_online_offline:
                goOnlineOfflineApi(driverId);
                break;

            case R.id.go_offline_img:
                Log.e(this.getClass().getName(), "ID" + driverId);
                goOnlineOfflineApi(driverId);
                break;
            case R.id.search_icon:
                CommonMethod.callOnlyActivity(getActivity(), NewGooglePlaceSearch.class);
                break;

            case R.id.driver_icon_profile:
                CommonMethod.callOnlyActivity(getActivity(), Profile_Activity.class);
                break;
            case R.id.btn_02_accept:

                StartTripRequest startTripRequest = new StartTripRequest();
                startTripRequest.setBooking_id(loginPreference.getBookingId());
                startTripRequest.setDriver_id(driverId);
                Log.e(this.getClass().getName(),"CURRENT_TIME" + CommonMethod.getCurrentTime());
                startTripRequest.setStart_time(CommonMethod.getCurrentTime());
                startTripRequest(startTripRequest);
                break;
            case R.id.btn_01_reachLocation:
                AcceptBookingRequest reachLocationRequest = new AcceptBookingRequest();
                reachLocationRequest.setBooking_id(loginPreference.getBookingId());
                reachLocationRequest.setDriver_id(driverId);
                reactLocationRequest(reachLocationRequest);

                break;

            case R.id.btn_04_endTrip:

                EndTripRequest endTripRequest = new EndTripRequest();
                endTripRequest.setBooking_id(loginPreference.getBookingId());
                endTripRequest.setDriver_id(driverId);
                endTripRequest.setDistance(10);
                endTripRequest.setEnd_time(CommonMethod.getCurrentTime());
                callEndTripRequestApi(endTripRequest);

                break;

        }

    }



    private void goOnlineOfflineApi(int userId) {
        myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));

        bookingViewModels.postOfflineResponse(userId).observe(this, offlineResponse -> {
            myDialog.dismiss();
            if (!TextUtils.isEmpty(offlineResponse.getMessage())) {
                if (offlineResponse.getResult() != null) {
                    int result = offlineResponse.getResult();
                    loginPreference.setOfflineResult_id(offlineResponse.getResult());
                    if (result == 0) {
                        go_online_offline.setVisibility(View.GONE);
                        go_offline_img.setVisibility(View.VISIBLE);
                        txtOfflineOnline.setText(getString(R.string.you_are_offline_now));
                        CommonMethod.showToast(mContext, getString(R.string.you_are_offline_now));

                    } else {
                        go_online_offline.setVisibility(View.VISIBLE);
                        go_offline_img.setVisibility(View.GONE);
                        txtOfflineOnline.setText(getString(R.string.you_are_online_now));
                        CommonMethod.showToast(mContext, getString(R.string.you_are_online_now));

                    }
                } else {
                    CommonMethod.showAlert(offlineResponse.getMessage(), getActivity());

                }
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
            }
        });


    }


    private void showCustomMoneyDialog(View views) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = views.findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_home_money, viewGroup, false);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        alertDialog = builder.create();
        Objects.requireNonNull(alertDialog.getWindow()).setGravity(Gravity.TOP);
        TextView checkAccountStatusText = dialogView.findViewById(R.id.totalAmountTextView);
        checkAccountStatusText.setOnClickListener(v -> {
            alertDialog.dismiss();
            CommonMethod.setSimpleFragment(new PaymentHistory_fragment(), getActivity(), R.id.mapFrameLayout);

        });

        TextView moneyTextView = dialogView.findViewById(R.id.moneyTextView);
        moneyTextView.setOnClickListener(v -> {
            alertDialog.dismiss();
            CommonMethod.setSimpleFragment(new PaymentHistory_fragment(), getActivity(), R.id.mapFrameLayout);
        });

        TextView contactUsTextView = dialogView.findViewById(R.id.zeroTripCompletedTextView);
        contactUsTextView.setOnClickListener(v -> {
            alertDialog.dismiss();
            CommonMethod.setSimpleFragment(new PaymentHistory_fragment(), getActivity(), R.id.mapFrameLayout);


        });

        alertDialog.show();
    }


    private void showPickupRequest() {
        AppData.getInstance().setFromOutSide(false);
        // CommonMethod.clearNotifications(MainActivity.activity);
        ViewGroup viewGroup = view.findViewById(android.R.id.content);
        final View dialogView = LayoutInflater.from(mContext).inflate(R.layout.pickup_request_dialog, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);

        final TextView name;
        int bookingId = notificationData.id;
        loginPreference.setBookingId(bookingId);

        ImageView image = dialogView.findViewById(R.id.image);
        name = dialogView.findViewById(R.id.name);
        // time = dialogView.findViewById(R.id.time);
        // reach_time = dialogView.findViewById(R.id.reach_time);
        // RatingBar rating = dialogView.findViewById(R.id.rating);


        /*if (!TextUtils.isEmpty(notificationData.rating)) {
            float rateNum = Float.valueOf(notificationData.rating);
            rating.setRating(rateNum);
        }*/

        String userImg = notificationData.user_image.trim();
        if (!userImg.contains("https:")) {
            userImg = Constants.IMAGE_URL + notificationData.user_image.trim();
        }
        CommonMethod.loadImageResized(getActivity(), userImg, image);
        name.setText(notificationData.user_name);
        dialogView.findViewById(R.id.accept).setOnClickListener(v -> {
            AppData.getInstance().setIsOngoing(false);
            alertDialog.dismiss();
            int bookingIdVal = notificationData.id;
            Log.e(this.getClass().getName(), "Booking_ID " + bookingId + " " + bookingIdVal);
            AcceptBookingRequest acceptBookingRequest = new AcceptBookingRequest();
            acceptBookingRequest.setBooking_id(bookingId);
            acceptBookingRequest.setDriver_id(driverId);
            acceptBooking(acceptBookingRequest);
        });

        dialogView.findViewById(R.id.reject).setOnClickListener(v -> alertDialog.dismiss());

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            try {
                alertDialog.dismiss();
                CommonMethod.clearNotifications(getActivityNonNull());
                AppData.getInstance().setIsOngoing(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, Constants.NOTIFICATION_CANCEL_TIME);


        double currentLat, currentLong;
        if (AppData.getInstance().getLatLng() != null) {
            currentLat = /*SendLocationService.currentLatitude*/AppData.getInstance().getLatLng().latitude;
            currentLong = /*SendLocationService.currentLongitude*/AppData.getInstance().getLatLng().latitude;
        } else {
            currentLat = Double.parseDouble(loginPreference.getLatitude());
            currentLong = Double.parseDouble(loginPreference.getLongitude());
        }


        if (!TextUtils.isEmpty(notificationData.sLat)) {
            final double destinationLat = Double.parseDouble(notificationData.sLat);
            final double destinationLang = Double.parseDouble(notificationData.sLng);

            final double finalCurrentLat = currentLat;
            final double finalCurrentLong = currentLong;
            new Thread(() -> CommonMethod.getLocationDistance("Home", new com.google.maps.model.LatLng(finalCurrentLat, finalCurrentLong),
                    new com.google.maps.model.LatLng(destinationLat, destinationLang))).start();
        }
    }

    private void acceptBooking(AcceptBookingRequest acceptBookingRequest) {
        myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));

        bookingViewModels.postAcceptRequestViewModel(acceptBookingRequest).observe(this, acceptBookingResponse -> {
            myDialog.dismiss();
            if (acceptBookingResponse != null) {
                Log.e(this.getClass().getName(), "Accept_booking_request" + acceptBookingResponse );
                Log.e(this.getClass().getName(), "USER_ID_VAL" + acceptBookingResponse.getBookingDetail().getUserId() + "");
                loginPreference.setBookingType(Constants.BOOKING_ACCEPT);
                AppData.getInstance().setFROM("");

                    loginPreference.setUSER_REG_ID(acceptBookingResponse.getBookingDetail().getUserId());

                CommonMethod.showToast(getActivity(), acceptBookingResponse.getMessage());
                if (ll_01_contentLayer_show.getVisibility() == View.GONE) {
                    ll_01_contentLayer_show.startAnimation(slide_up);
                }
                ll_01_contentLayer_show.setVisibility(View.VISIBLE);

                CommonMethod.loadImageResized(getActivity(), Constants.IMAGE_URL
                        + acceptBookingResponse.getBookingDetail().getUsers().getUserImage(),img01UserImg);
                txt01UserName.setText(acceptBookingResponse.getBookingDetail().getUsers().getName());
                sourceAddressText.setText(acceptBookingResponse.getBookingDetail().getSource());
                currentTimeText.setText(acceptBookingResponse.getBookingDetail().getTotalTime());
                kmMeterText.setText(acceptBookingResponse.getBookingDetail().getDistance());


            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());

            }
        });
    }

    private void startTripRequest(StartTripRequest startTripRequest){
        myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        bookingViewModels.postStartTripRequestViewModel( startTripRequest).observe(this, acceptBookingResponse -> {
            myDialog.dismiss();
            Log.e(this.getClass().getName(),"RESPONSE_START_TRIP" + acceptBookingResponse.getError());
            if (acceptBookingResponse.getError() != null && !acceptBookingResponse.getError()){
                loginPreference.setBookingType(Constants.START_TRIP);
                CommonMethod.showToast(getActivity(), acceptBookingResponse.getMessage());
                CommonMethod.setSimpleFragment(new SecurityEnterPasswordFragment(),getActivity(), R.id.fragment_container);
            } else if (acceptBookingResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(),acceptBookingResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection),getActivity());
            }
        });
    }

    private void reactLocationRequest(AcceptBookingRequest acceptBookingRequest){
        myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        bookingViewModels.postReactLocationViewModel(acceptBookingRequest).observe(this, reachLocationResponse -> {
            myDialog.dismiss();
            if (reachLocationResponse.getError() != null && !reachLocationResponse.getError()){
                CommonMethod.showToast(getActivity(), reachLocationResponse.getMessage());
                loginPreference.setBookingType(Constants.REACHED_LOCATION);
                if (ll_01_contentLayer_show.getVisibility() == View.VISIBLE) {
                    ll_01_contentLayer_show.startAnimation(slide_down);
                    ll_01_contentLayer_show.setVisibility(View.GONE);
                }
                if (ll_02_contentLayer_start_ride.getVisibility() == View.GONE) {
                    ll_02_contentLayer_start_ride.startAnimation(slide_up);
                }
                ll_02_contentLayer_start_ride.setVisibility(View.VISIBLE);
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
            }
        });

    }


    private void callEndTripRequestApi(EndTripRequest endTripRequest) {
        bookingViewModels.postEndTripViewModel(endTripRequest).observe(this, endTripResponse -> {
            if (endTripResponse.getError() != null && !endTripResponse.getError()){
                loginPreference.setBookingType(Constants.START_TRIP);

                CommonMethod.setSimpleFragment(new UserRatingFragment(), getActivityNonNull() ,R.id.mapFrameLayout);

            } else if (endTripResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(), endTripResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivityNonNull());
            }

        });
    }

    private void getBookingDetailsApi(int bookingId){
        bookingViewModels.getBookingDetailsViewModel(bookingId).observe(this, bookingDetailsResponse -> {
            if (bookingDetailsResponse.getError() != null && !bookingDetailsResponse.getError()){
                Log.e(this.getClass().getName(),"GetDetailsBooking" + bookingDetailsResponse.getResult());
                loginPreference.setUSER_REG_ID(bookingDetailsResponse.getResult().getUsers().getId());

                if (ll_01_contentLayer_show.getVisibility() == View.GONE) {
                    ll_01_contentLayer_show.startAnimation(slide_up);
                }
                ll_01_contentLayer_show.setVisibility(View.VISIBLE);
                CommonMethod.loadImageResized(getActivity(), Constants.IMAGE_URL
                        + bookingDetailsResponse.getResult().getUsers().getUserImage(),img01UserImg);
                txt01UserName.setText(bookingDetailsResponse.getResult().getUsers().getName());
                sourceAddressText.setText(bookingDetailsResponse.getResult().getSource());
                currentTimeText.setText(bookingDetailsResponse.getResult().getTotalTime());
                kmMeterText.setText(bookingDetailsResponse.getResult().getDistance());

            }
        });
    }


    private void DOCUMENT_FRAGMENT() {
        Document_Fragment document_fragment = new Document_Fragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.HOME_SCREEN, "first_Signup");
        CommonMethod.setSimpleFragment(document_fragment, getActivityNonNull(), R.id.fragment_container);
        document_fragment.setArguments(bundle);
    }

    private FragmentActivity getActivityNonNull() {
        if (super.getActivity() != null) {
            return super.getActivity();
        } else {
            throw new RuntimeException("null returned from getActivity()");
        }
    }


}
