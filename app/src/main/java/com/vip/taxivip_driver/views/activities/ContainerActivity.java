package com.vip.taxivip_driver.views.activities;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.vip.taxivip_driver.utils.Constants;

import com.vip.taxivip_driver.views.fragments.documents.Document_Fragment;
import com.vip.taxivip_driver.views.fragments.home_view.HomeFragment;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;

public class ContainerActivity extends AppCompatActivity {
    FrameLayout container;
    String type;
    private LoginPreference loginPreference;
    private int verifyAccountDoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_container_);
        Initialise();

        // HomeFragment homeFragment = new HomeFragment();
        //CallFragment(homeFragment);
        CommonMethod.setSimpleFragment(new HomeFragment(), ContainerActivity.this, R.id.fragment_container);


    }

    private void Initialise() {
        loginPreference = new LoginPreference(this);
        container = findViewById(R.id.fragment_container);
        verifyAccountDoc = loginPreference.getVerifyDoc();
    }


    @Override
    protected void onResume() {
        super.onResume();
        getIntentData();
    }

    private void DOCUMENT_FRAGMENT() {
        Document_Fragment document_fragment = new Document_Fragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.HOME_SCREEN, "first_Signup");
        //CallFragment(document_fragment);
        CommonMethod.setSimpleFragment(document_fragment, ContainerActivity.this, R.id.fragment_container);
        document_fragment.setArguments(bundle);
    }


    private void getIntentData() {
        if (getIntent() != null) {
            type = getIntent().getStringExtra(Constants.TYPE);
            if (type != null) {
                if (Constants.DOCUMENT.equals(type)) {
                    DOCUMENT_FRAGMENT();
                }
            }
            if (verifyAccountDoc == 0) {
                DOCUMENT_FRAGMENT();
            }
        }

    }

    /*public void CallFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }*/


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
            /*if(getFragmentManager().getBackStackEntryCount() > 0){
                //super.onBackPressed();
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }*/
    }


}
