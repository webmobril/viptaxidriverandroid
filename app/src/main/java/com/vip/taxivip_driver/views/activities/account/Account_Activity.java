package com.vip.taxivip_driver.views.activities.account;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.activities.profile.Profile_Activity;
import com.vip.taxivip_driver.views.activities.welcom.WelcomeActivity;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.views.fragments.accounts_.About_fragment;
import com.vip.taxivip_driver.views.fragments.accounts_.ContactUs_fragment;
import com.vip.taxivip_driver.views.fragments.accounts_.Security_fragment;
import com.vip.taxivip_driver.views.fragments.documents.Document_Fragment;
import com.vip.taxivip_driver.views.fragments.documents.VehicleModel_fragment;
import com.vip.taxivip_driver.views.fragments.payments.Payment_fragment;
import com.vip.taxivip_driver.views.fragments.profiles_fragments.EditInfo_fragment;
import com.vip.taxivip_driver.views.fragments.signup_views.ResetPasswordFragment;
import com.vip.taxivip_driver.services.MyLocationService;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.AccountViewModel;


public class Account_Activity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout nav_documents, nav_payment, nav_edit_info,
            nav_about, nav_security, nav_vehicles, nav_contact, nav_logout, nav_resetPassword;
    Fragment fragment = null;
    private TextView tvTitleToolbar, vehicleTypeValue;
    private ImageView iv_Back;
    private LoginPreference loginPreference;
    private int userId;
    private AccountViewModel accountViewModel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_fragment_layout);
        loginPreference = new LoginPreference(getApplicationContext());
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);
        userId = loginPreference.getRegister_id();
        initViews();
    }


    private void initViews() {
        iv_Back = findViewById(R.id.iv_Back);
        tvTitleToolbar = findViewById(R.id.tvTitleToolbar);
        vehicleTypeValue = findViewById(R.id.vehicleTypeValue);
        nav_documents = findViewById(R.id.nav_documents);
        nav_payment = findViewById(R.id.nav_payment);
        nav_edit_info = findViewById(R.id.nav_edit_info);
        nav_about = findViewById(R.id.nav_about);
        nav_security = findViewById(R.id.nav_security);
        nav_contact = findViewById(R.id.nav_contact);
        nav_logout = findViewById(R.id.nav_logout);
        nav_vehicles = findViewById(R.id.nav_vehicles);
        nav_resetPassword = findViewById(R.id.nav_resetPassword);
        tvTitleToolbar.setText(getString(R.string.account));

        //OnClick Listener
        iv_Back.setOnClickListener(this);
        nav_vehicles.setOnClickListener(this);
        nav_documents.setOnClickListener(this);
        nav_payment.setOnClickListener(this);
        nav_edit_info.setOnClickListener(this);
        nav_about.setOnClickListener(this);
        nav_security.setOnClickListener(this);
        nav_contact.setOnClickListener(this);
        nav_logout.setOnClickListener(this);
        nav_resetPassword.setOnClickListener(this);

        if (loginPreference.getVehicleType() != null && !loginPreference.getVehicleType().equals("")) {
            vehicleTypeValue.setText(loginPreference.getVehicleType());
        }

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.nav_logout) {
            LogoutDialog(userId);

        } else if (v.getId() == R.id.nav_vehicles) {
            // fragment = new Add_vehicle_fragment();
            //fragment = new VehicleModel_fragment();
            // CallFragment(fragment);
            CommonMethod.setSimpleFragment(new VehicleModel_fragment(), Account_Activity.this, R.id.frame_about);


        } else if (v.getId() == R.id.nav_documents) {
            fragment = new Document_Fragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.HOME_SCREEN, "account_screen");
            CommonMethod.setSimpleFragment(fragment, Account_Activity.this, R.id.frame_about);
            fragment.setArguments(bundle);


        } else if (v.getId() == R.id.nav_payment) {
            CommonMethod.setSimpleFragment(new Payment_fragment(), Account_Activity.this, R.id.frame_about);


        } else if (v.getId() == R.id.nav_edit_info) {
            CommonMethod.setSimpleFragment(new EditInfo_fragment(), Account_Activity.this, R.id.frame_about);


        } else if (v.getId() == R.id.nav_about) {
            CommonMethod.setSimpleFragment(new About_fragment(), Account_Activity.this, R.id.frame_about);


        } else if (v.getId() == R.id.nav_security) {
            CommonMethod.setSimpleFragment(new Security_fragment(), Account_Activity.this, R.id.frame_about);


        } else if (v.getId() == R.id.nav_contact) {
            CommonMethod.setSimpleFragment(new ContactUs_fragment(), Account_Activity.this, R.id.frame_about);

        } else if (v.getId() == R.id.nav_resetPassword) {
            CommonMethod.setSimpleFragment(new ResetPasswordFragment(), Account_Activity.this, R.id.frame_about);


        } else if (v.getId() == R.id.iv_Back) {
            CommonMethod.callActivity(this, Profile_Activity.class);
        }

    }

    private void LogoutDialog(int id) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, choice) -> {
            switch (choice) {
                case DialogInterface.BUTTON_POSITIVE:
                    try {
                        //if (CommonMethod.isNetworkAvailable(getApplicationContext())) {
                            logoutApi(id);
                        /*} else {
                            CommonMethod.showAlert(getString(R.string.internet_connection), this);
                        }*/
                    } catch (Exception ioe) {
                        ioe.printStackTrace();
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(Account_Activity.this);
        builder.setMessage(getString(R.string.are_you_sure_logout))
                .setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }


    private void logoutApi(int userId) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(this, getString(R.string.please_wait));
        accountViewModel.putLogOutResponse(userId).observe(this, logoutResponse -> {
            myDialog.dismiss();
            if (logoutResponse.getError() != null && !logoutResponse.getError()) {
                if (CommonMethod.isMyServiceRunning(Account_Activity.this, MyLocationService.class)) {
                    stopService(new Intent(Account_Activity.this, MyLocationService.class));
                }
                CommonMethod.showToast(Account_Activity.this, logoutResponse.getMessage());
                loginPreference.clearSharedPreferences(Account_Activity.this);
                new Handler().postDelayed(() -> CommonMethod.callActivity(this, WelcomeActivity.class), 200);
            } else if (logoutResponse.getMessage() != null){
                CommonMethod.showAlert(logoutResponse.getMessage(), Account_Activity.this);
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), this);

            }

        });
    }

    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }


}
