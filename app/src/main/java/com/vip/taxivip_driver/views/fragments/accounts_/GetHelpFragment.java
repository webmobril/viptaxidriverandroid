package com.vip.taxivip_driver.views.fragments.accounts_;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.ContactAdminRequest;
import com.vip.taxivip_driver.views.activities.account.Account_Activity;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.AccountViewModel;

import java.util.Objects;


public class GetHelpFragment extends Fragment implements View.OnClickListener {

    private EditText ed_name, ed_subject, ed_message;
    private Button btn_send;
    private AccountViewModel accountViewModel;
    private LoginPreference loginPreference;
    private int driverId;
    private Context mContext;
    private ImageView iv_Back;

    public GetHelpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_get_help, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        ed_name = view.findViewById(R.id.ed_name);
        ed_subject = view.findViewById(R.id.ed_subject);
        ed_message = view.findViewById(R.id.ed_message);
        btn_send = view.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);
        iv_Back.setOnClickListener(this);

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_send) {
            String nameVal = ed_name.getText().toString();
            String subjectVal = ed_subject.getText().toString();
            String messageVal = ed_message.getText().toString();
                if (isValidated(nameVal, subjectVal, messageVal)) {
                    ContactAdminRequest contactAdminRequest = new ContactAdminRequest();
                    contactAdminRequest.setUser_id(driverId);
                    contactAdminRequest.setName(nameVal);
                    contactAdminRequest.setSubject(subjectVal);
                    contactAdminRequest.setMessage(messageVal);
                    getHelpApi(contactAdminRequest);
                }
        } else if (v.getId() == R.id.iv_Back) {
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
    }


    private void getHelpApi(ContactAdminRequest contactAdminRequest) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        accountViewModel.getHelpResponseMutableLiveData(contactAdminRequest).observe(this, getHelpResponse -> {
            myDialog.dismiss();
            if (getHelpResponse.getError() != null && !getHelpResponse.getError()) {
                CommonMethod.showToast(mContext, getHelpResponse.getMessage());
                new Handler().postDelayed(() -> CommonMethod.callActivity(mContext, Account_Activity.class),100);
            } else if (getHelpResponse.getMessage() != null){
                CommonMethod.showToast(mContext, getHelpResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());

            }
        });
    }


    private boolean isValidated(String userName, String subject, String message) {
        if (TextUtils.isEmpty(userName)) {
            ed_name.requestFocus();
            ed_name.setError(getString(R.string.please_enter_name));
            return false;
        } else if (TextUtils.isEmpty(subject)) {
            ed_subject.requestFocus();
            ed_subject.setError(getString(R.string.please_enter_subject));
            return false;
        } else if (TextUtils.isEmpty(message)) {
            ed_message.requestFocus();
            ed_message.setError(getString(R.string.please_enter_message));
            return false;
        }
        return true;
    }

}
