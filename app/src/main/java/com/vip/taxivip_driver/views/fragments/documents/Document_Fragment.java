package com.vip.taxivip_driver.views.fragments.documents;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.activities.welcom.WelcomeActivity;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.views.fragments.accounts_.GetHelpFragment;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.AccountViewModel;


public class Document_Fragment extends Fragment implements View.OnClickListener {
    private TextView tvTitleToolbar;
    private TextView txt_driver_pancard_image, txt_driver_license_image,
            txt_vehicle_permit_image, txt_criminal_record_image, txt_police_record,
            vehicle_certificate_txt, txt_car_insurance_image;
    private Button btn_continue;
    private ImageView iv_Back, iv_document_icon;
    private LoginPreference loginPreference;
    private int driverId;
    private AccountViewModel accountViewModel;
    private AlertDialog alertDialog;
    private String frameType;
    private View views;
    private Context mContext;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
        Bundle bundle = getArguments();
        if (bundle != null) {
            frameType = bundle.getString(Constants.HOME_SCREEN);
            Log.e(this.getClass().getName(), "FRAME_TYPE" + frameType);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        views = inflater.inflate(R.layout.document_fragment, container, false);
        initViews(views);
        onClickListner();
        return views;
    }


    private void initViews(View view) {


        iv_Back = view.findViewById(R.id.iv_Back);
        iv_document_icon = view.findViewById(R.id.iv_document_icon);
        iv_document_icon.setVisibility(View.VISIBLE);
        tvTitleToolbar = view.findViewById(R.id.tvTitleToolbar);
        tvTitleToolbar.setText(R.string.documents);
        txt_driver_pancard_image = view.findViewById(R.id.txt_driver_pancard_image);
        vehicle_certificate_txt = view.findViewById(R.id.vehicle_certificate_txt);
        txt_driver_license_image = view.findViewById(R.id.txt_driver_license_image);
        txt_vehicle_permit_image = view.findViewById(R.id.txt_vehicle_permit_image);
        txt_criminal_record_image = view.findViewById(R.id.txt_criminal_record_image);
        txt_police_record = view.findViewById(R.id.txt_police_record);
        txt_car_insurance_image = view.findViewById(R.id.txt_car_insurance_image);
        btn_continue = view.findViewById(R.id.btn_continue);
    }

    private void onClickListner() {
        vehicle_certificate_txt.setOnClickListener(this);
        btn_continue.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
        iv_document_icon.setOnClickListener(this);
        txt_car_insurance_image.setOnClickListener(this);
        txt_driver_pancard_image.setOnClickListener(this);
        txt_driver_license_image.setOnClickListener(this);
        txt_vehicle_permit_image.setOnClickListener(this);
        txt_criminal_record_image.setOnClickListener(this);
        txt_police_record.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txt_driver_license_image) {
            callActivityPhoto("driver_licence_", frameType);

        } else if (v.getId() == R.id.vehicle_certificate_txt) {

            callActivityPhoto("driver_vehicle_certificate", frameType);

        } else if (v.getId() == R.id.txt_vehicle_permit_image) {

            callActivityPhoto("driver_vehicle_permit", frameType);

        } else if (v.getId() == R.id.txt_criminal_record_image) {
            callActivityPhoto("driver_criminal_record_", frameType);

        } else if (v.getId() == R.id.txt_car_insurance_image) {
            callActivityPhoto("driver_car_insurance", frameType);
        } else if (v.getId() == R.id.txt_police_record) {
            callActivityPhoto("police_record", frameType);

        } else if (v.getId() == R.id.iv_document_icon) {
            showCustomDialog();
        } else if (v.getId() == R.id.btn_continue) {


            VehicleModel_fragment vehicleModel_fragment = new VehicleModel_fragment();
            Bundle args = new Bundle();
            args.putString(Constants.HOME_SCREEN, frameType);
            vehicleModel_fragment.setArguments(args);
            CallFragment(vehicleModel_fragment);
        } else if (v.getId() == R.id.iv_Back) {
            if (frameType.equals("first_Signup")) {
                CommonMethod.exitPopup(getActivity());
            } else {
                getActivity().onBackPressed();
            }


        }

    }


    private void callActivityPhoto(String docType, String screenOpen) {
        Intent intent = new Intent(getActivity(), TakePhotoActivity.class);
        intent.putExtra(Constants.DRIVER_DOCUMENTS_IMG, docType);
        intent.putExtra(Constants.HOME_SCREEN, screenOpen);
        startActivity(intent);
    }


    private void CallFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (frameType != null && frameType.equals("first_Signup")) {
            fragmentTransaction.replace(R.id.fragment_container, fragment).addToBackStack(null);
        } else {
            fragmentTransaction.replace(R.id.frame_about, fragment).addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = views.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_help, viewGroup, false);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        alertDialog = builder.create();

        ImageView img_close = dialogView.findViewById(R.id.img_close);
        img_close.setOnClickListener(v -> alertDialog.dismiss());

        TextView checkAccountStatusText = dialogView.findViewById(R.id.checkAccountStatusText);
        checkAccountStatusText.setOnClickListener(v -> {

            alertDialog.dismiss();
            DocumentAccountSatusFragment fragment = new DocumentAccountSatusFragment();
            Bundle args = new Bundle();
            args.putString(Constants.HOME_SCREEN, frameType);
            fragment.setArguments(args);
            CallFragment(fragment);
        });

        TextView contactUsTextView = dialogView.findViewById(R.id.contactUsTextView);
        contactUsTextView.setOnClickListener(v -> {

            alertDialog.dismiss();
            //GetHelpFragment fragment = new GetHelpFragment();
            CallFragment(new GetHelpFragment());
        });


        Button btn_logout = dialogView.findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(v -> {
            // alertDialog.dismiss();
            LogoutDialog(driverId);
        });
        alertDialog.show();
    }

    private void LogoutDialog(int driverIdVal) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, choice) -> {
            switch (choice) {
                case DialogInterface.BUTTON_POSITIVE:

                    //if (CommonMethod.isNetworkAvailable(mContext)) {
                    alertDialog.dismiss();
                    logoutApi(driverIdVal);
                   /* } else {
                        CommonMethod.showToast(getActivity(), getActivity().getString(R.string.internet_connection));

                    }*/

                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(getString(R.string.are_you_sure_logout))
                .setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }


    private void logoutApi(int userId) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        accountViewModel.putLogOutResponse(userId).observe(this, logoutResponse -> {
            myDialog.dismiss();
            alertDialog.dismiss();
            if (logoutResponse.getError() != null && !logoutResponse.getError()) {
                CommonMethod.showToast(getActivity(), logoutResponse.getMessage());
                loginPreference.clearSharedPreferences(getActivity());
                new Handler().postDelayed(() ->
                        CommonMethod.callActivity(getActivity(), WelcomeActivity.class), 1000);
            } else if (logoutResponse.getMessage() != null) {
                CommonMethod.showToast(getActivity(), logoutResponse.getMessage());
            } else {
                CommonMethod.showToast(getActivity(), getActivity().getString(R.string.internet_connection));
            }

        });
    }


}

