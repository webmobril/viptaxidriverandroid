package com.vip.taxivip_driver.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.interfaces.GetAddress;
import com.vip.taxivip_driver.models.SearchAddressModel;

import java.util.ArrayList;


public class SearchedAddressAdapter extends RecyclerView.Adapter<SearchedAddressAdapter.ViewHolder> {

    public ArrayList<SearchAddressModel> android;
    private Context context;
    GetAddress getMyItem;

    public SearchedAddressAdapter(FragmentActivity activity, ArrayList<SearchAddressModel> servicesModelArrayList, GetAddress getMyItem) {
        this.android = servicesModelArrayList;
        this.context = activity;
        this.getMyItem = getMyItem;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_show_address, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int i) {

        holder.text1.setText(android.get(i).getAddress());
        holder.itemView.setOnClickListener(view -> getMyItem.onClickChange(android.get(i).getAddress()));


    }

    @Override
    public int getItemCount() {
        return android.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text1;
        public CheckBox checkBox;


        public ViewHolder(View view) {
            super(view);
            text1 = view.findViewById(R.id.text1);

        }
    }
}

