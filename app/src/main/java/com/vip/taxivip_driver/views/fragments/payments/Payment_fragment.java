package com.vip.taxivip_driver.views.fragments.payments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.CommonMethod;

public class Payment_fragment extends Fragment implements View.OnClickListener {
    private TextView tvTitleToolbar;
    private ImageView iv_Back;

    public Payment_fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_fragment, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        TextView payment = view.findViewById(R.id.addPaymentMethodTxt);
        TextView txt_payment = view.findViewById(R.id.txt_payment);
        iv_Back = view.findViewById(R.id.iv_Back);
        tvTitleToolbar = view.findViewById(R.id.tvTitleToolbar);
        tvTitleToolbar.setText(R.string.payment);
        iv_Back.setOnClickListener(this);
        payment.setOnClickListener(this);

        txt_payment.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_Back) {
            getActivity().onBackPressed();

        } else if (v.getId() == R.id.addPaymentMethodTxt) {
            CommonMethod.setSimpleFragment(new Add_bank_account(), getActivity(), R.id.frame_about);
        } else if (v.getId() == R.id.txt_payment) {
            CommonMethod.setSimpleFragment(new Payment_method_fragment(), getActivity(), R.id.frame_about);


        }
    }

}
