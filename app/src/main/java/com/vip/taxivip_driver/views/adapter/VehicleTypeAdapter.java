package com.vip.taxivip_driver.views.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.views.fragments.documents.VehicleTypeFragment;
import com.vip.taxivip_driver.models.VehicleTypeResponse;
import com.vip.taxivip_driver.utils.CommonMethod;

import java.util.List;

public class VehicleTypeAdapter extends RecyclerView.Adapter<VehicleTypeAdapter.VehicleTypeViewHolder> {

    private List<VehicleTypeResponse.Result> vehicleTypeResult;
    private Context mContext;
    int index = -1;
    public int mSelectedItem = -1;
    MyclickListnerLearning myAdapterlistener;
    private String vehicleType;

    public interface MyclickListnerLearning  {
        void onViewClick( String nameType, int vehcileTypeId);
    }

    public  VehicleTypeAdapter(Context mContext, List<VehicleTypeResponse.Result> vehicleTypeResult, VehicleTypeFragment myAdapterlistener, String vehicleType){
        this.mContext= mContext;
        this.vehicleTypeResult = vehicleTypeResult;
        this.myAdapterlistener = myAdapterlistener;
        this.vehicleType = vehicleType;
    }

    @NonNull
    @Override
    public VehicleTypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_type_adapter_item,parent,false);
        return new VehicleTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VehicleTypeViewHolder holder, int position) {

        if (vehicleTypeResult.size() >0){
            holder.vehicleTypeTxt.setText(vehicleTypeResult.get(position).getName());
            CommonMethod.loadImageCarType(mContext, Constants.IMAGE_URL + vehicleTypeResult.get(position).getIcon(),holder.carTypeIcon);


        }
        Log.e("TAG", "ADAPTER_VEHICLE_TYPE" + vehicleType);
        /*if (vehicleType.equals(vehicleTypeResult.get(position).getName())){
            holder.backgroundRelative.setBackgroundColor(Color.parseColor("#808080"));

        }*/
        vehicleTypeResult.get(position).setChecked(position== mSelectedItem);

        holder.backgroundRelative.setOnClickListener(v -> {
            index = position;
            myAdapterlistener.onViewClick(vehicleTypeResult.get(position).getName() , vehicleTypeResult.get(position).getId());
            notifyDataSetChanged();

           /* if(index==position){
                holder.backgroundRelative.setBackgroundColor(Color.parseColor("#FFEB3B"));
                holder.tv.setTextColor(Color.parseColor("#ffffff"));
            }
            else
            {
                holder.linearlayout.setBackgroundColor(Color.parseColor("#ffffff"));
                holder.tv.setTextColor(Color.parseColor("#6200EA"));
            }*/


        });

         if(index==position){
                holder.backgroundRelative.setBackgroundColor(Color.parseColor("#808080"));

            }else{
                holder.backgroundRelative.setBackgroundColor(Color.parseColor("#ffffff"));
            }



        if (vehicleType.equals(vehicleTypeResult.get(position).getName())){
           // myAdapterlistener.onViewClick(vehicleTypeResult.get(position).getName(),vehicleTypeResult.get(position).getId());
            holder.backgroundRelative.setBackgroundColor(Color.parseColor("#808080"));

        }

    }

    @Override
    public int getItemCount() {
        return vehicleTypeResult.size();
    }

    public class VehicleTypeViewHolder extends RecyclerView.ViewHolder {
        private TextView vehicleTypeTxt;
        private ImageView carTypeIcon;
        private RelativeLayout backgroundRelative;
        public VehicleTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            vehicleTypeTxt = itemView.findViewById(R.id.vehicleTypeTxt);
            carTypeIcon= itemView.findViewById(R.id.carTypeIcon);
            backgroundRelative = itemView.findViewById(R.id.backgroundRelative);
           /* vehicleTypeTxt.setOnClickListener(v -> {
                int posIndex = getAdapterPosition();
                myAdapterlistener.onViewClick(v, vehicleTypeResult.get(posIndex).getName() , vehicleTypeResult.get(posIndex).getId());
            });*/
        }
    }
}
