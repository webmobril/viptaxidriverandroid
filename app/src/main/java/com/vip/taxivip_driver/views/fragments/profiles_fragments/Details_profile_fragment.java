package com.vip.taxivip_driver.views.fragments.profiles_fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;

import java.util.Objects;


public class Details_profile_fragment extends Fragment implements View.OnClickListener {

    private ProgressBar post_progressbar;
    private LoginPreference loginPreference;
    private SingInSignUpViewModel signupViewModel;
    private int driverId;
    private Button btn_no_rating, btn_add_details;
    private ImageView editProfileImg, iv_Back, driver_profile_img;
    private TextView driver_nameTxt, aboutYourSelfTextView, languageKnowTxtView, fromWhereTxtView;
    private Context mContext;


    public Details_profile_fragment() {
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signupViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_profile_fragment,
                container, false);
        initView(view);
        return view;
    }


    /* initialize views */
    private void initView(View view) {
        driver_profile_img = view.findViewById(R.id.driver_prifle_img);
        driver_nameTxt = view.findViewById(R.id.driver_nameTxt);
        aboutYourSelfTextView = view.findViewById(R.id.aboutYourSelfTextView);
        fromWhereTxtView = view.findViewById(R.id.fromWhereTxtView);
        languageKnowTxtView = view.findViewById(R.id.languageKnowTxtView);
        post_progressbar = view.findViewById(R.id.post_progressbar);
        editProfileImg = view.findViewById(R.id.editProfileImg);
        btn_no_rating = view.findViewById(R.id.btn_no_rating_yet);
        btn_add_details = view.findViewById(R.id.btn_add_details);
        iv_Back = view.findViewById(R.id.iv_Back);
        driver_nameTxt.setText(loginPreference.getName());
        if (CommonMethod.isNetworkAvailable(mContext)) {
            postGetProfile(driverId);
        } else {
            CommonMethod.showToast(mContext, getString(R.string.internet_connection));
        }


        btn_no_rating.setOnClickListener(this);
        btn_add_details.setOnClickListener(this);
        editProfileImg.setOnClickListener(this);
        iv_Back.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.iv_Back) {
            getActivity().onBackPressed();
        } else if (v.getId() == R.id.editProfileImg) {
            CommonMethod.setSimpleFragment(new ProfileAboutFragment(), Objects.requireNonNull(getActivity()), R.id.frame_profile);
        } else if (v.getId() == R.id.btn_add_details) {
            CommonMethod.setSimpleFragment(new ProfileAboutFragment(), Objects.requireNonNull(getActivity()), R.id.frame_profile);

        } else if (v.getId() == R.id.btn_no_rating_yet) {

            Toast.makeText(getActivity(), "Coming..", Toast.LENGTH_SHORT).show();
        }
    }


    private void postGetProfile(int driverId) {
        post_progressbar.setVisibility(View.VISIBLE);
        signupViewModel.postGetProfileViewModel(driverId).observe(this, updateProfileResponse -> {
            post_progressbar.setVisibility(View.GONE);
            if (updateProfileResponse.getError() != null && !updateProfileResponse.getError()) {

                if (updateProfileResponse.getResult() != null) {
                    driver_nameTxt.setText(updateProfileResponse.getResult().getName());
                    loginPreference.setName(updateProfileResponse.getResult().getName());
                    loginPreference.setUserImage(updateProfileResponse.getResult().getUserImage());
                    loginPreference.setLanguageKnown(updateProfileResponse.getResult().getLanguageKnown());

                    CommonMethod.loadImageResized(getActivity(),
                            Constants.IMAGE_URL + updateProfileResponse.getResult().getUserImage(), driver_profile_img);

                    if (updateProfileResponse.getResult().getDescribeYourself() != null) {
                        aboutYourSelfTextView.setText(updateProfileResponse.getResult().getDescribeYourself());
                    }
                    if (updateProfileResponse.getResult().getLanguageKnown() != null) {
                        String languge = updateProfileResponse.getResult().getLanguageKnown().substring(1).replaceAll(",", " &");
                        languageKnowTxtView.setText(getString(R.string.knows) + " " + languge);
                    }
                    if (updateProfileResponse.getResult().getHomeaddress() != null) {
                        fromWhereTxtView.setText(getString(R.string.from) + updateProfileResponse.getResult().getHomeaddress());
                    }

                }

            } else {
                CommonMethod.showToast(getActivity(), updateProfileResponse.getMessage());
            }
        });

    }


}
