package com.vip.taxivip_driver.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.vip.taxivip_driver.R;

public class Step_verification_fragment extends Fragment {
    private Button btn_setupnow;
    Fragment fragment;
 private ImageView iv_Back;
    public Step_verification_fragment(){}
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.two_step_verification, container, false);
        initView(view);
        return view;
    }
    private void initView(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        btn_setupnow = view.findViewById(R.id.btn_setupnow);
        iv_Back.setOnClickListener(v -> getActivity().onBackPressed());
        btn_setupnow.setOnClickListener(view1 -> {
            fragment = new SecurityEnterPasswordFragment();
            loadFragment(fragment);
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_about, fragment);
        fragmentTransaction.commit();
    }
}
