package com.vip.taxivip_driver.views.fragments.documents;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.activities.ContainerActivity;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.models.DocumentResponse;
import com.vip.taxivip_driver.network.RestApiService;
import com.vip.taxivip_driver.network.RetrofitInstance;
import com.vip.taxivip_driver.utils.LoginPreference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class TakePhotoFragment extends Fragment implements View.OnClickListener {

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 5;
    private ImageView img_document_upload;
    private static final int PICK_GALLERY_IMAGE = 1;
    private static final int PICK_CAMERA_IMAGE = 2;

    private int driverId;
    private Bitmap update_bitmap;
    private String update_imgPath;
    private String update_imageString;
    private File update_destination;
    private LoginPreference loginPreference;
    private  ImageView iv_Back;
    private String documentTypes,typeScreen;
    //private ProgressDialog progressDialog;
    private ProgressBar _progressbarDoc;
    private Context mContext;

    public TakePhotoFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            documentTypes = args.getString(Constants.DRIVER_DOCUMENTS_IMG);
            typeScreen = args.getString(Constants.HOME_SCREEN);

            Log.e(this.getClass().getName(),"DocumnetTypeUpload" + documentTypes + "Type" +typeScreen);

           // showReceivedData.setText(args.getString(DATA_RECEIVE));
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_take_photo, container, false);

        initViews(view);
        return view;
    }

    private void initViews(View view) {
        _progressbarDoc = view.findViewById(R.id._progressbarDoc);

        loginPreference = new LoginPreference(getActivity());
        iv_Back= view.findViewById(R.id.iv_Back);
        driverId= loginPreference.getRegister_id();
        img_document_upload = view.findViewById(R.id.img_document_upload);
        img_document_upload.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_document_upload){
            CaptureImage();
           // checkRunTimePermission();

        }

        else if (v.getId() == R.id.iv_Back){
            if (typeScreen.equals("first_Signup")){
                Intent intent = new Intent(getActivity(), ContainerActivity.class);
                startActivity(intent);
            }
            else {
                getActivity().onBackPressed();

            }


        }
    }


    private void checkRunTimePermission() {
        int storage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);

        if (storage == PackageManager.PERMISSION_GRANTED) {
            selectImage();
        } else {
            runTimePermission();
        }
    }


    private void runTimePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_ID_MULTIPLE_PERMISSIONS);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("msg", "Permission callback called-------");
        // Initialize the map with both permissions
        // Fill with actual results from user
        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<>();
            perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for  permissions

                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ) {
                    Log.e("msg", "All Permissions granted");
                } else {
                    Log.e("msg", "Some permissions are not granted ask again ");
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showDialogOK(getResources().getString(R.string.permission_req),
                                (dialog, which) -> {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkAndRequestPermissions();
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            // proceed with logic by disabling the related features or quit the app.
                                            break;
                                    }
                                });
                    }
                    //permission is denied (and never ask again is  checked)
                    //shouldShowRequestPermissionRationale will return false
                    else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.enable_permission), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.Okay), okListener)
                .setNegativeButton(getResources().getString(R.string.Okay), okListener)
                .create()
                .show();
    }

    private boolean checkAndRequestPermissions() {
        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[0]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void CaptureImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA}, 1);
            }
        } else {
            selectImage();
        }
    }



    private void selectImage() {
        final Dialog dialog = new Dialog(getActivity());

        dialog.setContentView(R.layout.camer_dialog_layout);
        TextView txt_no = dialog.findViewById(R.id.btn_no);
        txt_no.setOnClickListener(v -> {
            dialog.dismiss();
        });
        ImageView imageViewCamera = dialog.findViewById(R.id.camera_img);
        imageViewCamera.setOnClickListener(v -> {
            dialog.dismiss();
            Fragment frag = this;
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            //startActivityForResult(intent, PICK_CAMERA_IMAGE);
            frag.startActivityForResult(intent, Constants.CAMERA_REQUEST);
        });
        ImageView imageViewGallery = dialog.findViewById(R.id.gallery_icon);
        imageViewGallery.setOnClickListener(v -> {
            dialog.dismiss();
            Intent intentGalley = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            getActivity().startActivityForResult(intentGalley, Constants.GALLERY_IMAGE);
            //startActivityForResult(intentGalley, PICK_GALLERY_IMAGE);
        });

        dialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == Constants.CAMERA_REQUEST  && data != null) {
            try {
                update_bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                update_bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
                byte[] imageBytes = bytes.toByteArray();
                update_imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                update_destination = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                        "Camera" + "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    update_destination.createNewFile();
                    fo = new FileOutputStream(update_destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                update_imgPath = update_destination.getAbsolutePath();
                img_document_upload.setImageBitmap(update_bitmap);


                documentUploadApi();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        else if (requestCode == Constants.GALLERY_IMAGE  && data != null) {
            Uri selectedImage = data.getData();
            try {
                update_bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                update_bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
                Log.e("Activity", "Pick from Gallery::>>> ");
                update_imgPath = getRealPathFromURI(selectedImage);
                byte[] imageBytes = bytes.toByteArray();
                update_imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                update_destination = new File(update_imgPath);
                img_document_upload.setImageBitmap(update_bitmap);

                documentUploadApi();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }



    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }




    private void documentUploadApi() {
        _progressbarDoc.setVisibility(View.VISIBLE);
        MultipartBody.Part permit_req_body = null,carInsurance_body= null;
        MultipartBody.Part Driverlicen_body= null,policeRecord_body = null ,criminalRecord_body = null,vehicelCertificate_body = null;
        if (documentTypes.equals("driver_licence_")){

            File license = new File(String.valueOf(update_destination));
            RequestBody license_req = RequestBody.create(MediaType.parse("multipart/form-data"), license);
            Driverlicen_body = MultipartBody.Part.createFormData("driver_license_image", license.getName(), license_req);

        }else if (documentTypes.equals("driver_vehicle_certificate")){
            File vehicelCertificate = new File(String.valueOf(update_destination));
            RequestBody ehicelCertificate_req = RequestBody.create(MediaType.parse("multipart/form-data"), vehicelCertificate);
            vehicelCertificate_body = MultipartBody.Part.createFormData("registration_certificate_image", vehicelCertificate.getName(), ehicelCertificate_req);
        }else if (documentTypes.equals("driver_vehicle_permit")){

            File permit = new File(String.valueOf(update_destination));
            RequestBody permit_req = RequestBody.create(MediaType.parse("multipart/form-data"), permit);
            permit_req_body = MultipartBody.Part.createFormData("permit_image", permit.getName(), permit_req);

        }
        else if (documentTypes.equals("driver_car_insurance")){

            File carInsurance = new File(String.valueOf(update_destination));
            RequestBody carInsurance_req = RequestBody.create(MediaType.parse("multipart/form-data"), carInsurance);
            carInsurance_body= MultipartBody.Part.createFormData("car_insurance_image", carInsurance.getName(), carInsurance_req);

        }


        else if (documentTypes.equals("driver_criminal_record_")){
            File criminalRecord = new File(String.valueOf(update_destination));
            RequestBody license_req = RequestBody.create(MediaType.parse("multipart/form-data"), criminalRecord);
            criminalRecord_body = MultipartBody.Part.createFormData("criminal_record_img", criminalRecord.getName(), license_req);


        }else if (documentTypes.equals("police_record")){
            File police_record = new File(String.valueOf(update_destination));
            RequestBody policeRecord_req = RequestBody.create(MediaType.parse("multipart/form-data"), police_record);
            policeRecord_body = MultipartBody.Part.createFormData("police_record_img", police_record.getName(), policeRecord_req);

        }

        RequestBody user = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(driverId));


        RestApiService retrofitInterface = RetrofitInstance.getApiService();

        Call<DocumentResponse> call = retrofitInterface.getDocumentUpload(user, Driverlicen_body,vehicelCertificate_body,permit_req_body, carInsurance_body, criminalRecord_body,
                policeRecord_body);

        call.enqueue(new Callback<DocumentResponse>() {
            @Override
            public void onResponse(Call<DocumentResponse> call, Response<DocumentResponse> response) {
                Log.e("uploading response", new Gson().toJson(response.body()));
                _progressbarDoc.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    DocumentResponse documentResponse = response.body();
                    if (documentResponse != null) {
                        if (!documentResponse.getMessage().isEmpty()) {
                            Toast.makeText(mContext, documentResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, documentResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<DocumentResponse> call, Throwable t) {
                _progressbarDoc.setVisibility(View.GONE);
                Log.e("error response", t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



}
