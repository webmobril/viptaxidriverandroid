package com.vip.taxivip_driver.views.fragments.signup_views;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.hbb20.CountryCodePicker;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.ForgetPasswordRequest;
import com.vip.taxivip_driver.views.activities.welcom.SignInActivity;
import com.vip.taxivip_driver.models.ForgetPasswordResponse;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.viewmodels.ForgetResetPasswordViewModel;

import java.util.Objects;


public class ForgetPassword_fragment extends Fragment {

    private ForgetResetPasswordViewModel forgetPasswordViewModel;
    private EditText ed_password, ed_newPassword, input_mobile;
    private Button btn_confirm;
    private CheckBox forget_checked;
    private boolean checked;
    private ImageView iv_Back;
    private Context mContext;
    private CountryCodePicker ccp;
    private String countryCode;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forget_password_fragment, container, false);
        forgetPasswordViewModel = new ViewModelProvider(this).get(ForgetResetPasswordViewModel.class);
        initView(view);
        return view;
    }

    private void initView(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        ed_password = view.findViewById(R.id.ed_password);
        input_mobile = view.findViewById(R.id.input_mobile);
        ed_newPassword = view.findViewById(R.id.ed_confirm_password);
        btn_confirm = view.findViewById(R.id.btn_confirm);
        forget_checked = view.findViewById(R.id.forget_agree);
        iv_Back.setOnClickListener(v -> {
            Objects.requireNonNull(getActivity()).onBackPressed();
        });
        ccp = view.findViewById(R.id.ccp);
        countryCode = ccp.getDefaultCountryCode();

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode = ccp.getSelectedCountryCode();
                Log.e(this.getClass().getName(), "SELECTED_COUNTRY" + countryCode);
            }
        });
        btn_confirm.setOnClickListener(view1 -> {
            String mobile = input_mobile.getText().toString().trim();
            String password = ed_password.getText().toString().trim();
            String confirmPassword = ed_newPassword.getText().toString().trim();
            checked = forget_checked.isChecked();

            //if (CommonMethod.isNetworkAvailable(mContext)) {
                if (isValidated(mobile, password, confirmPassword, checked)) {
                    ForgetPasswordRequest forgetPasswordRequest = new ForgetPasswordRequest();
                    forgetPasswordRequest.setMobile(mobile);
                    forgetPasswordRequest.setNew_password(password);
                    forgetPasswordRequest.setConfirm_password(confirmPassword);
                    forgetPasswordRequest.setRoles_id(5);
                    forgetPasswordRequest.setCountry_code(countryCode);
                    getForgetPassApi(forgetPasswordRequest);
                }
            /*} else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
            }*/
        });

    }

    private void getForgetPassApi(ForgetPasswordRequest forgetPasswordRequest) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));

        forgetPasswordViewModel.getForgetResponse(forgetPasswordRequest).observe(this, new Observer<ForgetPasswordResponse>() {
            @Override
            public void onChanged(ForgetPasswordResponse passwordResponse) {
                myDialog.dismiss();
                if (passwordResponse.getError() != null && !passwordResponse.getError()) {
                    Log.e(this.getClass().getName(), "ForgetPassData" + passwordResponse.getMessage());
                    CommonMethod.showToast(getActivity(), passwordResponse.getMessage());
                    CommonMethod.callActivity(getActivity(), SignInActivity.class);

                } else if (passwordResponse.getMessage() != null){
                    CommonMethod.showToast(getActivity(), passwordResponse.getMessage());
                } else {
                    CommonMethod.showAlert(getString(R.string.internet_connection),getActivity());
                }
            }
        });

    }


    private boolean isValidated(String mobile, String password, String confirmPassword, boolean checked) {
        String Regex = "[^\\d]";
        String PhoneDigits = mobile.replaceAll(Regex, "");
        if ((PhoneDigits.length() < 7) || (PhoneDigits.length() > 15)) {
            input_mobile.setError(getString(R.string.enter_valid_mobile_no));
            input_mobile.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(password)) {
            ed_password.setError(getString(R.string.password_required));
            ed_password.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(confirmPassword)) {
            ed_newPassword.setError(getString(R.string.enter_confirm_password));
            ed_newPassword.requestFocus();
            return false;
        } else if (!password.equals(confirmPassword)) {
            CommonMethod.showToast(getActivity(), getString(R.string.password_did_match));
            return false;
        } else if (!checked) {
            CommonMethod.showToast(getActivity(), getString(R.string.check_term));
            return false;
        }
        return true;
    }

}
