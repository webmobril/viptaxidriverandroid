
package com.vip.taxivip_driver.views.fragments.signup_views;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.ResetPasswordRequest;
import com.vip.taxivip_driver.views.activities.welcom.SignInActivity;
import com.vip.taxivip_driver.models.ResetPasswordResponse;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.ForgetResetPasswordViewModel;

import java.util.Objects;


public class ResetPasswordFragment extends Fragment {
    private ForgetResetPasswordViewModel forgetPasswordViewModel;
    private EditText et_old_password, et_create_new_password, et_create_confirm_password;
    private Button btn_confirm;
    private ProgressBar post_progressbar;
    private LoginPreference loginPreference;
    private int driverId;
    private ImageView iv_Back;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forgetPasswordViewModel = new ViewModelProvider(this).get(ForgetResetPasswordViewModel.class);
        loginPreference = new LoginPreference(getActivity());

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_reset_password, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        driverId = loginPreference.getRegister_id();
        iv_Back = view.findViewById(R.id.iv_Back);
        et_old_password = view.findViewById(R.id.et_old_password);
        et_create_new_password = view.findViewById(R.id.et_create_new_password);
        et_create_confirm_password = view.findViewById(R.id.et_create_confirm_password);
        btn_confirm = view.findViewById(R.id.btn_confirm);
        post_progressbar = view.findViewById(R.id.post_progressbar);


        iv_Back.setOnClickListener(v ->
            Objects.requireNonNull(getActivity()).onBackPressed()
        );
        btn_confirm.setOnClickListener(v -> {

            String oldPassword = et_old_password.getText().toString().trim();
            String newPassword = et_create_new_password.getText().toString().trim();
            String confirmPassword = et_create_confirm_password.getText().toString().trim();
           // if (CommonMethod.isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
                if (checkValidation(oldPassword, newPassword, confirmPassword)) {
                    ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();
                    resetPasswordRequest.setUser_id(driverId);
                    resetPasswordRequest.setOld_password(oldPassword);
                    resetPasswordRequest.setNew_password(newPassword);
                    resetPasswordRequest.setConfirm_password(confirmPassword);
                    postResetPassApi(resetPasswordRequest);
                }
           /* } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
            }*/

        });


    }


    private void postResetPassApi(ResetPasswordRequest resetPasswordRequest) {
        post_progressbar.setVisibility(View.VISIBLE);
        forgetPasswordViewModel.getResetPasswordResponse(resetPasswordRequest).observe(this, new Observer<ResetPasswordResponse>() {
            @Override
            public void onChanged(ResetPasswordResponse resetPasswordResponse) {
                post_progressbar.setVisibility(View.GONE);
                if (resetPasswordResponse != null && !resetPasswordResponse.getError()) {
                    Log.e(this.getClass().getName(), "ResetPassword Response==" + resetPasswordResponse.getMessage());
                    CommonMethod.showToast(getActivity(), resetPasswordResponse.getMessage());
                    CommonMethod.callActivity(getActivity(), SignInActivity.class);

                } else {
                    assert resetPasswordResponse != null;
                    CommonMethod.showToast(getActivity(), resetPasswordResponse.getMessage());

                }
            }
        });

    }


    private boolean checkValidation(String m_old_password, String m_new_password, String m_confirm_password) {

        if (TextUtils.isEmpty(m_old_password)) {
            et_old_password.requestFocus();
            et_old_password.setError(getString(R.string.enter_current_password), null);
            return false;
        } else if (TextUtils.isEmpty(m_new_password)) {
            et_create_new_password.requestFocus();
            et_create_new_password.setError(getString(R.string.enter_new_password), null);
            return false;
        } else if (TextUtils.isEmpty(m_confirm_password)) {
            et_create_confirm_password.requestFocus();
            et_create_confirm_password.setError(getString(R.string.please_confirm_password), null);
            return false;
        } else if (!m_new_password.equals(m_confirm_password)) {
            et_create_confirm_password.requestFocus();
            et_create_confirm_password.setError(getString(R.string.password_not_match));
            return false;
        }
        return true;
    }


}
