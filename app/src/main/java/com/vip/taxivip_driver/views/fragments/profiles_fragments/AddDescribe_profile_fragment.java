package com.vip.taxivip_driver.views.fragments.profiles_fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.ProfileDescribeRequest;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.ProfileViewModel;

import java.util.Objects;


public class AddDescribe_profile_fragment extends Fragment implements View.OnClickListener {

    private TextView txt_save, textDescribe, txt_wordsCount;
    private ImageView iv_Back;
    private String valueText;
    private ProfileViewModel profileViewModel;
    private LoginPreference loginPreference;
    private int driverId;
    private ProgressBar post_progressbar;
    private EditText edit_value;

    public AddDescribe_profile_fragment() {
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.e(this.getClass().getName(), "onAttach");
        Bundle bundle = getArguments();
        if (bundle != null) {
            valueText = bundle.getString(Constants.TYPE_VALUE);
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_describe_yourself, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        Log.e(this.getClass().getName(), "onCreateView");
        post_progressbar = view.findViewById(R.id.post_progressbar);
        edit_value = view.findViewById(R.id.edit_value);
        iv_Back = view.findViewById(R.id.iv_Back);
        txt_wordsCount = view.findViewById(R.id.txt_wordsCount);
        textDescribe = view.findViewById(R.id.textDescribe);
        txt_save = view.findViewById(R.id.txt_save);
        txt_save.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
        edit_value.setText(loginPreference.getDescribeYourself());
        Log.e(this.getClass().getName(), "VAlue_Get" + valueText);


        if (valueText != null) {
            textDescribe.setText(R.string.where_are_you_from);
            if (loginPreference.getWhereFrom() != null) {
                edit_value.setText(loginPreference.getWhereFrom());
                int length = loginPreference.getWhereFrom().length();
                Log.e(this.getClass().getName(), "WhereFrom" + length);
                txt_wordsCount.setText(length + "/100" + "");
            }
        } else {
            if (loginPreference.getDescribeYourself() != null) {
                edit_value.setText(loginPreference.getDescribeYourself());
                int length = loginPreference.getDescribeYourself().length();
                Log.e(this.getClass().getName(), "DescribeYourSelfLEngth" + length);
                txt_wordsCount.setText(length + "/100" + "");
            } /*else if (loginPreference.getWhereFrom() !=null){
                edit_value.setText(loginPreference.getWhereFrom());
                int length = loginPreference.getWhereFrom().length();
                Log.e(this.getClass().getName(),"WhereFrom" + length);
                txt_wordsCount.setText(length +"/50"+ "");
            }*/
        }


        edit_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txt_wordsCount.setText(100 - s.toString().length() + "/100");

            }
        });


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txt_save) {
            if (valueText != null && valueText.equals("HomeAddress")) {
                ProfileDescribeRequest profileDescribeRequest = new ProfileDescribeRequest();
                profileDescribeRequest.setDriver_id(driverId);
                profileDescribeRequest.setDescribe_yourself(loginPreference.getDescribeYourself());
                profileDescribeRequest.setHomeaddress(edit_value.getText().toString());
                updateProfilePost(profileDescribeRequest);
            } else {
                ProfileDescribeRequest profileDescribeRequest = new ProfileDescribeRequest();
                profileDescribeRequest.setDriver_id(driverId);
                profileDescribeRequest.setDescribe_yourself(edit_value.getText().toString());
                profileDescribeRequest.setHomeaddress(loginPreference.getWhereFrom());
                updateProfilePost(profileDescribeRequest);
            }

        } else if (v.getId() == R.id.iv_Back) {
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
    }


    private void updateProfilePost(ProfileDescribeRequest profileDescribeRequest) {
        post_progressbar.setVisibility(View.VISIBLE);
        profileViewModel.postUpdateProfileDescribeViewModel(profileDescribeRequest)
                .observe(this, updateProfileResponse -> {
                    post_progressbar.setVisibility(View.GONE);
                    Log.e(this.getClass().getName(), "UPDATE_Profile_RESPONSE=" + updateProfileResponse.getMessage());

                    if (updateProfileResponse.getError() != null && !updateProfileResponse.getError()) {
                        CommonMethod.showToast(getActivity(), updateProfileResponse.getMessage());
                        if (valueText != null && valueText.equals("HomeAddress")) {
                            loginPreference.setWhereFrom(updateProfileResponse.getResult().getHomeaddress());

                        } else {
                            if (updateProfileResponse.getResult().getDescribeYourself() != null) {
                                loginPreference.setDescribeYourself(updateProfileResponse.getResult().getDescribeYourself());
                                int length = updateProfileResponse.getResult().getDescribeYourself().length();
                                Log.e(this.getClass().getName(), "DescribeYourSelfLength" + length);
                                txt_wordsCount.setText(length / 100 + "");
                            }
                        }
                        Objects.requireNonNull(getActivity()).onBackPressed();

                    } else if (updateProfileResponse.getMessage() != null){
                        CommonMethod.showToast(getActivity(), updateProfileResponse.getMessage());
                    } else {
                        CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
                    }

                });

    }


}
