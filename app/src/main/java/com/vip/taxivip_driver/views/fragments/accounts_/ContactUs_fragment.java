package com.vip.taxivip_driver.views.fragments.accounts_;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.viewmodels.AccountViewModel;


public class ContactUs_fragment extends Fragment implements View.OnClickListener {
    private ImageView iv_Back;
    private AccountViewModel accountViewModel;
    private TextView contactTextView,webUrlTextView,emailTextView;

    public ContactUs_fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contactus_fragment,container,false);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);

        initViews(view);
        return view;
    }

   private void initViews(View view){
       contactTextView = view.findViewById(R.id.contactTextView);
       webUrlTextView = view.findViewById(R.id.webUrlTextView);
       emailTextView = view.findViewById(R.id.emailTextView);
       iv_Back= view.findViewById(R.id.iv_Back);
       iv_Back.setOnClickListener(this);
       getContactUsInfoApi();


   }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_Back) {
            getActivity().onBackPressed();
        }
    }


    private void getContactUsInfoApi(){
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        accountViewModel.getContactUsViewModel().observe(this, contactUsResponse -> {
            myDialog.dismiss();
            Log.e(this.getClass().getName(),"RESPONSE_CONTACT" + contactUsResponse.getError());
            if (contactUsResponse.getError() != null && !contactUsResponse.getError()){
                contactTextView.setText(contactUsResponse.getResult().getContactNo());
                webUrlTextView.setText(contactUsResponse.getResult().getContactEmail());

            } else if (contactUsResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(),contactUsResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection),getActivity());
            }

        });
    }




}
