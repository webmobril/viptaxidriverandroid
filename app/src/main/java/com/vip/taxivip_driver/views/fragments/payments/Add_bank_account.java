package com.vip.taxivip_driver.views.fragments.payments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vip.taxivip_driver.R;

public class Add_bank_account extends Fragment implements View.OnClickListener {

    private ImageView iv_Back;
    public Add_bank_account() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_bank_account_fragment, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        iv_Back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_Back){
            getActivity().onBackPressed();
        }
    }
}
