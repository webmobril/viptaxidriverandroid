package com.vip.taxivip_driver.views.fragments.accounts_;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.viewmodels.AccountViewModel;

public class TermsCondition_fragment extends Fragment implements View.OnClickListener {

    private ProgressBar mProgressD;
    private TextView tvTitleToolbar,termsCondition_txt;
    private ImageView iv_Back;
    private AccountViewModel accountViewModel;
    private ProgressBar post_progressbar;

    public TermsCondition_fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.terms_condition_fragment,container,false);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);

        initViews(view);
        return view;

    }

    private void initViews(View view) {
        post_progressbar = view.findViewById(R.id.post_progressbar);
        iv_Back = view.findViewById(R.id.iv_Back);
        tvTitleToolbar = view.findViewById(R.id.tvTitleToolbar);
        tvTitleToolbar.setText(R.string._term_condition);
        termsCondition_txt = view.findViewById(R.id.termsCondition_txt);
        iv_Back.setOnClickListener(this);
        getTermsServicesResponse();

    }



    private void  getTermsServicesResponse(){
        post_progressbar.setVisibility(View.VISIBLE);
        accountViewModel.getTermsConditionViewModel(1).observe(this, termsServicesResponse -> {
            post_progressbar.setVisibility(View.GONE);
            if (termsServicesResponse.getError() != null && !termsServicesResponse.getError()){
                if (termsServicesResponse.getResult().getContentSpanish() != null) {
                    termsCondition_txt.setText(termsServicesResponse.getResult().getContentSpanish());
                } else {
                    termsCondition_txt.setText(termsServicesResponse.getResult().getContentEnglish());

                }
            }
        });


    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.iv_Back){
            getActivity().onBackPressed();
        }
    }
}
