package com.vip.taxivip_driver.views.fragments.profiles_fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;


public class ProfileAboutFragment extends Fragment implements View.OnClickListener {
    private ImageView img_addDescribe, img_addLanguage, img_addShareLocation, editProfileImg, iv_Back, driver_icon_profile;
    private TextView tvToolbarTitle, nameText;
    private LoginPreference loginPreference;
    private Context mContext;

    public ProfileAboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_about, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        loginPreference = new LoginPreference(getActivity());
        driver_icon_profile = view.findViewById(R.id.driver_icon_profile);
        nameText = view.findViewById(R.id.nameText);
        iv_Back = view.findViewById(R.id.iv_Back);
        editProfileImg = view.findViewById(R.id.editProfileImg);
        editProfileImg.setVisibility(View.GONE);
        tvToolbarTitle = view.findViewById(R.id.tvToolbarTitle);
        img_addDescribe = view.findViewById(R.id.img_addDescribe);
        img_addLanguage = view.findViewById(R.id.img_addLanguage);
        img_addShareLocation = view.findViewById(R.id.img_addShareLocation);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(R.string.edit_profile);
        img_addDescribe.setOnClickListener(this);
        img_addLanguage.setOnClickListener(this);
        img_addShareLocation.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
        nameText.setText(loginPreference.getName());
        CommonMethod.loadImageResized(getActivity(), Constants.IMAGE_URL + loginPreference.getUserImage(), driver_icon_profile);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_Back:
                getActivity().onBackPressed();
                break;
            case R.id.img_addDescribe:
                CommonMethod.setSimpleFragment(new AddDescribe_profile_fragment(), getActivity(), R.id.frame_editProfile);
                break;

            case R.id.img_addLanguage:
                CommonMethod.setSimpleFragment(new AddLanguageFragment(), getActivity(), R.id.frame_editProfile);

                break;

            case R.id.img_addShareLocation:
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TYPE_VALUE, "HomeAddress");
                AddDescribe_profile_fragment fragment1 = new AddDescribe_profile_fragment();
                fragment1.setArguments(bundle);
                CommonMethod.setSimpleFragment(fragment1, getActivity(), R.id.frame_editProfile);

                break;
        }
    }

}
