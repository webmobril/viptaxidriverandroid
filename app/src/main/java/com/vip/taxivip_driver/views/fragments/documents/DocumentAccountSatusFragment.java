package com.vip.taxivip_driver.views.fragments.documents;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;


public class DocumentAccountSatusFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    private ImageView iv_Back;
    private SingInSignUpViewModel signupViewModel;
    private LoginPreference loginPreference;
    private int driverId;
    private ProgressDialog progressDialog;
    private LinearLayout driverLicenseLayout, vehicleCertificateLayout, vehiclePermitLayout,
            criminalRecordLayout, policRecordLayout, vehicleCarInsuranceLayout;
    private TextView documentVerify, uploadDocNextTxt;
    private RadioGroup documentRadioGroup;
    private String frameType;
    private Button btn_submit;
    private Context mContext;

    public DocumentAccountSatusFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            frameType = bundle.getString(Constants.HOME_SCREEN);
            Log.e(this.getClass().getName(), "FrameType" + frameType);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_document_account_satus, container, false);
        signupViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        btn_submit = view.findViewById(R.id.btn_submit);
        vehicleCarInsuranceLayout = view.findViewById(R.id.vehicleCarInsuranceLayout);
        documentRadioGroup = view.findViewById(R.id.documentRadioGroup);
        documentVerify = view.findViewById(R.id.documentVerify);
        uploadDocNextTxt = view.findViewById(R.id.uploadDocNextTxt);
        driverLicenseLayout = view.findViewById(R.id.driverLicenseLayout);
        vehicleCertificateLayout = view.findViewById(R.id.vehicleCertificateLayout);
        vehiclePermitLayout = view.findViewById(R.id.vehiclePermitLayout);
        criminalRecordLayout = view.findViewById(R.id.criminalRecordLayout);
        policRecordLayout = view.findViewById(R.id.policRecordLayout);
        iv_Back = view.findViewById(R.id.iv_Back);
        iv_Back.setOnClickListener(this);
        progressDialog = new ProgressDialog(getActivity());
        if (CommonMethod.isNetworkAvailable(mContext)) {
            postGetProfile(driverId);
        } else {
            CommonMethod.showToast(mContext, getString(R.string.internet_connection));
        }

        documentRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton radioButton = documentRadioGroup.findViewById(checkedId);
            int index = documentRadioGroup.indexOfChild(radioButton);
            radioButton = group.findViewById(checkedId);
            if (!radioButton.isPressed()) {
                return;
            }
            // Add logic here
            switch (index) {
                case 2: // first button
                    callActivityPhoto("driver_licence_", frameType);
                    break;
                case 4:

                    callActivityPhoto("driver_vehicle_certificate", frameType);
                    break;

                case 6:
                    callActivityPhoto("driver_car_insurance", frameType);

                    break;
                case 8:
                    callActivityPhoto("driver_vehicle_permit", frameType);

                    break;

                case 10:
                    callActivityPhoto("driver_criminal_record_", frameType);
                    break;

                case 12:
                    callActivityPhoto("police_record", frameType);
                    break;


            }
        });
    }


    private void callActivityPhoto(String docType, String screenOpen) {
        Intent intent = new Intent(getActivity(), TakePhotoActivity.class);
        intent.putExtra(Constants.DRIVER_DOCUMENTS_IMG, docType);
        intent.putExtra(Constants.HOME_SCREEN, screenOpen);
        startActivity(intent);
    }


   /* public void CallFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (frameType != null && frameType.equals("first_Signup")) {
            fragmentTransaction.replace(R.id.fragment_container, fragment).addToBackStack(null);
        } else {
            fragmentTransaction.replace(R.id.frame_about, fragment).addToBackStack(null);
        }
        fragmentTransaction.commit();
    }*/


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.iv_Back) {
            getActivity().onBackPressed();
        }
    }


    private void postGetProfile(int driverId) {
        progressDialog.show();
        signupViewModel.postGetProfileViewModel(driverId).observe(this, updateProfileResponse -> {
            progressDialog.dismiss();
            if (updateProfileResponse.getError() != null && !updateProfileResponse.getError()) {
                Log.e(this.getClass().getName(), "CheckDocStatus_Response=" + updateProfileResponse.getError());
                if (updateProfileResponse.getResult().getDocuments() != null) {
                    int verifyDocumets = updateProfileResponse.getResult().getVerify();
                    loginPreference.setUserImage(updateProfileResponse.getResult().getUserImage());
                    String registration_verified = updateProfileResponse.getResult().getDocuments().getRegistrationImg();
                    String permitImage = updateProfileResponse.getResult().getDocuments().getPermitImage();
                    String criminal_record = updateProfileResponse.getResult().getDocuments().getCriminalRecordImg();
                    String driverLicence = updateProfileResponse.getResult().getDocuments().getDlImage();
                    String policeRecord = updateProfileResponse.getResult().getDocuments().getPoliceRecordImg();
                    String carInsurance = updateProfileResponse.getResult().getDocuments().getInsuranceImage();
                    if (verifyDocumets == 1) {
                        documentVerify.setText(R.string.document_Verify);
                    }
                    if (driverLicence != null) {
                        driverLicenseLayout.setVisibility(View.VISIBLE);
                    }
                    if (registration_verified != null) {
                        vehicleCertificateLayout.setVisibility(View.VISIBLE);
                        uploadDocNextTxt.setText(getString(R.string.car_insurance_image));
                    }
                    if (carInsurance != null) {
                        vehicleCarInsuranceLayout.setVisibility(View.VISIBLE);
                        uploadDocNextTxt.setText(getString(R.string.vehicle_permit));
                    }
                    if (permitImage != null) {
                        vehiclePermitLayout.setVisibility(View.VISIBLE);
                        uploadDocNextTxt.setText(getString(R.string.criminal_record));
                    }
                    if (criminal_record != null) {
                        criminalRecordLayout.setVisibility(View.VISIBLE);
                        uploadDocNextTxt.setText(getString(R.string.police_record));
                    }
                    if (policeRecord != null) {
                        policRecordLayout.setVisibility(View.VISIBLE);
                        uploadDocNextTxt.setText(getString(R.string.complete_doc));
                        btn_submit.setText(getString(R.string.complete_doc));

                    }
                }

            } else {
                CommonMethod.showToast(mContext, updateProfileResponse.getMessage());
            }
        });

    }
}
