package com.vip.taxivip_driver.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.models.TripHistoryResponse;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.Constants;

import java.util.List;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.PaymentHolder> {

    private List<TripHistoryResponse.Result> tripHistoryResponseList;
    private Context mContext;

    public PaymentHistoryAdapter(Context mContext,List<TripHistoryResponse.Result> tripHistoryResponseList ) {
        this.mContext= mContext;
        this.tripHistoryResponseList = tripHistoryResponseList;
    }

    @NonNull
    @Override
    public PaymentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_rating_list, parent, false);
        return new PaymentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentHolder holder, int position) {

        if (tripHistoryResponseList.size() >0){
            holder.userNameTextView.setText(tripHistoryResponseList.get(position).getUsers().getName());
           // holder.totalAmountTextView.setText(tripHistoryResponseList.get(position).getTotalFare());
            holder.sourceAddressOne.setText(tripHistoryResponseList.get(position).getSource());
            holder.destinationAddressOne.setText(tripHistoryResponseList.get(position).getDestination());
            CommonMethod.loadImageResized(mContext, Constants.IMAGE_URL + tripHistoryResponseList.get(position).getUsers().getUserImage(),holder.userImage);

        }
    }

    @Override
    public int getItemCount() {
        return tripHistoryResponseList.size();
    }

    public class PaymentHolder extends RecyclerView.ViewHolder {
        private TextView userNameTextView,totalAmountTextView,sourceAddressOne,
                sourceAddressTwo,destinationAddressOne,destinationAddressTwo;
        private ImageView userImage;
        private RatingBar driver_ratingbar;
        public PaymentHolder(@NonNull View itemView) {
            super(itemView);
            userImage = itemView.findViewById(R.id.userImage);
            userNameTextView = itemView.findViewById(R.id.userNameTextView);
            totalAmountTextView =itemView.findViewById(R.id.totalAmountTextView);
            sourceAddressOne = itemView.findViewById(R.id.sourceAddressOne);
            sourceAddressTwo= itemView.findViewById(R.id.sourceAddressTwo);
            destinationAddressOne= itemView.findViewById(R.id.destinationAddressOne);
            destinationAddressTwo = itemView.findViewById(R.id.destinationAddressTwo);
            driver_ratingbar= itemView.findViewById(R.id.driver_ratingBar);

        }
    }
}