package com.vip.taxivip_driver.views.fragments.documents;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.VehicleUploadRequest;
import com.vip.taxivip_driver.views.activities.account.Account_Activity;
import com.vip.taxivip_driver.views.adapter.VehicleTypeAdapter;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.models.VehicleTypeResponse;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.ProfileViewModel;
import java.util.List;

public class VehicleTypeFragment extends Fragment implements View.OnClickListener, VehicleTypeAdapter.MyclickListnerLearning {

    private ProfileViewModel profileViewModel;
    private Button btn_confirm;
    private ImageView iv_Back;
    private RecyclerView recyclerViewVehicleType;
    private VehicleTypeAdapter vehicleTypeAdapter;
    private String carModelVal,carBrandVal, carYearVal, carColorVal;
    private LoginPreference loginPreference;
    private int driverId;
    private int vehicleTypeGetId;
    private String vehicleName;
    private Context mContext;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId= loginPreference.getRegister_id();
        Bundle bundle = getArguments();
        if (bundle != null){
            carModelVal = bundle.getString(Constants.VEHICLE_MODEL);
            carBrandVal = bundle.getString(Constants.VEHICLE_BRAND);
            carYearVal = bundle.getString(Constants.VEHICLE_YEAR);
            carColorVal = bundle.getString(Constants.VEHICLE_COLOR);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vehicle_type_fragment, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        btn_confirm= view.findViewById(R.id.btn_confirm);
        iv_Back= view.findViewById(R.id.iv_Back);
        recyclerViewVehicleType = view.findViewById(R.id.recyclerViewVehicleType);
        iv_Back.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);
        vehicleName = loginPreference.getVehicleType();
       //if (CommonMethod.isNetworkAvailable(mContext)) {
           getVehicleTypeApi();
       /*}  else {
           CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
       }*/
    }

    private void getVehicleTypeApi() {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        profileViewModel.getVehicleTypeViewModel().observe(this, vehicleTypeResponse -> {
            myDialog.dismiss();
            if (vehicleTypeResponse != null && vehicleTypeResponse.getError() != null && !vehicleTypeResponse.getError() ) {
                List<VehicleTypeResponse.Result> listResponse = vehicleTypeResponse.getResult();
                if (listResponse != null) {
                    prepareRecyclerView(listResponse);
                }
            }
             else if (vehicleTypeResponse.getMessage() != null){
                 CommonMethod.showAlert(vehicleTypeResponse.getMessage(), getActivity());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());

            }

        });
    }

    private void prepareRecyclerView(List<VehicleTypeResponse.Result> resultList) {
        vehicleTypeAdapter = new VehicleTypeAdapter(getActivity(),resultList, this,vehicleName);
        CommonMethod.setRecyclerView(getActivity(),recyclerViewVehicleType);
        recyclerViewVehicleType.setAdapter(vehicleTypeAdapter);
        vehicleTypeAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.iv_Back){
         getActivity().onBackPressed();
        }
        else if (v.getId() == R.id.btn_confirm){
            if (vehicleTypeGetId != 0) {
                VehicleUploadRequest vehicleUploadRequest = new VehicleUploadRequest();
                vehicleUploadRequest.setUser_id(driverId);
                vehicleUploadRequest.setCar_name(carBrandVal);
                vehicleUploadRequest.setModel_number(carModelVal);
                vehicleUploadRequest.setCar_year(carYearVal);
                vehicleUploadRequest.setCar_color(carColorVal);
                vehicleUploadRequest.setViehcle_type_id(vehicleTypeGetId);
                vehicleModelTypeUploadApi(vehicleUploadRequest);
            } else {
                CommonMethod.showToast(getActivity(),mContext.getString(R.string.select_vehicle_type));
            }
        }
    }


    private void vehicleModelTypeUploadApi(VehicleUploadRequest vehicleUploadRequest) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        profileViewModel.postVehicleModelMutable(vehicleUploadRequest).observe(this, documentResponse -> {
            myDialog.dismiss();
            if (documentResponse.getError() != null && !documentResponse.getError()){
                Log.e(this.getClass().getName(),"DriverTypeResponse== " + documentResponse.getMessage());
                CommonMethod.showToast(getActivity(),documentResponse.getMessage());
                loginPreference.setVehicleType(vehicleName);
                CommonMethod.callActivity(mContext ,Account_Activity.class);
            }
            else if (documentResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(),documentResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection),getActivity());
            }
        });
    }

    @Override
    public void onViewClick( String nameType, int vehicleTypeId) {
        vehicleTypeGetId = vehicleTypeId;
        vehicleName = nameType;
        Log.e(this.getClass().getName(),"Vehicle_Name" + nameType +"VehicleType_ID" + vehicleTypeId);

    }




}
