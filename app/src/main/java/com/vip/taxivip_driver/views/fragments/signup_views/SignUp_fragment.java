package com.vip.taxivip_driver.views.fragments.signup_views;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.hbb20.CountryCodePicker;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.SingupRequest;
import com.vip.taxivip_driver.views.adapter.PlacesAutoCompleteAdapter;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.models.CityMessage;
import com.vip.taxivip_driver.models.StateMessage;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class SignUp_fragment extends Fragment {


    private PlacesAutoCompleteAdapter mAdapter;
    private Button btn_continue;
    private SingInSignUpViewModel signupViewModel;
    private EditText email, lastName, firstName, mobile_no, password, confirm_password;
    private String deviceToken;
    private LoginPreference loginPreference;
    private CheckBox signUpTermsCondition_checkbox;
    private boolean checked;
    private ImageView iv_Back;
    private Spinner state_NameSp, spinner_city;
    private List<StateMessage> stateList;
    private List<CityMessage> cityMessageList;
    private int CityID;
    private int stateID;
    private ArrayAdapter<CityMessage> spinnerCityArrayAdapter;
    private View view;
    private AutoCompleteTextView autocompleteView;
    private TextView termsConditionTxt;

    private CountryCodePicker ccp;
    private String countryCode;
    private Context mContext;


    public SignUp_fragment() {
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext =context;


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signupViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        deviceToken = loginPreference.getDeviceToken();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.signup_fragment, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        // PrivacyTerms();
        Log.e(this.getClass().getName(), "FireBaseToken" + deviceToken);
        autocompleteView = view.findViewById(R.id.addressAutocomplete);
        iv_Back = view.findViewById(R.id.iv_Back);
        firstName = view.findViewById(R.id.ed_first_name);
        lastName = view.findViewById(R.id.ed_last_name);
        mobile_no = view.findViewById(R.id.ed_phone_number);
        email = view.findViewById(R.id.ed_email);
        password = view.findViewById(R.id.ed_password);
        confirm_password = view.findViewById(R.id.ed_confirm_password);
        signUpTermsCondition_checkbox = view.findViewById(R.id.signUpTermsCondition_checkbox);
        state_NameSp = view.findViewById(R.id.state_Name);
        spinner_city = view.findViewById(R.id.spinner_city);
        ccp = view.findViewById(R.id.ccp);
        termsConditionTxt = view.findViewById(R.id.termsConditionTxt);
        countryCode = ccp.getDefaultCountryCode();
        firstName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        lastName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
       /* firstName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        lastName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
*/
        //autocompleteView.setOnItemClickListener(this);
        autocompleteView.setAdapter(new PlacesAutoCompleteAdapter(getActivity(), R.layout.autocomplete_list_item));


        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode = ccp.getSelectedCountryCode();
                Log.e(this.getClass().getName(), "SELECTED_COUNTRY" + countryCode);
            }
        });

       /* spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String state=   spinner_city.getItemAtPosition(spinner_city.getSelectedItemPosition()).toString();

                CityID=cityMessageList.get(position).getId();
                Log.e(this.getClass().getName(),"CITY_Spinner_ID" + state + "CITY_ID" +CityID);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        state_NameSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String state=   state_NameSp.getItemAtPosition(state_NameSp.getSelectedItemPosition()).toString();
                 stateID=stateList.get(position).getId();
               // int stateId = state_NameSp.getId();
                Log.e(this.getClass().getName(),"State_Spiner_ID" + state + "STATEID"+stateID);
                //Toast.makeText(,state_Name,Toast.LENGTH_LONG).show();
                loadCityData(stateID);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
        iv_Back.setOnClickListener(v -> {
            getActivityNonNull().onBackPressed();
        });


        btn_continue = view.findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(view1 -> {
            String firstNameVal = firstName.getText().toString().trim();
            String lastNameVal = lastName.getText().toString().trim();
            String mobileVal = mobile_no.getText().toString().trim();
            String emailVal = email.getText().toString().trim();
            String passwordVal = password.getText().toString().trim();
            String confirmPassword = confirm_password.getText().toString().trim();
            String addressVal = autocompleteView.getText().toString();
            checked = signUpTermsCondition_checkbox.isChecked();
                if (isValidated(firstNameVal, lastNameVal, emailVal, confirmPassword, passwordVal, mobileVal, checked)) {
                    btn_continue.setClickable(false);
                    SingupRequest singupRequest = new SingupRequest();
                    singupRequest.setFirstname(firstNameVal);
                    singupRequest.setLastname(lastNameVal);
                    singupRequest.setEmail(emailVal);
                    singupRequest.setPassword(passwordVal);
                    singupRequest.setConfirm_password(confirmPassword);
                    singupRequest.setMobile(mobileVal);
                    singupRequest.setAgree_terms(checked);
                    singupRequest.setDevice_token(deviceToken);
                    singupRequest.setDevice_type(1);
                    singupRequest.setCity_id(addressVal);
                    singupRequest.setCountry_code(countryCode);

                    postSingUpCall(singupRequest);
                }
        });


        //loadStateData();

    }


    private void loadStateData() {
        signupViewModel.getStateResponse().observe(this, stateResponse -> {
            // Boolean error = stateResponse.ge;

            Log.e(this.getClass().getName(), "STATE_RESPONSE=" + stateResponse.size());
            if (stateResponse.size() > 0) {
                stateList = new ArrayList<>();
                for (int i = 0; i < stateResponse.size(); i++) {
                    StateMessage stateMessage = new StateMessage();
                    String name = stateResponse.get(i).getName();
                    int id = stateResponse.get(i).getId();
                    stateMessage.setName(name);
                    stateMessage.setId(id);
                    stateList.add(stateMessage);

                }
                ArrayAdapter<StateMessage> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, stateList);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                state_NameSp.setAdapter(spinnerArrayAdapter);
            }


        });
    }


    private void loadCityData(int stateId) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));

        signupViewModel.getCityResponse(stateId).observe(this, stateResponse -> {
            // cityMessageList.clear();
            myDialog.dismiss();
            Log.e(this.getClass().getName(), "STATE_RESPONSE=" + stateResponse.size());
            if (stateResponse.size() > 0) {

                cityMessageList = new ArrayList<>();
                for (int i = 0; i < stateResponse.size(); i++) {
                    CityMessage cityMessage = new CityMessage();
                    String name = stateResponse.get(i).getName();
                    int id = stateResponse.get(i).getId();
                    cityMessage.setName(name);
                    cityMessage.setId(id);
                    cityMessageList.add(cityMessage);
                }
                spinnerCityArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, cityMessageList);
                spinnerCityArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                spinner_city.setAdapter(spinnerCityArrayAdapter);
                spinnerCityArrayAdapter.notifyDataSetChanged();


            }


        });
    }


    private void postSingUpCall(SingupRequest singupRequest) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        signupViewModel.getSignupResponse(singupRequest)
                .observe(this, signupResponse -> {
                    btn_continue.setClickable(true);
                    myDialog.dismiss();

                    if (signupResponse.getError() != null && !signupResponse.getError()) {
                        Log.e(TAG, "SingUpResponse==" + signupResponse.getMessage());
                        CommonMethod.showToast(getActivity(), signupResponse.getMessage());
                        loginPreference.setOtpRegister(signupResponse.getResult().getOtp());
                        loginPreference.setRegister_id(signupResponse.getResult().getId());
                        loginPreference.setMobile(signupResponse.getResult().getMobile());
                        String otpVal = signupResponse.getResult().getOtp();
                        String mobile = signupResponse.getResult().getMobile();
                        String countryCodeVal = signupResponse.getResult().getCountryCode();
                        CallFragment(otpVal, mobile, countryCodeVal);

                    } else {
                        if (signupResponse.getMessage() != null) {
                            CommonMethod.showToast(getActivity(), signupResponse.getMessage());
                        } else {
                            CommonMethod.showToast(getActivity(), getString(R.string.internet_connection));

                        }
                    }
                });
    }


    private boolean isValidated(String firstNameVal, String lastNameVal, String emailVal, String confirmPassword, String passwordVal, String mobileVal, boolean checked) {
        String Regex = "[^\\d]";
        String PhoneDigits = mobileVal.replaceAll(Regex, "");
        if (TextUtils.isEmpty(firstNameVal)) {
            firstName.setError(getString(R.string.enter_first_name));
            firstName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(lastNameVal)) {
            lastName.setError(getString(R.string.enter_last_name));
            lastName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(emailVal) ||
                !android.util.Patterns.EMAIL_ADDRESS.matcher(emailVal).matches()) {
            email.setError(getString(R.string.email_not_valid));
            email.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(mobileVal)) {
            mobile_no.setError(getString(R.string.enter_valid_mobile_no));
            mobile_no.requestFocus();
            return false;
        } else if ((PhoneDigits.length() < 7) || (PhoneDigits.length() > 15)) {
            mobile_no.setError(getString(R.string.enter_valid_mobile_no));
            mobile_no.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(passwordVal)) {
            password.setError(getString(R.string.password_required), null);
            password.requestFocus();
            return false;
        } else if (!passwordVal.equals(confirmPassword)) {
            Toast.makeText(getActivity(), getString(R.string.password_did_match), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!checked) {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_term), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void PrivacyTerms() {
        String privacy_signup = "I read and agree to Terms and Conditions";
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(privacy_signup);
        ClickableSpan terms = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.blue));
            }

            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(getActivity(), Terms_Fragment.class);
                startActivity(intent);*/
            }
        };
        ssbuilder.setSpan(terms, privacy_signup.indexOf("Terms and Conditions")
                , privacy_signup.indexOf("Terms and Conditions") + String.valueOf("Terms and Conditions").length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        termsConditionTxt.setText(ssbuilder);
        termsConditionTxt.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private FragmentActivity getActivityNonNull() {
        if (super.getActivity() != null) {
            return super.getActivity();
        } else {
            throw new RuntimeException("null returned from getActivity()");
        }
    }


    private void CallFragment(String otpVal, String mobile, String countryCodeV) {
        OtpFragment otp_fragment = new OtpFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.OTP, otpVal);
        bundle.putString(Constants.MOBILE_NO, mobile);
        bundle.putString(Constants.COUNTRY_CODE, countryCodeV);
        otp_fragment.setArguments(bundle);
        CommonMethod.setSimpleFragment(otp_fragment, getActivityNonNull(), R.id.fragment_container_signup);
    }


}
