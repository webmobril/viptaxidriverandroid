package com.vip.taxivip_driver.views.fragments.profiles_fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.adapter.LanguageAdapter;
import com.vip.taxivip_driver.models.LanguageResponse;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.ProfileViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class AddLanguageFragment extends Fragment implements View.OnClickListener, LanguageAdapter.MyclickListnerLearning {

    private ProfileViewModel profileViewModel;
    private LoginPreference loginPreference;
    private int driverId;
    private LanguageAdapter languageAdapter;
    private String selectedLanguage;


    private ProgressBar post_progressbar;
    private ImageView iv_Back;
    private TextView txt_save;
    private RecyclerView recyclerViewLanguage;
    private Context mContext;
    private ArrayList<String> stringArrayList = new ArrayList<>();

    public AddLanguageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_language, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        recyclerViewLanguage = view.findViewById(R.id.recyclerViewLanguage);
        post_progressbar = view.findViewById(R.id.post_progressbar);
        iv_Back = view.findViewById(R.id.iv_Back);
        txt_save = view.findViewById(R.id.txt_save);
        txt_save.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
        getLanguageApi();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_Back) {
            Objects.requireNonNull(getActivity()).onBackPressed();
        } else if (v.getId() == R.id.txt_save) {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String commaSeparatedValues = String.join(", ", stringArrayList);
                Log.e(this.getClass().getName(), "ValueComma" + commaSeparatedValues);
                updateProfilePost(driverId, commaSeparatedValues);
            }


        }


    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void getLanguageApi() {
        post_progressbar.setVisibility(View.VISIBLE);
        profileViewModel.getLanguageViewModel().observe(this, languageResponse -> {
            post_progressbar.setVisibility(View.GONE);
            List<LanguageResponse.Result> listResponse = languageResponse.getResult();
            if (listResponse != null) {
                prepareRecyclerView(listResponse);
            }
        });

    }

    private void prepareRecyclerView(List<LanguageResponse.Result> resultList) {
        languageAdapter = new LanguageAdapter(getActivity(), resultList, this);
        recyclerViewLanguage.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewLanguage.setItemAnimator(new DefaultItemAnimator());
        recyclerViewLanguage.setAdapter(languageAdapter);
        languageAdapter.notifyDataSetChanged();
    }


    private void updateProfilePost(int driverId, String language_known) {
        post_progressbar.setVisibility(View.VISIBLE);
        profileViewModel.postUpdateAddLanguageViewModel(driverId, language_known)
                .observe(this, updateProfileResponse -> {
                    post_progressbar.setVisibility(View.GONE);
                    if (updateProfileResponse.getError() != null && !updateProfileResponse.getError()) {
                        Log.e(this.getClass().getName(), "Language_RESPONSE=" + updateProfileResponse.getResult().getLanguageKnown());

                        loginPreference.setLanguageKnown(updateProfileResponse.getResult().getLanguageKnown());
                        CommonMethod.showToast(mContext, updateProfileResponse.getMessage());
                        Objects.requireNonNull(getActivity()).onBackPressed();
                    } else if (updateProfileResponse.getMessage() != null){
                        CommonMethod.showToast(mContext, updateProfileResponse.getMessage());
                    } else {
                        CommonMethod.showAlert(getString(R.string.internet_connection),getActivity());
                    }
                });

    }


    @Override
    public void onViewClick(View view, ArrayList<String> nameType) {
        stringArrayList = nameType;
        Log.e(this.getClass().getName(), "Value_click " + nameType);
    }
}
