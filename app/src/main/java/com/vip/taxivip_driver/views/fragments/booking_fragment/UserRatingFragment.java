package com.vip.taxivip_driver.views.fragments.booking_fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.models.BookingDetailsResponse;
import com.vip.taxivip_driver.requestclass.ReviewRequest;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.models.SendReviewResponse;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.BookingViewModels;


public class UserRatingFragment extends Fragment implements View.OnClickListener {
    private ImageView iv_Back,badImageView,happyImgView,excellentImgView,userImage;
    private BookingViewModels bookingViewModels;
    private EditText ed_ratingComments;
    private Button btn_done;
    private LoginPreference loginPreference;
    private int driverId;
    private LinearLayout excellentLayout,happyLayout,badLayout;
    int ratingValue;
    private RatingBar driver_ratingBar;
    private TextView userNameTextView;

    public UserRatingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookingViewModels = new ViewModelProvider(this).get(BookingViewModels.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_rating, container, false);
        initViews(view);
        return view;
    }



    private void initViews(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        btn_done = view.findViewById(R.id.btn_done);
        userImage = view.findViewById(R.id.userImage);
        badImageView = view.findViewById(R.id.badImageView);
        happyImgView = view.findViewById(R.id.happyImgView);
        excellentImgView = view.findViewById(R.id.excellentImgView);
        ed_ratingComments = view.findViewById(R.id.ed_ratingComments);
        excellentLayout = view.findViewById(R.id.excellentLayout);
        happyLayout = view.findViewById(R.id.happyLayout);
        badLayout = view.findViewById(R.id.badLayout);
        driver_ratingBar = view.findViewById(R.id.driver_ratingBar);
        userNameTextView = view.findViewById(R.id.userNameTextView);


        driver_ratingBar.setOnRatingBarChangeListener((ratingBar, ratings, fromUser) -> {
            if (ratings < 1.0f)
                driver_ratingBar.setRating(1.0f);
        });
        getBookingDetailsApi(loginPreference.getBookingId());

        // click listener
        iv_Back.setOnClickListener(this);
        btn_done.setOnClickListener(this);
        excellentLayout.setOnClickListener(this);
        happyLayout.setOnClickListener(this);
        badLayout.setOnClickListener(this);
    }


    private void getBookingDetailsApi(int bookingId){
        bookingViewModels.getBookingDetailsViewModel(bookingId).observe(this, bookingDetailsResponse -> {
            if (bookingDetailsResponse.getError() != null && !bookingDetailsResponse.getError()){
                Log.e(this.getClass().getName(),"GetDetailsBooking" + bookingDetailsResponse.getResult());
                loginPreference.setUSER_REG_ID(bookingDetailsResponse.getResult().getUsers().getId());
                CommonMethod.loadImageResized(getActivity(),
                        Constants.IMAGE_URL + bookingDetailsResponse.getResult().getUsers().getUserImage(),userImage);
                userNameTextView.setText(bookingDetailsResponse.getResult().getUsers().getName());

            }
        });
    }



     private void postReviewApi(ReviewRequest reviewRequest){
         bookingViewModels.postReviewViewModel(reviewRequest).observe(this, new Observer<SendReviewResponse>() {
             @Override
             public void onChanged(SendReviewResponse sendReviewResponse) {
                 if (sendReviewResponse != null && sendReviewResponse.getError() != null && !sendReviewResponse.getError() ){
                     Log.e(this.getClass().getName(),"ReviewResponse" + sendReviewResponse);
                     CommonMethod.showToast(getActivity(), sendReviewResponse.getMessage());
                     loginPreference.setBookingId(0);

                 }  else {
                     CommonMethod.showToast(getActivity() ,getString(R.string.internet_connection));
                 }
             }
         });
     }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.iv_Back){
            getActivity().onBackPressed();
        }
        else if (v.getId() == R.id.btn_done){

            Log.e(this.getClass().getName(),"Values_" +ratingValue + " " +ed_ratingComments.getText().toString() + " RAting" +driver_ratingBar.getRating());


            ReviewRequest reviewRequest = new ReviewRequest();
            reviewRequest.setSender_id(driverId);
            // reciever id
            reviewRequest.setReceiver_id(loginPreference.getUSER_REG_ID());
            reviewRequest.setRating(String.valueOf(driver_ratingBar.getRating()));
            reviewRequest.setReview_message(ed_ratingComments.getText().toString());
            reviewRequest.setBooking_id(loginPreference.getBookingId());
            // role id always 2 for driver
            reviewRequest.setRoles_id(2);

            postReviewApi(reviewRequest);
        }
        else if (v.getId() == R.id.excellentLayout){
            ratingValue =3;
        }
        else if (v.getId() == R.id.happyLayout){
            ratingValue=2;

        }
        else if (v.getId() == R.id.badLayout){
            ratingValue=1;
        }
    }
}
