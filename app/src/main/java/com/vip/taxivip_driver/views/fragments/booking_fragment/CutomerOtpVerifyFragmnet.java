package com.vip.taxivip_driver.views.fragments.booking_fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;
import com.vip.taxivip_driver.views.activities.ContainerActivity;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;


public class CutomerOtpVerifyFragmnet extends Fragment {
    private static final String TAG = "OtpFragment";
    private Button btn_done;
    private String otp, mobile;
    int userid;
    private EditText ed_1, ed_2, ed_3, ed_4;
    SingInSignUpViewModel otpViewModel;
    private LoginPreference loginPreference;
    private TextView resend_otp;
    private ProgressBar post_progressbar;
    private Context mContext;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = getContext();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.customer_otp_verify_fragment, container, false);
        otpViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(getActivity());

        initView(view);
        AlertDialog();

        return view;
    }


    private void initView(View view) {
        ed_1 = view.findViewById(R.id.ed_1);
        ed_2 = view.findViewById(R.id.ed_2);
        ed_3 = view.findViewById(R.id.ed_3);
        ed_4 = view.findViewById(R.id.ed_4);

        post_progressbar = view.findViewById(R.id.post_progressbar);
        resend_otp = view.findViewById(R.id.txt_resend_otp);
        btn_done = view.findViewById(R.id.btn_done);
        btn_done.setOnClickListener(view12 ->
        {
            String otpVal = ed_1.getText().toString().trim() + ed_2.getText().toString().trim() + ed_3.getText().toString() + ed_4.getText().toString().trim();
            if (CommonMethod.isNetworkAvailable(mContext)) {
                if (otpVal != "") {

                }
            }
        });

        resend_otp.setOnClickListener(view1 -> {
                    if (CommonMethod.isNetworkAvailable(mContext)) {
                        CallResendOTPApi();
                    }
                }
        );

        ed_1.addTextChangedListener(new GenericTextWatcher(ed_1));
        ed_2.addTextChangedListener(new GenericTextWatcher(ed_2));
        ed_3.addTextChangedListener(new GenericTextWatcher(ed_3));
        ed_4.addTextChangedListener(new GenericTextWatcher(ed_4));

    }

    private void CallResendOTPApi() {
        post_progressbar.setVisibility(View.VISIBLE);
        userid = loginPreference.getRegister_id();
        otpViewModel.getResendOTP(userid).observe(this, resendOTPResponse -> {
            post_progressbar.setVisibility(View.GONE);
            if (resendOTPResponse.getError() != null && !resendOTPResponse.getError()) {
                Log.e(TAG, "resendOTPResponse" + resendOTPResponse.getMessage());
                ResendAlertDialog();
                Toast.makeText(getActivity(), resendOTPResponse.getMessage(), Toast.LENGTH_SHORT).show();
                loginPreference.setResend_otp(resendOTPResponse.getResult().getOtp());
            } else {
                Toast.makeText(getActivity(), resendOTPResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void ResendAlertDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_design);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button btnNo = dialog.findViewById(R.id.btn_ok);
        TextView tv_message = dialog.findViewById(R.id.txt_otp);
        tv_message.setText(loginPreference.getResend_otp());

        btnNo.setOnClickListener(view -> dialog.cancel());
        dialog.show();

    }

    private void AlertDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_design);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button btnNo = dialog.findViewById(R.id.btn_ok);
        TextView tv_message = dialog.findViewById(R.id.txt_otp);
        tv_message.setText(loginPreference.getOtpRegister());

        btnNo.setOnClickListener(view -> dialog.cancel());
        dialog.show();
    }

    private void callDocumentFragment() {
        Intent intent = new Intent(mContext, ContainerActivity.class);
        intent.putExtra(Constants.TYPE, Constants.DOCUMENT);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    public class GenericTextWatcher implements TextWatcher {
        private View view;

        public GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.ed_1:
                    if (text.length() == 1)
                        ed_2.requestFocus();
                    break;

                case R.id.ed_2:
                    if (text.length() == 1)
                        ed_3.requestFocus();
                    else if (text.length() == 0)
                        ed_1.requestFocus();
                    break;

                case R.id.ed_3:
                    if (text.length() == 1)
                        ed_4.requestFocus();
                    else if (text.length() == 0)
                        ed_2.requestFocus();
                    break;

                case R.id.ed_4:

                    if (text.length() == 0)
                        ed_3.requestFocus();
                    break;

            }
        }
    }

}
