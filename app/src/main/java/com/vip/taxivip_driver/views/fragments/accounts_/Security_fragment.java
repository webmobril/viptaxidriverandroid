package com.vip.taxivip_driver.views.fragments.accounts_;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.views.fragments.Step_verification_fragment;

public class Security_fragment extends Fragment implements View.OnClickListener {

    private ImageView iv_Back;
    private RelativeLayout layout_2_step_verification;

    public Security_fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.security_fragment,container,false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        iv_Back= view.findViewById(R.id.iv_Back);
        layout_2_step_verification = view.findViewById(R.id.layout_2_step_verification);
        layout_2_step_verification.setOnClickListener(this);
        iv_Back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.iv_Back) {
            getActivity().onBackPressed();
        }
        else if (v.getId() ==R.id.layout_2_step_verification){
            Step_verification_fragment fragment = new Step_verification_fragment();
            CommonMethod.setSimpleFragment(new Step_verification_fragment(), getActivity(), R.id.frame_about);
        }
    }


    /*private void CallFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_about, fragment);
        fragmentTransaction.commit();
    }*/
}
