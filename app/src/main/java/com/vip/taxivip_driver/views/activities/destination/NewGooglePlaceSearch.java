package com.vip.taxivip_driver.views.activities.destination;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.SetDestinationRequest;
import com.vip.taxivip_driver.views.adapter.PlacesAutoCompleteDestinationAdapter;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.HomeViewModel;


public class NewGooglePlaceSearch extends AppCompatActivity implements PlacesAutoCompleteDestinationAdapter.ClickListener {

    private Handler handler;
    private EditText txtDestination;
    private PlacesAutoCompleteDestinationAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;
    String strSelected = "";
    ImageView backArrow, imgDestClose;
    HomeViewModel homeViewModel;
    int driverId;
    private LoginPreference loginPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_destination);
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        Places.initialize(this, getResources().getString(R.string.google_maps_key));
        txtDestination = findViewById(R.id.search_et);

        recyclerView = findViewById(R.id.list_search);
        txtDestination.addTextChangedListener(filterTextWatcher);
        backArrow = findViewById(R.id.iv_Back);
        imgDestClose = findViewById(R.id.clearImg);
        loginPreference = new LoginPreference(getApplicationContext());
        driverId = loginPreference.getRegister_id();

        txtDestination.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                strSelected = "destination";
                imgDestClose.setVisibility(View.VISIBLE);
            } else {
                imgDestClose.setVisibility(View.GONE);
            }
        });

        imgDestClose.setOnClickListener(v -> {
            txtDestination.setText("");
            recyclerView.setVisibility(View.GONE);
            imgDestClose.setVisibility(View.GONE);
            txtDestination.requestFocus();
        });

        mAutoCompleteAdapter = new PlacesAutoCompleteDestinationAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();

        backArrow.setOnClickListener(v -> {
            finish();
        });


    }


    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            //imgDestClose.setVisibility(View.VISIBLE);
            //strSelected = "destination";
            if (!s.toString().equals("")) {

                Runnable runnable = () -> {
                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                    if (recyclerView.getVisibility() == View.GONE) {
                        recyclerView.setVisibility(View.VISIBLE);
                        imgDestClose.setVisibility(View.VISIBLE);
                        strSelected = "destination";
                    } else {
                        if (recyclerView.getVisibility() == View.VISIBLE) {
                            recyclerView.setVisibility(View.GONE);
                        }
                    }
                };
                if (handler != null) {
                    handler.removeCallbacksAndMessages(null);
                } else {
                    handler = new Handler();
                }
                handler.postDelayed(runnable, 1000);


            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // imgDestClose.setVisibility(View.VISIBLE);

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // imgDestClose.setVisibility(View.VISIBLE);
            //strSelected = "destination";
        }
    };

    @Override
    public void click(Place place) {
        recyclerView.setVisibility(View.GONE);
        String destinationAddress = place.getAddress();
        String destLatitudeLongitude = place.getLatLng().toString();
        String destLatitude = place.getLatLng().latitude + "";
        String destLongitude = place.getLatLng().longitude + "";
        txtDestination.setText(destinationAddress);
        setDestinationDialog(destLatitudeLongitude, destLatitude, destLongitude);
    }


    private void setDestinationDialog(String address, String latitude, String longitude) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, choice) -> {
            switch (choice) {
                case DialogInterface.BUTTON_POSITIVE:
                    SetDestinationRequest setDestinationRequest = new SetDestinationRequest();
                    setDestinationRequest.setUser_id(driverId);
                    setDestinationRequest.setLocation(address);
                    setDestinationRequest.setLat(latitude);
                    setDestinationRequest.setLng(longitude);

                    postSetDestinationApi(setDestinationRequest);


                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(NewGooglePlaceSearch.this);
        builder.setMessage(getString(R.string.are_you_sure_logout))
                .setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener).show();
    }


    private void postSetDestinationApi(SetDestinationRequest setDestinationRequest) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(this, getString(R.string.please_wait));
        homeViewModel.postDestinationViewModel(setDestinationRequest).observe(this, updateProfileResponse -> {
            myDialog.dismiss();
            String message = null;
            if (updateProfileResponse != null && updateProfileResponse.getError()) {
                message = updateProfileResponse.getMessage();
                CommonMethod.showAlert(message, NewGooglePlaceSearch.this);
            } else {
                assert updateProfileResponse != null;
                if (updateProfileResponse.getMessage() != null) {
                    CommonMethod.showAlert(message, NewGooglePlaceSearch.this);
                } else {
                    CommonMethod.showAlert(getString(R.string.internet_connection), NewGooglePlaceSearch.this);

                }
            }

        });
    }


}
