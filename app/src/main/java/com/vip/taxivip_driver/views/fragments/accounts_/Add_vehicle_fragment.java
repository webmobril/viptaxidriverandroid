package com.vip.taxivip_driver.views.fragments.accounts_;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;


public class Add_vehicle_fragment extends Fragment implements View.OnClickListener {
    private TextView tvTitleToolbar;
    private ImageView iv_Back;
    private Button btn_add_vehicle;
    private LoginPreference loginPreference;
    private int driverId;
    private EditText et_brand, et_model, et_year, et_color;
    private SingInSignUpViewModel signupViewModel;
    private Context mContext;

    public Add_vehicle_fragment() {
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_vehicle_fragment, container, false);
        signupViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        driverId = loginPreference.getRegister_id();
        initViews(view);

        return view;
    }

    private void initViews(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        tvTitleToolbar = view.findViewById(R.id.tvTitleToolbar);
        tvTitleToolbar.setText(R.string.vehicles);
        iv_Back.setOnClickListener(this);

        et_brand = view.findViewById(R.id.et_brand);
        et_model = view.findViewById(R.id.et_model);
        et_year = view.findViewById(R.id.et_year);
        et_color = view.findViewById(R.id.et_color);
        btn_add_vehicle = view.findViewById(R.id.btn_add_vehicle);
        btn_add_vehicle.setOnClickListener(this);
        postGetProfile(driverId);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.iv_Back) {
            getActivity().onBackPressed();
        } else if (v.getId() == R.id.btn_add_vehicle) {
            String brandCar = et_brand.getText().toString();
            String carModel = et_model.getText().toString();
            String carYear = et_year.getText().toString();
            String carColor = et_color.getText().toString();
            if (CommonMethod.isNetworkAvailable(mContext)) {
                if (isValidated(brandCar, carModel, carYear, carColor)) {
                    // VehicleModelUploadApi(driverId, brandCar, carModel, carYear, carColor);
                }
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
            }

        }
    }


    private void postGetProfile(int driverId) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        signupViewModel.postGetProfileViewModel(driverId).observe(this, updateProfileResponse -> {
            myDialog.dismiss();
            if (updateProfileResponse.getError() != null && !updateProfileResponse.getError()) {
                Log.e(this.getClass().getName(), "Get_ProfileModelResponse=" + updateProfileResponse.getError());

                if (updateProfileResponse.getResult().getDocuments() != null) {
                    et_brand.setText(updateProfileResponse.getResult().getDocuments().getCarName());
                    et_model.setText(updateProfileResponse.getResult().getDocuments().getModelNumber());
                    et_year.setText(updateProfileResponse.getResult().getDocuments().getCarYear());
                    et_color.setText(updateProfileResponse.getResult().getDocuments().getCarColor());

                }

            } else {
                CommonMethod.showToast(mContext, updateProfileResponse.getMessage());
            }
        });

    }

   /* private void VehicleModelUploadApi(int driverId, String carBrand, String  carModel, String carYear , String carColor) {
        post_progressbar.setVisibility(View.VISIBLE);
        profileViewModel.postVehicleModelMutable(driverId,carBrand,carModel ,carYear,carColor).observe(this, documentResponse -> {
            post_progressbar.setVisibility(View.GONE);
            if (documentResponse.getError() != null && !documentResponse.getError()){
                Log.e(this.getClass().getName(),"VehicleTypeResponse== " + documentResponse.getMessage());
                Toast.makeText(getActivity(),  documentResponse.getMessage(), Toast.LENGTH_SHORT).show();

                getActivity().onBackPressed();

            }
            else {
                Toast.makeText(getActivity(), "Error ", Toast.LENGTH_SHORT).show();
            }
        });
    }

*/

    private boolean isValidated(String carBrandVal, String carModelVal, String carYearVal, String carColorVal) {
        if (TextUtils.isEmpty(carBrandVal)) {
            et_brand.requestFocus();
            et_brand.setError(getString(R.string.enter_brand));
            return false;
        } else if (TextUtils.isEmpty(carModelVal)) {
            et_model.requestFocus();
            et_model.setError(getString(R.string.enter_model));
            return false;
        } else if (TextUtils.isEmpty(carYearVal)) {
            et_year.requestFocus();
            et_year.setError(getString(R.string.please_enter_year));
            return false;
        } else if (TextUtils.isEmpty(carColorVal)) {
            et_color.requestFocus();
            et_color.setError(getString(R.string.please_enter_color));
            return false;
        }
        return true;
    }


}
