package com.vip.taxivip_driver.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vip.taxivip_driver.R;


public class SecurityEnterPasswordFragment extends Fragment implements View.OnClickListener {
    public SecurityEnterPasswordFragment() {}
    private Button save;
    private EditText ed_password;
    private ImageView iv_Back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.enter_password, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        save = view.findViewById(R.id.btn_save);
        ed_password = view.findViewById(R.id.ed_password);
        iv_Back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_Back){
            getActivity().onBackPressed();
        }
    }
}
