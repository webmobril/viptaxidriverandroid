package com.vip.taxivip_driver.views.fragments.signup_views;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.requestclass.OtpVerifyRequest;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;
import com.vip.taxivip_driver.views.activities.ContainerActivity;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;


public class OtpFragment extends Fragment {
    private static final String TAG = "OtpFragment";
    private Button btn_done;
    private String otp, mobile;
    int userid;
    private EditText ed_1, ed_2, ed_3, ed_4;
    SingInSignUpViewModel otpViewModel;
    private LoginPreference loginPreference;
    private TextView resend_otp, txt_counter;
    private Context mContext;
    private String otpValType;
    private String mobileValType;
    private String countryCode;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = getContext();

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        otpViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(getActivity());
        userid = loginPreference.getRegister_id();
        Bundle args = getArguments();
        if (args != null) {
            otpValType = args.getString(Constants.OTP);
            mobileValType = args.getString(Constants.MOBILE_NO);
            countryCode = args.getString(Constants.COUNTRY_CODE);
            Log.e(this.getClass().getName(), "OTP_VAL" + otpValType + "MOBILE_TYPE" + mobileValType + "CONTRY_CODE" + countryCode);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.otp_fragment, container, false);

        initView(view);
        if (!otpValType.equals("signin")) {
            AlertDialog();
        } else {
            CallResendOTPApi();
        }


        return view;
    }


    private void initView(View view) {
        ed_1 = view.findViewById(R.id.ed_1);
        ed_2 = view.findViewById(R.id.ed_2);
        ed_3 = view.findViewById(R.id.ed_3);
        ed_4 = view.findViewById(R.id.ed_4);
        txt_counter = view.findViewById(R.id.txt_counter);

        resend_otp = view.findViewById(R.id.txt_resend_otp);
        btn_done = view.findViewById(R.id.btn_done);
        btn_done.setOnClickListener(view12 ->
        {
            String otpVal = ed_1.getText().toString().trim() + ed_2.getText().toString().trim() + ed_3.getText().toString() + ed_4.getText().toString().trim();
           // if (CommonMethod.isNetworkAvailable(mContext)) {
                if (!otpVal.equals("")) {
                    OTP_Response(otpVal);
                /*} else {
                    CommonMethod.showToast(getActivity(), "Please fill all details");
                }*/
            }
        });

        resend_otp.setOnClickListener(view1 -> {
           // String otpVal = ed_1.getText().toString().trim() + ed_2.getText().toString().trim() + ed_3.getText().toString() + ed_4.getText().toString().trim();

           // if (CommonMethod.isNetworkAvailable(mContext)) {
                // if (!otpVal.equals("")) {
                CallResendOTPApi();
               /* }else {
                    Toast.makeText(mContext, "Please fill all text", Toast.LENGTH_SHORT).show();

                }*/
            /*} else {
                CommonMethod.showToast(getActivity(), getString(R.string.internet_connection));
            }*/

        });

        ed_1.addTextChangedListener(new GenericTextWatcher(ed_1));
        ed_2.addTextChangedListener(new GenericTextWatcher(ed_2));
        ed_3.addTextChangedListener(new GenericTextWatcher(ed_3));
        ed_4.addTextChangedListener(new GenericTextWatcher(ed_4));


    }

    private void CallResendOTPApi() {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        otpViewModel.getResendOTP(userid).observe(this, resendOTPResponse -> {
            myDialog.dismiss();
            if (resendOTPResponse.getError() != null && !resendOTPResponse.getError()) {
                Log.e(TAG, "resendOTPResponse" + resendOTPResponse.getMessage());
                loginPreference.setResend_otp(resendOTPResponse.getResult().getOtp());
                CommonMethod.showToast(getActivity(), resendOTPResponse.getMessage());
                new Handler().postAtTime(() -> ResendAlertDialog(), 1000);

            }
            else {
               if (resendOTPResponse.getMessage() != null) {
                   CommonMethod.showToast(getActivity(), resendOTPResponse.getMessage());
               } else {
                   CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());
               }
            }
        });
    }

    private void OTP_Response(String otpVal) {

        mobile = mobileValType;
        Log.e(this.getClass().getName(), " CountryCoe" + countryCode);
        OtpVerifyRequest otpVerifyRequest = new OtpVerifyRequest();
        otpVerifyRequest.setMobile(mobile);
        otpVerifyRequest.setOtp(otpVal);
        otpVerifyRequest.setRoles_id(5);
        otpVerifyRequest.setCountry_code(countryCode);
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));

        otpViewModel.getOTP(otpVerifyRequest).observe(this, otpResponse -> {
            myDialog.dismiss();
            if (otpResponse.getError() != null && !otpResponse.getError()) {
                if (otpResponse.getMessage().contains("Invalid OTP")) {
                    CommonMethod.showToast(getActivity(), otpResponse.getMessage());
                } else {
                    if (otpResponse.getResult() != null && otpResponse.getResult().getUser() != null)
                        CommonMethod.showToast(getActivity(), otpResponse.getMessage());
                    loginPreference.setRegisterToken(otpResponse.getResult().getAccessToken());
                    assert otpResponse.getResult() != null;
                    loginPreference.setRegister_id(otpResponse.getResult().getUser().getId());
                    loginPreference.setMobile(otpResponse.getResult().getUser().getMobile());
                    loginPreference.setFirstName(otpResponse.getResult().getUser().getFirstName());
                    loginPreference.setLastName(otpResponse.getResult().getUser().getLastName());
                    loginPreference.setUser_email(otpResponse.getResult().getUser().getEmail());
                    loginPreference.setUserImage(otpResponse.getResult().getUser().getUserImage());
                    loginPreference.setDescribeYourself(otpResponse.getResult().getUser().getDescribeYourself());
                    loginPreference.setLanguageKnown(otpResponse.getResult().getUser().getLanguageKnown());
                    loginPreference.setWhereFrom(otpResponse.getResult().getUser().getHomeaddress());
                    loginPreference.setCityName(otpResponse.getResult().getUser().getCityId());
                    loginPreference.setVerifyDoc(otpResponse.getResult().getUser().getVerify());
                    callDocumentFragment();
                }


            } else if (otpResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(), otpResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection),getActivity());
            }
        });
    }

    private void ResendAlertDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_design);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button btnNo = dialog.findViewById(R.id.btn_ok);
        TextView tv_message = dialog.findViewById(R.id.txt_otp);
        tv_message.setText(loginPreference.getResend_otp());

        btnNo.setOnClickListener(view -> {
            dialog.cancel();
            callTimer();
        });
        dialog.show();

    }

    private void AlertDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_design);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button btnNo = dialog.findViewById(R.id.btn_ok);
        TextView tv_message = dialog.findViewById(R.id.txt_otp);
        tv_message.setText(loginPreference.getOtpRegister());

        btnNo.setOnClickListener(view -> {
            dialog.cancel();
            callTimer();
        });
        dialog.show();
    }

    private void callDocumentFragment() {
        Intent intent = new Intent(mContext, ContainerActivity.class);
        intent.putExtra(Constants.TYPE, Constants.DOCUMENT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    private void callTimer() {

        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                txt_counter.setText("" + millisUntilFinished / 1000);
                resend_otp.setClickable(false);
            }

            public void onFinish() {
                txt_counter.setText("0.0");
                resend_otp.setClickable(true);
                resend_otp.setTextColor(Color.parseColor("#ff00ddff"));
            }

        }.start();
    }


    public class GenericTextWatcher implements TextWatcher {
        private View view;

        public GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.ed_1:
                    if (text.length() == 1)
                        ed_2.requestFocus();
                    break;

                case R.id.ed_2:
                    if (text.length() == 1)
                        ed_3.requestFocus();
                    else if (text.length() == 0)
                        ed_1.requestFocus();
                    break;

                case R.id.ed_3:
                    if (text.length() == 1)
                        ed_4.requestFocus();
                    else if (text.length() == 0)
                        ed_2.requestFocus();
                    break;

                case R.id.ed_4:

                    if (text.length() == 0)
                        ed_3.requestFocus();
                    break;

            }
        }
    }

}
