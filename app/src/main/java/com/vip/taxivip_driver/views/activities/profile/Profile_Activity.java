package com.vip.taxivip_driver.views.activities.profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.activities.ContainerActivity;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.views.fragments.accounts_.GetHelpFragment;
import com.vip.taxivip_driver.views.fragments.payments.PaymentHistory_fragment;
import com.vip.taxivip_driver.views.fragments.profiles_fragments.Details_profile_fragment;
import com.vip.taxivip_driver.views.activities.account.Account_Activity;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.viewmodels.SingInSignUpViewModel;


public class Profile_Activity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout lay_payment, lay_profile, lay_account;
    private ImageView iv_Back, driver_icon_profile;
    private LoginPreference loginPreference;
    private SingInSignUpViewModel signupViewModel;
    private int driverId;
    private TextView helpTextView;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        signupViewModel = new ViewModelProvider(this).get(SingInSignUpViewModel.class);
        loginPreference = new LoginPreference(this);
        driverId = loginPreference.getRegister_id();
        initView();
    }

    private void initView() {
        iv_Back = findViewById(R.id.iv_Back);
        helpTextView = findViewById(R.id.helpTextView);
        lay_payment = findViewById(R.id.layout_payment);
        lay_profile = findViewById(R.id.layout_profile);
        lay_account = findViewById(R.id.layout_account);
        driver_icon_profile = findViewById(R.id.driver_icon_profile);
        iv_Back.setOnClickListener(this);
        lay_payment.setOnClickListener(this);
        lay_profile.setOnClickListener(this);
        lay_account.setOnClickListener(this);
        helpTextView.setOnClickListener(this);
        postGetProfile(driverId);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_Back:
                CommonMethod.callOnlyActivity(this, ContainerActivity.class);
                break;
            case R.id.layout_account:
                CommonMethod.callOnlyActivity(this, Account_Activity.class);
                break;
            case R.id.layout_profile:
                CommonMethod.setSimpleFragment(new Details_profile_fragment(),this,R.id.frame_profile);
                break;

            case R.id.layout_payment:
                CommonMethod.setSimpleFragment(new PaymentHistory_fragment(),this,R.id.frame_profile);
                break;
            case R.id.helpTextView:
                CommonMethod.setSimpleFragment(new GetHelpFragment(),this,R.id.frame_profile);
                break;
        }
    }


    private void postGetProfile(int driverId) {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(Profile_Activity.this, getString(R.string.please_wait));
        signupViewModel.postGetProfileViewModel(driverId).observe(this, updateProfileResponse -> {
            myDialog.dismiss();
            if (updateProfileResponse.getError() != null && !updateProfileResponse.getError()) {
                if (updateProfileResponse.getResult() != null) {
                    loginPreference.setUserImage(updateProfileResponse.getResult().getUserImage());
                    CommonMethod.loadImageResized(Profile_Activity.this,
                            Constants.IMAGE_URL + updateProfileResponse.getResult().getUserImage(), driver_icon_profile);

                    if (updateProfileResponse.getResult().getDocuments() != null) {
                        String registration_verified = updateProfileResponse.getResult().getDocuments().getRegistrationImg();
                        String permitImage = updateProfileResponse.getResult().getDocuments().getPermitImage();
                        String criminal_record = updateProfileResponse.getResult().getDocuments().getCriminalRecordImg();
                        String driverLicence = updateProfileResponse.getResult().getDocuments().getDlImage();
                        String policeRecord = updateProfileResponse.getResult().getDocuments().getPoliceRecordImg();
                        String carInsurance = updateProfileResponse.getResult().getDocuments().getInsuranceImage();
                        if (driverLicence != null) {
                            loginPreference.setDocumentImage(driverLicence);
                        }
                        if (registration_verified != null) {
                            loginPreference.setDocumentRegImage(registration_verified);
                        }
                        if (carInsurance != null) {
                            loginPreference.setDocumentInsImage(carInsurance);
                        }
                        if (permitImage != null) {
                            loginPreference.setDocumentVpImage(permitImage);
                        }
                        if (criminal_record != null) {
                            loginPreference.setDocumentCrimImage(criminal_record);
                        }
                        if (policeRecord != null) {
                            loginPreference.setDocumentPolicImage(policeRecord);
                        }
                    }
                    if (updateProfileResponse.getResult().getDocuments() != null) {
                        if (updateProfileResponse.getResult().getDocuments().getViehcleType() != null) {
                            loginPreference.setVehicleType(updateProfileResponse.getResult().getDocuments().getViehcleType().getName());
                        }
                    }
                }

            } else if (updateProfileResponse.getMessage() != null){
                CommonMethod.showToast(this,updateProfileResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection),this);
            }
        });

    }

    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

}
