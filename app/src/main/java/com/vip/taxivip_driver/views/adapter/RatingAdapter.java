package com.vip.taxivip_driver.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.vip.taxivip_driver.R;

import java.util.List;

public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.RatingHolder> {

    List<String> list;

    @NonNull
    @Override
    public RatingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_rating_list, parent, false);
        return new RatingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RatingHolder extends RecyclerView.ViewHolder {
        public RatingHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
