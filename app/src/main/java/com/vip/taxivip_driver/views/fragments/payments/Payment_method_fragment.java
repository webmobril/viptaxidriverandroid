package com.vip.taxivip_driver.views.fragments.payments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.CommonMethod;


public class Payment_method_fragment extends Fragment implements View.OnClickListener {

    private ImageView iv_Back;
    private TextView txt_card_type;
    public Payment_method_fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_mthod_fragment,container,false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        txt_card_type = view.findViewById(R.id.txt_card_type);
        txt_card_type.setOnClickListener(view1 -> {
            CommonMethod.setSimpleFragment(new Add_card_fragment(),getActivity(),R.id.frame_about);
        });

            iv_Back = view.findViewById(R.id.iv_Back);
            iv_Back.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_Back){
            getActivity().onBackPressed();
        }
    }

}
