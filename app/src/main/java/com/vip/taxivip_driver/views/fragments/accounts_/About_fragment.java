package com.vip.taxivip_driver.views.fragments.accounts_;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.viewmodels.AccountViewModel;

import java.util.Objects;

public class About_fragment extends Fragment implements View.OnClickListener {

    private TextView tvTitleToolbar, about_txt;
    private ImageView iv_Back;
    private AccountViewModel accountViewModel;

    public About_fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_fragment, container, false);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);

        initViews(view);
        return view;

    }

    private void initViews(View view) {
        iv_Back = view.findViewById(R.id.iv_Back);
        tvTitleToolbar = view.findViewById(R.id.tvTitleToolbar);
        tvTitleToolbar.setText(R.string.about);
        about_txt = view.findViewById(R.id.about_txt);
        iv_Back.setOnClickListener(this);
        getAboutResponse();

    }


    private void getAboutResponse() {
        ProgressDialog myDialog = CommonMethod.showProgressDialog(getActivity(), getString(R.string.please_wait));
        accountViewModel.getAboutUSViewModel(1).observe(this, aboutUsResponse -> {
            myDialog.dismiss();
            if (aboutUsResponse.getError() != null && !aboutUsResponse.getError()) {
                about_txt.setText(aboutUsResponse.getResult().getContentEnglish());
            } else if (aboutUsResponse.getMessage() != null){
                CommonMethod.showToast(getActivity(), aboutUsResponse.getMessage());
            } else {
                CommonMethod.showAlert(getString(R.string.internet_connection), getActivity());

            }
        });


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_Back) {
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
    }
}
