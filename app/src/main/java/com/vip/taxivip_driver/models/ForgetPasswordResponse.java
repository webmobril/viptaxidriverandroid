package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgetPasswordResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("txtpassword")
        @Expose
        private Object txtpassword;
        @SerializedName("fb_id")
        @Expose
        private Object fbId;
        @SerializedName("g_id")
        @Expose
        private Object gId;
        @SerializedName("business_name")
        @Expose
        private Object businessName;
        @SerializedName("homeaddress")
        @Expose
        private Object homeaddress;
        @SerializedName("officeaddress")
        @Expose
        private Object officeaddress;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("latitude_home")
        @Expose
        private String latitudeHome;
        @SerializedName("longitude_home")
        @Expose
        private String longitudeHome;
        @SerializedName("cur_lat")
        @Expose
        private String curLat;
        @SerializedName("cur_lng")
        @Expose
        private String curLng;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("country_id")
        @Expose
        private Object countryId;
        @SerializedName("state_id")
        @Expose
        private Object stateId;
        @SerializedName("city_id")
        @Expose
        private Object cityId;
        @SerializedName("agree_terms")
        @Expose
        private Integer agreeTerms;
        @SerializedName("upgraded_business")
        @Expose
        private Integer upgradedBusiness;
        @SerializedName("user_image")
        @Expose
        private String userImage;
        @SerializedName("roles_id")
        @Expose
        private Integer rolesId;
        @SerializedName("verify")
        @Expose
        private Integer verify;
        @SerializedName("user_thumb_verification")
        @Expose
        private Integer userThumbVerification;
        @SerializedName("email_verify")
        @Expose
        private Integer emailVerify;
        @SerializedName("verify_token")
        @Expose
        private Object verifyToken;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("live")
        @Expose
        private Integer live;
        @SerializedName("ongoing_status")
        @Expose
        private Integer ongoingStatus;
        @SerializedName("tax_number")
        @Expose
        private Object taxNumber;
        @SerializedName("company_id")
        @Expose
        private Object companyId;
        @SerializedName("taxi_id")
        @Expose
        private Object taxiId;
        @SerializedName("taxi_number")
        @Expose
        private Object taxiNumber;
        @SerializedName("login_type")
        @Expose
        private Integer loginType;
        @SerializedName("profit_ratio")
        @Expose
        private Integer profitRatio;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("otp_time")
        @Expose
        private String otpTime;
        @SerializedName("stripe_customer_id")
        @Expose
        private Object stripeCustomerId;
        @SerializedName("auth_token")
        @Expose
        private String authToken;
        @SerializedName("verify_status")
        @Expose
        private Integer verifyStatus;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getTxtpassword() {
            return txtpassword;
        }

        public void setTxtpassword(Object txtpassword) {
            this.txtpassword = txtpassword;
        }

        public Object getFbId() {
            return fbId;
        }

        public void setFbId(Object fbId) {
            this.fbId = fbId;
        }

        public Object getGId() {
            return gId;
        }

        public void setGId(Object gId) {
            this.gId = gId;
        }

        public Object getBusinessName() {
            return businessName;
        }

        public void setBusinessName(Object businessName) {
            this.businessName = businessName;
        }

        public Object getHomeaddress() {
            return homeaddress;
        }

        public void setHomeaddress(Object homeaddress) {
            this.homeaddress = homeaddress;
        }

        public Object getOfficeaddress() {
            return officeaddress;
        }

        public void setOfficeaddress(Object officeaddress) {
            this.officeaddress = officeaddress;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitudeHome() {
            return latitudeHome;
        }

        public void setLatitudeHome(String latitudeHome) {
            this.latitudeHome = latitudeHome;
        }

        public String getLongitudeHome() {
            return longitudeHome;
        }

        public void setLongitudeHome(String longitudeHome) {
            this.longitudeHome = longitudeHome;
        }

        public String getCurLat() {
            return curLat;
        }

        public void setCurLat(String curLat) {
            this.curLat = curLat;
        }

        public String getCurLng() {
            return curLng;
        }

        public void setCurLng(String curLng) {
            this.curLng = curLng;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Object getCountryId() {
            return countryId;
        }

        public void setCountryId(Object countryId) {
            this.countryId = countryId;
        }

        public Object getStateId() {
            return stateId;
        }

        public void setStateId(Object stateId) {
            this.stateId = stateId;
        }

        public Object getCityId() {
            return cityId;
        }

        public void setCityId(Object cityId) {
            this.cityId = cityId;
        }

        public Integer getAgreeTerms() {
            return agreeTerms;
        }

        public void setAgreeTerms(Integer agreeTerms) {
            this.agreeTerms = agreeTerms;
        }

        public Integer getUpgradedBusiness() {
            return upgradedBusiness;
        }

        public void setUpgradedBusiness(Integer upgradedBusiness) {
            this.upgradedBusiness = upgradedBusiness;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public Integer getRolesId() {
            return rolesId;
        }

        public void setRolesId(Integer rolesId) {
            this.rolesId = rolesId;
        }

        public Integer getVerify() {
            return verify;
        }

        public void setVerify(Integer verify) {
            this.verify = verify;
        }

        public Integer getUserThumbVerification() {
            return userThumbVerification;
        }

        public void setUserThumbVerification(Integer userThumbVerification) {
            this.userThumbVerification = userThumbVerification;
        }

        public Integer getEmailVerify() {
            return emailVerify;
        }

        public void setEmailVerify(Integer emailVerify) {
            this.emailVerify = emailVerify;
        }

        public Object getVerifyToken() {
            return verifyToken;
        }

        public void setVerifyToken(Object verifyToken) {
            this.verifyToken = verifyToken;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getLive() {
            return live;
        }

        public void setLive(Integer live) {
            this.live = live;
        }

        public Integer getOngoingStatus() {
            return ongoingStatus;
        }

        public void setOngoingStatus(Integer ongoingStatus) {
            this.ongoingStatus = ongoingStatus;
        }

        public Object getTaxNumber() {
            return taxNumber;
        }

        public void setTaxNumber(Object taxNumber) {
            this.taxNumber = taxNumber;
        }

        public Object getCompanyId() {
            return companyId;
        }

        public void setCompanyId(Object companyId) {
            this.companyId = companyId;
        }

        public Object getTaxiId() {
            return taxiId;
        }

        public void setTaxiId(Object taxiId) {
            this.taxiId = taxiId;
        }

        public Object getTaxiNumber() {
            return taxiNumber;
        }

        public void setTaxiNumber(Object taxiNumber) {
            this.taxiNumber = taxiNumber;
        }

        public Integer getLoginType() {
            return loginType;
        }

        public void setLoginType(Integer loginType) {
            this.loginType = loginType;
        }

        public Integer getProfitRatio() {
            return profitRatio;
        }

        public void setProfitRatio(Integer profitRatio) {
            this.profitRatio = profitRatio;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getOtpTime() {
            return otpTime;
        }

        public void setOtpTime(String otpTime) {
            this.otpTime = otpTime;
        }

        public Object getStripeCustomerId() {
            return stripeCustomerId;
        }

        public void setStripeCustomerId(Object stripeCustomerId) {
            this.stripeCustomerId = stripeCustomerId;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public Integer getVerifyStatus() {
            return verifyStatus;
        }

        public void setVerifyStatus(Integer verifyStatus) {
            this.verifyStatus = verifyStatus;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }
}
