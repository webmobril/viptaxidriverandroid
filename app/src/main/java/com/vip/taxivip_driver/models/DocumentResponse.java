package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DocumentResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }
    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("adhar_number")
        @Expose
        private String adharNumber;
        @SerializedName("pancard_image")
        @Expose
        private String pancardImage;
        @SerializedName("pancard_back_image")
        @Expose
        private String pancardBackImage;
        @SerializedName("adhar_verified_status")
        @Expose
        private Integer adharVerifiedStatus;
        @SerializedName("dl_image")
        @Expose
        private String dlImage;
        @SerializedName("dl_back_image")
        @Expose
        private String dlBackImage;
        @SerializedName("dl_number")
        @Expose
        private String dlNumber;
        @SerializedName("dl_expiry_date")
        @Expose
        private String dlExpiryDate;
        @SerializedName("dl_verified_status")
        @Expose
        private Integer dlVerifiedStatus;
        @SerializedName("insurance_image")
        @Expose
        private String insuranceImage;
        @SerializedName("insurance_back_image")
        @Expose
        private String insuranceBackImage;
        @SerializedName("insurance_number")
        @Expose
        private String insuranceNumber;
        @SerializedName("insurance_expiry_date")
        @Expose
        private String insuranceExpiryDate;
        @SerializedName("insurance_verified_status")
        @Expose
        private Integer insuranceVerifiedStatus;
        @SerializedName("registration_img")
        @Expose
        private String registrationImg;
        @SerializedName("registration_back_img")
        @Expose
        private String registrationBackImg;
        @SerializedName("registration_number")
        @Expose
        private String registrationNumber;
        @SerializedName("registration_expiry_date")
        @Expose
        private String registrationExpiryDate;
        @SerializedName("registration_verified_status")
        @Expose
        private Integer registrationVerifiedStatus;
        @SerializedName("permit_image")
        @Expose
        private String permitImage;
        @SerializedName("permit_expiry_date")
        @Expose
        private String permitExpiryDate;
        @SerializedName("permit_verified_status")
        @Expose
        private Integer permitVerifiedStatus;
        @SerializedName("criminal_record_img")
        @Expose
        private String criminalRecordImg;
        @SerializedName("criminal_verified_status")
        @Expose
        private Integer criminalVerifiedStatus;
        @SerializedName("police_record_img")
        @Expose
        private String policeRecordImg;
        @SerializedName("police_verified_status")
        @Expose
        private Integer policeVerifiedStatus;
        @SerializedName("model_number")
        @Expose
        private String modelNumber;
        @SerializedName("car_name")
        @Expose
        private String carName;
        @SerializedName("car_year")
        @Expose
        private String carYear;
        @SerializedName("car_color")
        @Expose
        private String carColor;
        @SerializedName("car_number")
        @Expose
        private String carNumber;
        @SerializedName("viehcle_type_id")
        @Expose
        private Integer viehcleTypeId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getAdharNumber() {
            return adharNumber;
        }

        public void setAdharNumber(String adharNumber) {
            this.adharNumber = adharNumber;
        }

        public String getPancardImage() {
            return pancardImage;
        }

        public void setPancardImage(String pancardImage) {
            this.pancardImage = pancardImage;
        }

        public String getPancardBackImage() {
            return pancardBackImage;
        }

        public void setPancardBackImage(String pancardBackImage) {
            this.pancardBackImage = pancardBackImage;
        }

        public Integer getAdharVerifiedStatus() {
            return adharVerifiedStatus;
        }

        public void setAdharVerifiedStatus(Integer adharVerifiedStatus) {
            this.adharVerifiedStatus = adharVerifiedStatus;
        }

        public String getDlImage() {
            return dlImage;
        }

        public void setDlImage(String dlImage) {
            this.dlImage = dlImage;
        }

        public String getDlBackImage() {
            return dlBackImage;
        }

        public void setDlBackImage(String dlBackImage) {
            this.dlBackImage = dlBackImage;
        }

        public String getDlNumber() {
            return dlNumber;
        }

        public void setDlNumber(String dlNumber) {
            this.dlNumber = dlNumber;
        }

        public String getDlExpiryDate() {
            return dlExpiryDate;
        }

        public void setDlExpiryDate(String dlExpiryDate) {
            this.dlExpiryDate = dlExpiryDate;
        }

        public Integer getDlVerifiedStatus() {
            return dlVerifiedStatus;
        }

        public void setDlVerifiedStatus(Integer dlVerifiedStatus) {
            this.dlVerifiedStatus = dlVerifiedStatus;
        }

        public String getInsuranceImage() {
            return insuranceImage;
        }

        public void setInsuranceImage(String insuranceImage) {
            this.insuranceImage = insuranceImage;
        }

        public String getInsuranceBackImage() {
            return insuranceBackImage;
        }

        public void setInsuranceBackImage(String insuranceBackImage) {
            this.insuranceBackImage = insuranceBackImage;
        }

        public String getInsuranceNumber() {
            return insuranceNumber;
        }

        public void setInsuranceNumber(String insuranceNumber) {
            this.insuranceNumber = insuranceNumber;
        }

        public Object getInsuranceExpiryDate() {
            return insuranceExpiryDate;
        }

        public void setInsuranceExpiryDate(String insuranceExpiryDate) {
            this.insuranceExpiryDate = insuranceExpiryDate;
        }

        public Integer getInsuranceVerifiedStatus() {
            return insuranceVerifiedStatus;
        }

        public void setInsuranceVerifiedStatus(Integer insuranceVerifiedStatus) {
            this.insuranceVerifiedStatus = insuranceVerifiedStatus;
        }

        public String getRegistrationImg() {
            return registrationImg;
        }

        public void setRegistrationImg(String registrationImg) {
            this.registrationImg = registrationImg;
        }

        public String getRegistrationBackImg() {
            return registrationBackImg;
        }

        public void setRegistrationBackImg(String registrationBackImg) {
            this.registrationBackImg = registrationBackImg;
        }

        public String getRegistrationNumber() {
            return registrationNumber;
        }

        public void setRegistrationNumber(String registrationNumber) {
            this.registrationNumber = registrationNumber;
        }

        public String getRegistrationExpiryDate() {
            return registrationExpiryDate;
        }

        public void setRegistrationExpiryDate(String registrationExpiryDate) {
            this.registrationExpiryDate = registrationExpiryDate;
        }

        public Integer getRegistrationVerifiedStatus() {
            return registrationVerifiedStatus;
        }

        public void setRegistrationVerifiedStatus(Integer registrationVerifiedStatus) {
            this.registrationVerifiedStatus = registrationVerifiedStatus;
        }

        public String getPermitImage() {
            return permitImage;
        }

        public void setPermitImage(String permitImage) {
            this.permitImage = permitImage;
        }

        public String getPermitExpiryDate() {
            return permitExpiryDate;
        }

        public void setPermitExpiryDate(String permitExpiryDate) {
            this.permitExpiryDate = permitExpiryDate;
        }

        public Integer getPermitVerifiedStatus() {
            return permitVerifiedStatus;
        }

        public void setPermitVerifiedStatus(Integer permitVerifiedStatus) {
            this.permitVerifiedStatus = permitVerifiedStatus;
        }

        public String getCriminalRecordImg() {
            return criminalRecordImg;
        }

        public void setCriminalRecordImg(String criminalRecordImg) {
            this.criminalRecordImg = criminalRecordImg;
        }

        public Integer getCriminalVerifiedStatus() {
            return criminalVerifiedStatus;
        }

        public void setCriminalVerifiedStatus(Integer criminalVerifiedStatus) {
            this.criminalVerifiedStatus = criminalVerifiedStatus;
        }

        public String getPoliceRecordImg() {
            return policeRecordImg;
        }

        public void setPoliceRecordImg(String policeRecordImg) {
            this.policeRecordImg = policeRecordImg;
        }

        public Integer getPoliceVerifiedStatus() {
            return policeVerifiedStatus;
        }

        public void setPoliceVerifiedStatus(Integer policeVerifiedStatus) {
            this.policeVerifiedStatus = policeVerifiedStatus;
        }

        public String getModelNumber() {
            return modelNumber;
        }

        public void setModelNumber(String modelNumber) {
            this.modelNumber = modelNumber;
        }

        public String getCarName() {
            return carName;
        }

        public void setCarName(String carName) {
            this.carName = carName;
        }

        public String getCarYear() {
            return carYear;
        }

        public void setCarYear(String carYear) {
            this.carYear = carYear;
        }

        public String getCarColor() {
            return carColor;
        }

        public void setCarColor(String carColor) {
            this.carColor = carColor;
        }

        public String getCarNumber() {
            return carNumber;
        }

        public void setCarNumber(String carNumber) {
            this.carNumber = carNumber;
        }

        public Integer getViehcleTypeId() {
            return viehcleTypeId;
        }

        public void setViehcleTypeId(Integer viehcleTypeId) {
            this.viehcleTypeId = viehcleTypeId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}
