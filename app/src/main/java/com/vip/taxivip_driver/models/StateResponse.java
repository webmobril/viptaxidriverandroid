package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StateResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private List<StateMessgeResponse> message = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<StateMessgeResponse> getMessage() {
        return message;
    }

    public void setMessage(List<StateMessgeResponse> message) {
        this.message = message;
    }



}
