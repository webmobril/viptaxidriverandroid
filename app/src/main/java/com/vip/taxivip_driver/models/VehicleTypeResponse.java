package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VehicleTypeResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }



    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("icon")
        @Expose
        private Object icon;
        @SerializedName("d_from")
        @Expose
        private String dFrom;
        @SerializedName("d_to")
        @Expose
        private String dTo;
        @SerializedName("d_price")
        @Expose
        private Integer dPrice;
        @SerializedName("d_meter")
        @Expose
        private String dMeter;
        @SerializedName("d_meter_amount")
        @Expose
        private Double dMeterAmount;
        @SerializedName("d_second")
        @Expose
        private String dSecond;
        @SerializedName("d_second_amount")
        @Expose
        private Double dSecondAmount;
        @SerializedName("d_distance_under")
        @Expose
        private String dDistanceUnder;
        @SerializedName("d_amount_under")
        @Expose
        private Integer dAmountUnder;
        @SerializedName("d_basic_price")
        @Expose
        private Integer dBasicPrice;
        @SerializedName("n_from")
        @Expose
        private String nFrom;
        @SerializedName("n_to")
        @Expose
        private String nTo;
        @SerializedName("n_price")
        @Expose
        private Integer nPrice;
        @SerializedName("n_meter")
        @Expose
        private String nMeter;
        @SerializedName("n_meter_amount")
        @Expose
        private Double nMeterAmount;
        @SerializedName("n_second")
        @Expose
        private String nSecond;
        @SerializedName("n_second_amount")
        @Expose
        private Double nSecondAmount;
        @SerializedName("n_distance_under")
        @Expose
        private String nDistanceUnder;
        @SerializedName("n_amount_under")
        @Expose
        private Integer nAmountUnder;
        @SerializedName("n_basic_price")
        @Expose
        private Integer nBasicPrice;
        @SerializedName("status")
        @Expose
        private Integer status;

        private boolean isChecked = false;

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getIcon() {
            return icon;
        }

        public void setIcon(Object icon) {
            this.icon = icon;
        }

        public String getDFrom() {
            return dFrom;
        }

        public void setDFrom(String dFrom) {
            this.dFrom = dFrom;
        }

        public String getDTo() {
            return dTo;
        }

        public void setDTo(String dTo) {
            this.dTo = dTo;
        }

        public Integer getDPrice() {
            return dPrice;
        }

        public void setDPrice(Integer dPrice) {
            this.dPrice = dPrice;
        }

        public String getDMeter() {
            return dMeter;
        }

        public void setDMeter(String dMeter) {
            this.dMeter = dMeter;
        }

        public Double getDMeterAmount() {
            return dMeterAmount;
        }

        public void setDMeterAmount(Double dMeterAmount) {
            this.dMeterAmount = dMeterAmount;
        }

        public String getDSecond() {
            return dSecond;
        }

        public void setDSecond(String dSecond) {
            this.dSecond = dSecond;
        }

        public Double getDSecondAmount() {
            return dSecondAmount;
        }

        public void setDSecondAmount(Double dSecondAmount) {
            this.dSecondAmount = dSecondAmount;
        }

        public String getDDistanceUnder() {
            return dDistanceUnder;
        }

        public void setDDistanceUnder(String dDistanceUnder) {
            this.dDistanceUnder = dDistanceUnder;
        }

        public Integer getDAmountUnder() {
            return dAmountUnder;
        }

        public void setDAmountUnder(Integer dAmountUnder) {
            this.dAmountUnder = dAmountUnder;
        }

        public Integer getDBasicPrice() {
            return dBasicPrice;
        }

        public void setDBasicPrice(Integer dBasicPrice) {
            this.dBasicPrice = dBasicPrice;
        }

        public String getNFrom() {
            return nFrom;
        }

        public void setNFrom(String nFrom) {
            this.nFrom = nFrom;
        }

        public String getNTo() {
            return nTo;
        }

        public void setNTo(String nTo) {
            this.nTo = nTo;
        }

        public Integer getNPrice() {
            return nPrice;
        }

        public void setNPrice(Integer nPrice) {
            this.nPrice = nPrice;
        }

        public String getNMeter() {
            return nMeter;
        }

        public void setNMeter(String nMeter) {
            this.nMeter = nMeter;
        }

        public Double getNMeterAmount() {
            return nMeterAmount;
        }

        public void setNMeterAmount(Double nMeterAmount) {
            this.nMeterAmount = nMeterAmount;
        }

        public String getNSecond() {
            return nSecond;
        }

        public void setNSecond(String nSecond) {
            this.nSecond = nSecond;
        }

        public Double getNSecondAmount() {
            return nSecondAmount;
        }

        public void setNSecondAmount(Double nSecondAmount) {
            this.nSecondAmount = nSecondAmount;
        }

        public String getNDistanceUnder() {
            return nDistanceUnder;
        }

        public void setNDistanceUnder(String nDistanceUnder) {
            this.nDistanceUnder = nDistanceUnder;
        }

        public Integer getNAmountUnder() {
            return nAmountUnder;
        }

        public void setNAmountUnder(Integer nAmountUnder) {
            this.nAmountUnder = nAmountUnder;
        }

        public Integer getNBasicPrice() {
            return nBasicPrice;
        }

        public void setNBasicPrice(Integer nBasicPrice) {
            this.nBasicPrice = nBasicPrice;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }

}


