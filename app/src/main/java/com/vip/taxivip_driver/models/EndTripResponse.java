package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EndTripResponse {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("fare_time")
        @Expose
        private Integer fareTime;
        @SerializedName("fare_distance")
        @Expose
        private Integer fareDistance;
        @SerializedName("extra_charge")
        @Expose
        private Integer extraCharge;

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public Integer getFareTime() {
            return fareTime;
        }

        public void setFareTime(Integer fareTime) {
            this.fareTime = fareTime;
        }

        public Integer getFareDistance() {
            return fareDistance;
        }

        public void setFareDistance(Integer fareDistance) {
            this.fareDistance = fareDistance;
        }

        public Integer getExtraCharge() {
            return extraCharge;
        }

        public void setExtraCharge(Integer extraCharge) {
            this.extraCharge = extraCharge;
        }

    }
}
