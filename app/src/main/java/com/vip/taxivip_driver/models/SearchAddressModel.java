package com.vip.taxivip_driver.models;

public class SearchAddressModel {
    String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
