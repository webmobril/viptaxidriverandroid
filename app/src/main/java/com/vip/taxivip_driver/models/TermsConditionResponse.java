package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TermsConditionResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }


    public class Result {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("content_english")
        @Expose
        private String contentEnglish;
        @SerializedName("content_spanish")
        @Expose
        private String contentSpanish;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContentSpanish() {
            return contentSpanish;
        }

        public void setContentSpanish(String contentSpanish) {
            this.contentSpanish = contentSpanish;
        }

        public String getContentEnglish() {
            return contentEnglish;
        }

        public void setContentEnglish(String contentEnglish) {
            this.contentEnglish = contentEnglish;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }
}
