package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse {

    @SerializedName("result")
    @Expose
    public Result result;

    public class Result {

        @SerializedName("rating")
        @Expose
        public String rating;

        @SerializedName("driver_id")
        @Expose
        public Object driverId;

        @SerializedName("created_at")
        @Expose
        public String createdAt;

        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("subcategory_id")
        @Expose
        public Object subcategoryId;
        @SerializedName("s_lat")
        @Expose
        public String sLat;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("s_lng")
        @Expose
        public String sLng;
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("user_name")
        @Expose
        public String user_name;

        @SerializedName("user_image")
        @Expose
        public String user_image;

        @SerializedName("status")
        @Expose
        public Integer status;

        @SerializedName("payments")
        @Expose
        public List<Payment> payments = null;
    }

    public class Payment {

        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("amount")
        @Expose
        public String amount;

    }


    public class Users {

        @SerializedName("g_id")
        @Expose
        public Object gId;
        @SerializedName("officeaddress")
        @Expose
        public String officeaddress;
        @SerializedName("cur_lat")
        @Expose
        public String curLat;
        @SerializedName("cur_lng")
        @Expose
        public String curLng;
        @SerializedName("latitude")
        @Expose
        public String latitude;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("device_type")
        @Expose
        public Object deviceType;
        @SerializedName("ongoing_status")
        @Expose
        public Integer ongoingStatus;
        @SerializedName("txtpassword")
        @Expose
        public String txtpassword;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("longitude_home")
        @Expose
        public String longitudeHome;
        @SerializedName("email_verify")
        @Expose
        public Integer emailVerify;
        @SerializedName("verify")
        @Expose
        public Integer verify;
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("state_id")
        @Expose
        public Integer stateId;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("live")
        @Expose
        public Integer live;
        @SerializedName("longitude")
        @Expose
        public String longitude;
        @SerializedName("business_name")
        @Expose
        public Object businessName;
        @SerializedName("latitude_home")
        @Expose
        public String latitudeHome;
        @SerializedName("user_image")
        @Expose
        public String userImage;
        @SerializedName("homeaddress")
        @Expose
        public String homeaddress;
        @SerializedName("mobile")
        @Expose
        public String mobile;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("upgraded_business")
        @Expose
        public Integer upgradedBusiness;
        @SerializedName("verify_token")
        @Expose
        public Object verifyToken;
        @SerializedName("fb_id")
        @Expose
        public Object fbId;
        @SerializedName("roles_id")
        @Expose
        public Integer rolesId;
        @SerializedName("device_token")
        @Expose
        public Object deviceToken;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("agree_terms")
        @Expose
        public Integer agreeTerms;
        @SerializedName("country_id")
        @Expose
        public Integer countryId;
        @SerializedName("city_id")
        @Expose
        public Integer cityId;
        @SerializedName("status")
        @Expose
        public Integer status;

    }


}
