package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingDetailsResponse {
    @SerializedName("error")
    @Expose
    private Boolean error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("user_rating")
    @Expose
    private Integer userRating;
    @SerializedName("driver_rating")
    @Expose
    private Integer driverRating;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Integer getUserRating() {
        return userRating;
    }

    public void setUserRating(Integer userRating) {
        this.userRating = userRating;
    }

    public Integer getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(Integer driverRating) {
        this.driverRating = driverRating;
    }


public class Result {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("driver_id")
    @Expose
    private Integer driverId;
    @SerializedName("company_id")
    @Expose
    private Object companyId;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("s_lat")
    @Expose
    private String sLat;
    @SerializedName("s_lng")
    @Expose
    private String sLng;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("d_lat")
    @Expose
    private String dLat;
    @SerializedName("d_lng")
    @Expose
    private String dLng;
    @SerializedName("ride_type")
    @Expose
    private Integer rideType;
    @SerializedName("payment_type")
    @Expose
    private Integer paymentType;
    @SerializedName("subcategory_id")
    @Expose
    private Object subcategoryId;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("total_time")
    @Expose
    private String totalTime;
    @SerializedName("date_of_booking")
    @Expose
    private String dateOfBooking;
    @SerializedName("time_of_booking")
    @Expose
    private String timeOfBooking;
    @SerializedName("city_tax")
    @Expose
    private Integer cityTax;
    @SerializedName("state_tax")
    @Expose
    private Integer stateTax;
    @SerializedName("start_time")
    @Expose
    private Object startTime;
    @SerializedName("end_time")
    @Expose
    private Object endTime;
    @SerializedName("accept_time")
    @Expose
    private String acceptTime;
    @SerializedName("reject_time")
    @Expose
    private Object rejectTime;
    @SerializedName("updated_amount")
    @Expose
    private Object updatedAmount;
    @SerializedName("updated_status")
    @Expose
    private Integer updatedStatus;
    @SerializedName("booking_type")
    @Expose
    private Integer bookingType;
    @SerializedName("schedule_time")
    @Expose
    private Object scheduleTime;
    @SerializedName("notify_times")
    @Expose
    private Integer notifyTimes;
    @SerializedName("cancel_time")
    @Expose
    private Object cancelTime;
    @SerializedName("reason")
    @Expose
    private Object reason;
    @SerializedName("fare_time")
    @Expose
    private Integer fareTime;
    @SerializedName("fare_distance")
    @Expose
    private Integer fareDistance;
    @SerializedName("total_fare")
    @Expose
    private Integer totalFare;
    @SerializedName("basic_price")
    @Expose
    private Integer basicPrice;
    @SerializedName("change_time")
    @Expose
    private String changeTime;
    @SerializedName("cancel_charge")
    @Expose
    private Integer cancelCharge;
    @SerializedName("extra_charge")
    @Expose
    private Integer extraCharge;
    @SerializedName("parent_booking_id")
    @Expose
    private Object parentBookingId;
    @SerializedName("cancel_booking_count")
    @Expose
    private Integer cancelBookingCount;
    @SerializedName("review_by_user")
    @Expose
    private Integer reviewByUser;
    @SerializedName("review_by_driver")
    @Expose
    private Integer reviewByDriver;
    @SerializedName("stripe_token")
    @Expose
    private Object stripeToken;
    @SerializedName("pay_status")
    @Expose
    private Integer payStatus;
    @SerializedName("taxi_id")
    @Expose
    private Object taxiId;
    @SerializedName("taxi_number")
    @Expose
    private Object taxiNumber;
    @SerializedName("taxi_name")
    @Expose
    private Object taxiName;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("users")
    @Expose
    private Users users;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Object getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Object companyId) {
        this.companyId = companyId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSLat() {
        return sLat;
    }

    public void setSLat(String sLat) {
        this.sLat = sLat;
    }

    public String getSLng() {
        return sLng;
    }

    public void setSLng(String sLng) {
        this.sLng = sLng;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDLat() {
        return dLat;
    }

    public void setDLat(String dLat) {
        this.dLat = dLat;
    }

    public String getDLng() {
        return dLng;
    }

    public void setDLng(String dLng) {
        this.dLng = dLng;
    }

    public Integer getRideType() {
        return rideType;
    }

    public void setRideType(Integer rideType) {
        this.rideType = rideType;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Object getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Object subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getDateOfBooking() {
        return dateOfBooking;
    }

    public void setDateOfBooking(String dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
    }

    public String getTimeOfBooking() {
        return timeOfBooking;
    }

    public void setTimeOfBooking(String timeOfBooking) {
        this.timeOfBooking = timeOfBooking;
    }

    public Integer getCityTax() {
        return cityTax;
    }

    public void setCityTax(Integer cityTax) {
        this.cityTax = cityTax;
    }

    public Integer getStateTax() {
        return stateTax;
    }

    public void setStateTax(Integer stateTax) {
        this.stateTax = stateTax;
    }

    public Object getStartTime() {
        return startTime;
    }

    public void setStartTime(Object startTime) {
        this.startTime = startTime;
    }

    public Object getEndTime() {
        return endTime;
    }

    public void setEndTime(Object endTime) {
        this.endTime = endTime;
    }

    public String getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(String acceptTime) {
        this.acceptTime = acceptTime;
    }

    public Object getRejectTime() {
        return rejectTime;
    }

    public void setRejectTime(Object rejectTime) {
        this.rejectTime = rejectTime;
    }

    public Object getUpdatedAmount() {
        return updatedAmount;
    }

    public void setUpdatedAmount(Object updatedAmount) {
        this.updatedAmount = updatedAmount;
    }

    public Integer getUpdatedStatus() {
        return updatedStatus;
    }

    public void setUpdatedStatus(Integer updatedStatus) {
        this.updatedStatus = updatedStatus;
    }

    public Integer getBookingType() {
        return bookingType;
    }

    public void setBookingType(Integer bookingType) {
        this.bookingType = bookingType;
    }

    public Object getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(Object scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public Integer getNotifyTimes() {
        return notifyTimes;
    }

    public void setNotifyTimes(Integer notifyTimes) {
        this.notifyTimes = notifyTimes;
    }

    public Object getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Object cancelTime) {
        this.cancelTime = cancelTime;
    }

    public Object getReason() {
        return reason;
    }

    public void setReason(Object reason) {
        this.reason = reason;
    }

    public Integer getFareTime() {
        return fareTime;
    }

    public void setFareTime(Integer fareTime) {
        this.fareTime = fareTime;
    }

    public Integer getFareDistance() {
        return fareDistance;
    }

    public void setFareDistance(Integer fareDistance) {
        this.fareDistance = fareDistance;
    }

    public Integer getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(Integer totalFare) {
        this.totalFare = totalFare;
    }

    public Integer getBasicPrice() {
        return basicPrice;
    }

    public void setBasicPrice(Integer basicPrice) {
        this.basicPrice = basicPrice;
    }

    public String getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(String changeTime) {
        this.changeTime = changeTime;
    }

    public Integer getCancelCharge() {
        return cancelCharge;
    }

    public void setCancelCharge(Integer cancelCharge) {
        this.cancelCharge = cancelCharge;
    }

    public Integer getExtraCharge() {
        return extraCharge;
    }

    public void setExtraCharge(Integer extraCharge) {
        this.extraCharge = extraCharge;
    }

    public Object getParentBookingId() {
        return parentBookingId;
    }

    public void setParentBookingId(Object parentBookingId) {
        this.parentBookingId = parentBookingId;
    }

    public Integer getCancelBookingCount() {
        return cancelBookingCount;
    }

    public void setCancelBookingCount(Integer cancelBookingCount) {
        this.cancelBookingCount = cancelBookingCount;
    }

    public Integer getReviewByUser() {
        return reviewByUser;
    }

    public void setReviewByUser(Integer reviewByUser) {
        this.reviewByUser = reviewByUser;
    }

    public Integer getReviewByDriver() {
        return reviewByDriver;
    }

    public void setReviewByDriver(Integer reviewByDriver) {
        this.reviewByDriver = reviewByDriver;
    }

    public Object getStripeToken() {
        return stripeToken;
    }

    public void setStripeToken(Object stripeToken) {
        this.stripeToken = stripeToken;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Object getTaxiId() {
        return taxiId;
    }

    public void setTaxiId(Object taxiId) {
        this.taxiId = taxiId;
    }

    public Object getTaxiNumber() {
        return taxiNumber;
    }

    public void setTaxiNumber(Object taxiNumber) {
        this.taxiNumber = taxiNumber;
    }

    public Object getTaxiName() {
        return taxiName;
    }

    public void setTaxiName(Object taxiName) {
        this.taxiName = taxiName;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

}

public class Users {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("stripe_customer_id")
    @Expose
    private String stripeCustomerId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStripeCustomerId() {
        return stripeCustomerId;
    }

    public void setStripeCustomerId(String stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
    }
}
}
