package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtpVerifiedResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("access_token")
        @Expose
        private String accessToken;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public class User {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("first_name")
            @Expose
            private String firstName;
            @SerializedName("last_name")
            @Expose
            private String lastName;
            @SerializedName("email")
            @Expose
            private String email;
            @SerializedName("txtpassword")
            @Expose
            private String txtpassword;
            @SerializedName("fb_id")
            @Expose
            private String fbId;
            @SerializedName("g_id")
            @Expose
            private String gId;
            @SerializedName("business_name")
            @Expose
            private String businessName;
            @SerializedName("homeaddress")
            @Expose
            private String homeaddress;
            @SerializedName("officeaddress")
            @Expose
            private String officeaddress;
            @SerializedName("latitude")
            @Expose
            private String latitude;
            @SerializedName("longitude")
            @Expose
            private String longitude;
            @SerializedName("latitude_home")
            @Expose
            private String latitudeHome;
            @SerializedName("longitude_home")
            @Expose
            private String longitudeHome;
            @SerializedName("cur_lat")
            @Expose
            private String curLat;
            @SerializedName("cur_lng")
            @Expose
            private String curLng;
           
            @SerializedName("describe_yourself")
            @Expose
            private String describeYourself;
            @SerializedName("language_known")
            @Expose
            private String languageKnown;
            
            @SerializedName("mobile")
            @Expose
            private String mobile;
            @SerializedName("country_id")
            @Expose
            private String countryId;
            @SerializedName("state_id")
            @Expose
            private String stateId;
            @SerializedName("city_id")
            @Expose
            private String cityId;
            @SerializedName("agree_terms")
            @Expose
            private Integer agreeTerms;
            @SerializedName("upgraded_business")
            @Expose
            private Integer upgradedBusiness;
            @SerializedName("user_image")
            @Expose
            private String userImage;
            @SerializedName("roles_id")
            @Expose
            private Integer rolesId;
            @SerializedName("verify")
            @Expose
            private Integer verify;
            @SerializedName("user_thumb_verification")
            @Expose
            private Integer userThumbVerification;
            @SerializedName("email_verify")
            @Expose
            private Integer emailVerify;
            @SerializedName("verify_token")
            @Expose
            private String verifyToken;
            @SerializedName("device_type")
            @Expose
            private String deviceType;
            @SerializedName("device_token")
            @Expose
            private String deviceToken;
            @SerializedName("status")
            @Expose
            private Integer status;
            @SerializedName("live")
            @Expose
            private Integer live;
            @SerializedName("ongoing_status")
            @Expose
            private Integer ongoingStatus;
            @SerializedName("tax_number")
            @Expose
            private String taxNumber;
            @SerializedName("company_id")
            @Expose
            private String companyId;
            @SerializedName("taxi_id")
            @Expose
            private String taxiId;
            @SerializedName("taxi_number")
            @Expose
            private String taxiNumber;
            @SerializedName("login_type")
            @Expose
            private Integer loginType;
            @SerializedName("profit_ratio")
            @Expose
            private Integer profitRatio;
            @SerializedName("otp")
            @Expose
            private String otp;
            @SerializedName("otp_time")
            @Expose
            private String otpTime;
            @SerializedName("stripe_customer_id")
            @Expose
            private String stripeCustomerId;
            @SerializedName("auth_token")
            @Expose
            private String authToken;
            @SerializedName("verify_status")
            @Expose
            private Integer verifyStatus;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("created_at")
            @Expose
            private String createdAt;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getTxtpassword() {
                return txtpassword;
            }

            public void setTxtpassword(String txtpassword) {
                this.txtpassword = txtpassword;
            }

            public String getFbId() {
                return fbId;
            }

            public void setFbId(String fbId) {
                this.fbId = fbId;
            }

            public String getGId() {
                return gId;
            }

            public void setGId(String gId) {
                this.gId = gId;
            }

            public String getBusinessName() {
                return businessName;
            }

            public void setBusinessName(String businessName) {
                this.businessName = businessName;
            }

            public String getHomeaddress() {
                return homeaddress;
            }

            public void setHomeaddress(String homeaddress) {
                this.homeaddress = homeaddress;
            }

            public String getOfficeaddress() {
                return officeaddress;
            }

            public void setOfficeaddress(String officeaddress) {
                this.officeaddress = officeaddress;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getLatitudeHome() {
                return latitudeHome;
            }

            public void setLatitudeHome(String latitudeHome) {
                this.latitudeHome = latitudeHome;
            }

            public String getLongitudeHome() {
                return longitudeHome;
            }

            public void setLongitudeHome(String longitudeHome) {
                this.longitudeHome = longitudeHome;
            }

            public String getCurLat() {
                return curLat;
            }

            public void setCurLat(String curLat) {
                this.curLat = curLat;
            }

            public String getCurLng() {
                return curLng;
            }

            public void setCurLng(String curLng) {
                this.curLng = curLng;
            }


            public String getDescribeYourself() {
                return describeYourself;
            }

            public void setDescribeYourself(String describeYourself) {
                this.describeYourself = describeYourself;
            }

            public String getLanguageKnown() {
                return languageKnown;
            }

            public void setLanguageKnown(String languageKnown) {
                this.languageKnown = languageKnown;
            }
            

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getCountryId() {
                return countryId;
            }

            public void setCountryId(String countryId) {
                this.countryId = countryId;
            }

            public String getStateId() {
                return stateId;
            }

            public void setStateId(String stateId) {
                this.stateId = stateId;
            }

            public String getCityId() {
                return cityId;
            }

            public void setCityId(String cityId) {
                this.cityId = cityId;
            }

            public Integer getAgreeTerms() {
                return agreeTerms;
            }

            public void setAgreeTerms(Integer agreeTerms) {
                this.agreeTerms = agreeTerms;
            }

            public Integer getUpgradedBusiness() {
                return upgradedBusiness;
            }

            public void setUpgradedBusiness(Integer upgradedBusiness) {
                this.upgradedBusiness = upgradedBusiness;
            }

            public String getUserImage() {
                return userImage;
            }

            public void setUserImage(String userImage) {
                this.userImage = userImage;
            }

            public Integer getRolesId() {
                return rolesId;
            }

            public void setRolesId(Integer rolesId) {
                this.rolesId = rolesId;
            }

            public Integer getVerify() {
                return verify;
            }

            public void setVerify(Integer verify) {
                this.verify = verify;
            }

            public Integer getUserThumbVerification() {
                return userThumbVerification;
            }

            public void setUserThumbVerification(Integer userThumbVerification) {
                this.userThumbVerification = userThumbVerification;
            }

            public Integer getEmailVerify() {
                return emailVerify;
            }

            public void setEmailVerify(Integer emailVerify) {
                this.emailVerify = emailVerify;
            }

            public String getVerifyToken() {
                return verifyToken;
            }

            public void setVerifyToken(String verifyToken) {
                this.verifyToken = verifyToken;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getDeviceToken() {
                return deviceToken;
            }

            public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

            public Integer getLive() {
                return live;
            }

            public void setLive(Integer live) {
                this.live = live;
            }

            public Integer getOngoingStatus() {
                return ongoingStatus;
            }

            public void setOngoingStatus(Integer ongoingStatus) {
                this.ongoingStatus = ongoingStatus;
            }

            public String getTaxNumber() {
                return taxNumber;
            }

            public void setTaxNumber(String taxNumber) {
                this.taxNumber = taxNumber;
            }

            public String getCompanyId() {
                return companyId;
            }

            public void setCompanyId(String companyId) {
                this.companyId = companyId;
            }

            public String getTaxiId() {
                return taxiId;
            }

            public void setTaxiId(String taxiId) {
                this.taxiId = taxiId;
            }

            public String getTaxiNumber() {
                return taxiNumber;
            }

            public void setTaxiNumber(String taxiNumber) {
                this.taxiNumber = taxiNumber;
            }

            public Integer getLoginType() {
                return loginType;
            }

            public void setLoginType(Integer loginType) {
                this.loginType = loginType;
            }

            public Integer getProfitRatio() {
                return profitRatio;
            }

            public void setProfitRatio(Integer profitRatio) {
                this.profitRatio = profitRatio;
            }

            public String getOtp() {
                return otp;
            }

            public void setOtp(String otp) {
                this.otp = otp;
            }

            public String getOtpTime() {
                return otpTime;
            }

            public void setOtpTime(String otpTime) {
                this.otpTime = otpTime;
            }

            public String getStripeCustomerId() {
                return stripeCustomerId;
            }

            public void setStripeCustomerId(String stripeCustomerId) {
                this.stripeCustomerId = stripeCustomerId;
            }

            public String getAuthToken() {
                return authToken;
            }

            public void setAuthToken(String authToken) {
                this.authToken = authToken;
            }

            public Integer getVerifyStatus() {
                return verifyStatus;
            }

            public void setVerifyStatus(Integer verifyStatus) {
                this.verifyStatus = verifyStatus;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }
        }
    }
}
