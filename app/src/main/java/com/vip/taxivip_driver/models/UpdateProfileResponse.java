package com.vip.taxivip_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateProfileResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }



    public class Country {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("country_code")
        @Expose
        private String countryCode;
        @SerializedName("flag_image")
        @Expose
        private String flagImage;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getFlagImage() {
            return flagImage;
        }

        public void setFlagImage(String flagImage) {
            this.flagImage = flagImage;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public static class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("txtpassword")
        @Expose
        private Object txtpassword;
        @SerializedName("fb_id")
        @Expose
        private Object fbId;
        @SerializedName("g_id")
        @Expose
        private Object gId;
        @SerializedName("address")
        @Expose
        private String  address;
        @SerializedName("homeaddress")
        @Expose
        private String homeaddress;
        @SerializedName("officeaddress")
        @Expose
        private String officeaddress;
        @SerializedName("describe_yourself")
        @Expose
        private String describeYourself;
        @SerializedName("language_known")
        @Expose
        private String languageKnown;

        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("latitude_home")
        @Expose
        private String latitudeHome;
        @SerializedName("longitude_home")
        @Expose
        private String longitudeHome;
        @SerializedName("cur_lat")
        @Expose
        private String curLat;
        @SerializedName("cur_lng")
        @Expose
        private String curLng;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("country_id")
        @Expose
        private Integer countryId;
        @SerializedName("state_id")
        @Expose
        private Object stateId;
        @SerializedName("city_id")
        @Expose
        private String cityId;
        @SerializedName("agree_terms")
        @Expose
        private Integer agreeTerms;
        @SerializedName("user_image")
        @Expose
        private String userImage;
        @SerializedName("roles_id")
        @Expose
        private Integer rolesId;
        @SerializedName("verify")
        @Expose
        private Integer verify;
        @SerializedName("user_thumb_verification")
        @Expose
        private Integer userThumbVerification;
        @SerializedName("email_verify")
        @Expose
        private Integer emailVerify;
        @SerializedName("verify_token")
        @Expose
        private Object verifyToken;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("live")
        @Expose
        private Integer live;
        @SerializedName("ongoing_status")
        @Expose
        private Integer ongoingStatus;
        @SerializedName("tax_number")
        @Expose
        private Object taxNumber;
        @SerializedName("company_id")
        @Expose
        private Integer companyId;
        @SerializedName("taxi_id")
        @Expose
        private Object taxiId;
        @SerializedName("taxi_number")
        @Expose
        private Object taxiNumber;
        @SerializedName("login_type")
        @Expose
        private Integer loginType;
        @SerializedName("profit_ratio")
        @Expose
        private Integer profitRatio;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("otp_time")
        @Expose
        private String otpTime;
        @SerializedName("stripe_customer_id")
        @Expose
        private Object stripeCustomerId;
        @SerializedName("auth_token")
        @Expose
        private String authToken;
        @SerializedName("verify_status")
        @Expose
        private Integer verifyStatus;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("roles")
        @Expose
        private Roles roles;
        @SerializedName("city")
        @Expose
        private Object city;
        @SerializedName("state")
        @Expose
        private Object state;
        @SerializedName("country")
        @Expose
        private Country country;

        @SerializedName("documents")
        @Expose
        private Documents documents;


        public Result(){

        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFirstName() {
            return firstName+"";
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getTxtpassword() {
            return txtpassword;
        }

        public void setTxtpassword(Object txtpassword) {
            this.txtpassword = txtpassword;
        }

        public Object getFbId() {
            return fbId;
        }

        public void setFbId(Object fbId) {
            this.fbId = fbId;
        }

        public Object getGId() {
            return gId;
        }

        public void setGId(Object gId) {
            this.gId = gId;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getHomeaddress() {
            return homeaddress;
        }

        public void setHomeaddress(String homeaddress) {
            this.homeaddress = homeaddress;
        }

        public String getDescribeYourself() {
            return describeYourself;
        }

        public void setDescribeYourself(String describeYourself) {
            this.describeYourself = describeYourself;
        }


        public String getLanguageKnown() {
            return languageKnown;
        }

        public void setLanguageKnown(String languageKnown) {
            this.languageKnown = languageKnown;
        }

        public String getOfficeaddress() {
            return officeaddress;
        }

        public void setOfficeaddress(String officeaddress) {
            this.officeaddress = officeaddress;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitudeHome() {
            return latitudeHome;
        }

        public void setLatitudeHome(String latitudeHome) {
            this.latitudeHome = latitudeHome;
        }

        public String getLongitudeHome() {
            return longitudeHome;
        }

        public void setLongitudeHome(String longitudeHome) {
            this.longitudeHome = longitudeHome;
        }

        public String getCurLat() {
            return curLat;
        }

        public void setCurLat(String curLat) {
            this.curLat = curLat;
        }

        public String getCurLng() {
            return curLng;
        }

        public void setCurLng(String curLng) {
            this.curLng = curLng;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Integer getCountryId() {
            return countryId;
        }

        public void setCountryId(Integer countryId) {
            this.countryId = countryId;
        }

        public Object getStateId() {
            return stateId;
        }

        public void setStateId(Object stateId) {
            this.stateId = stateId;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public Integer getAgreeTerms() {
            return agreeTerms;
        }

        public void setAgreeTerms(Integer agreeTerms) {
            this.agreeTerms = agreeTerms;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public Integer getRolesId() {
            return rolesId;
        }

        public void setRolesId(Integer rolesId) {
            this.rolesId = rolesId;
        }

        public Integer getVerify() {
            return verify;
        }

        public void setVerify(Integer verify) {
            this.verify = verify;
        }

        public Integer getUserThumbVerification() {
            return userThumbVerification;
        }

        public void setUserThumbVerification(Integer userThumbVerification) {
            this.userThumbVerification = userThumbVerification;
        }

        public Integer getEmailVerify() {
            return emailVerify;
        }

        public void setEmailVerify(Integer emailVerify) {
            this.emailVerify = emailVerify;
        }

        public Object getVerifyToken() {
            return verifyToken;
        }

        public void setVerifyToken(Object verifyToken) {
            this.verifyToken = verifyToken;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getLive() {
            return live;
        }

        public void setLive(Integer live) {
            this.live = live;
        }

        public Integer getOngoingStatus() {
            return ongoingStatus;
        }

        public void setOngoingStatus(Integer ongoingStatus) {
            this.ongoingStatus = ongoingStatus;
        }

        public Object getTaxNumber() {
            return taxNumber;
        }

        public void setTaxNumber(Object taxNumber) {
            this.taxNumber = taxNumber;
        }

        public Integer getCompanyId() {
            return companyId;
        }

        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        public Object getTaxiId() {
            return taxiId;
        }

        public void setTaxiId(Object taxiId) {
            this.taxiId = taxiId;
        }

        public Object getTaxiNumber() {
            return taxiNumber;
        }

        public void setTaxiNumber(Object taxiNumber) {
            this.taxiNumber = taxiNumber;
        }

        public Integer getLoginType() {
            return loginType;
        }

        public void setLoginType(Integer loginType) {
            this.loginType = loginType;
        }

        public Integer getProfitRatio() {
            return profitRatio;
        }

        public void setProfitRatio(Integer profitRatio) {
            this.profitRatio = profitRatio;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getOtpTime() {
            return otpTime;
        }

        public void setOtpTime(String otpTime) {
            this.otpTime = otpTime;
        }

        public Object getStripeCustomerId() {
            return stripeCustomerId;
        }

        public void setStripeCustomerId(Object stripeCustomerId) {
            this.stripeCustomerId = stripeCustomerId;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public Integer getVerifyStatus() {
            return verifyStatus;
        }

        public void setVerifyStatus(Integer verifyStatus) {
            this.verifyStatus = verifyStatus;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public Roles getRoles() {
            return roles;
        }

        public void setRoles(Roles roles) {
            this.roles = roles;
        }

        public Object getCity() {
            return city;
        }

        public void setCity(Object city) {
            this.city = city;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Country getCountry() {
            return country;
        }

        public void setCountry(Country country) {
            this.country = country;
        }

        public Documents getDocuments() {
            return documents;
        }

        public void setDocuments(Documents documents) {
            this.documents = documents;
        }

    }





    public class Documents {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("dl_image")
    @Expose
    private String dlImage;
    @SerializedName("dl_verified_status")
    @Expose
    private Integer dlVerifiedStatus;
    @SerializedName("insurance_image")
    @Expose
    private String insuranceImage;
    @SerializedName("insurance_verified_status")
    @Expose
    private Integer insuranceVerifiedStatus;
    @SerializedName("registration_img")
    @Expose
    private String registrationImg;
    @SerializedName("registration_verified_status")
    @Expose
    private Integer registrationVerifiedStatus;
    @SerializedName("permit_image")
    @Expose
    private String permitImage;
    @SerializedName("permit_verified_status")
    @Expose
    private Integer permitVerifiedStatus;
    @SerializedName("criminal_record_img")
    @Expose
    private String criminalRecordImg;
    @SerializedName("criminal_verified_status")
    @Expose
    private Integer criminalVerifiedStatus;
    @SerializedName("police_record_img")
    @Expose
    private String policeRecordImg;
    @SerializedName("police_verified_status")
    @Expose
    private Integer policeVerifiedStatus;
    @SerializedName("model_number")
    @Expose
    private String modelNumber;
    @SerializedName("car_name")
    @Expose
    private String carName;
    @SerializedName("car_year")
    @Expose
    private String carYear;
    @SerializedName("car_number")
    @Expose
    private String carNumber;
    @SerializedName("car_color")
    @Expose
    private String carColor;
    @SerializedName("viehcle_type_id")
    @Expose
    private Integer viehcleTypeId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("viehcle_type")
    @Expose
    private ViehcleType viehcleType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDlImage() {
        return dlImage;
    }

    public void setDlImage(String dlImage) {
        this.dlImage = dlImage;
    }

    public Integer getDlVerifiedStatus() {
        return dlVerifiedStatus;
    }

    public void setDlVerifiedStatus(Integer dlVerifiedStatus) {
        this.dlVerifiedStatus = dlVerifiedStatus;
    }

    public String getInsuranceImage() {
        return insuranceImage;
    }

    public void setInsuranceImage(String insuranceImage) {
        this.insuranceImage = insuranceImage;
    }

    public Integer getInsuranceVerifiedStatus() {
        return insuranceVerifiedStatus;
    }

    public void setInsuranceVerifiedStatus(Integer insuranceVerifiedStatus) {
        this.insuranceVerifiedStatus = insuranceVerifiedStatus;
    }

    public String getRegistrationImg() {
        return registrationImg;
    }

    public void setRegistrationImg(String registrationImg) {
        this.registrationImg = registrationImg;
    }

    public Integer getRegistrationVerifiedStatus() {
        return registrationVerifiedStatus;
    }

    public void setRegistrationVerifiedStatus(Integer registrationVerifiedStatus) {
        this.registrationVerifiedStatus = registrationVerifiedStatus;
    }

    public String getPermitImage() {
        return permitImage;
    }

    public void setPermitImage(String permitImage) {
        this.permitImage = permitImage;
    }

    public Integer getPermitVerifiedStatus() {
        return permitVerifiedStatus;
    }

    public void setPermitVerifiedStatus(Integer permitVerifiedStatus) {
        this.permitVerifiedStatus = permitVerifiedStatus;
    }

    public String getCriminalRecordImg() {
        return criminalRecordImg;
    }

    public void setCriminalRecordImg(String criminalRecordImg) {
        this.criminalRecordImg = criminalRecordImg;
    }

    public Integer getCriminalVerifiedStatus() {
        return criminalVerifiedStatus;
    }

    public void setCriminalVerifiedStatus(Integer criminalVerifiedStatus) {
        this.criminalVerifiedStatus = criminalVerifiedStatus;
    }

    public String getPoliceRecordImg() {
        return policeRecordImg;
    }

    public void setPoliceRecordImg(String policeRecordImg) {
        this.policeRecordImg = policeRecordImg;
    }

    public Integer getPoliceVerifiedStatus() {
        return policeVerifiedStatus;
    }

    public void setPoliceVerifiedStatus(Integer policeVerifiedStatus) {
        this.policeVerifiedStatus = policeVerifiedStatus;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarYear() {
        return carYear;
    }

    public void setCarYear(String carYear) {
        this.carYear = carYear;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public Integer getViehcleTypeId() {
        return viehcleTypeId;
    }

    public void setViehcleTypeId(Integer viehcleTypeId) {
        this.viehcleTypeId = viehcleTypeId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ViehcleType getViehcleType() {
        return viehcleType;
    }

    public void setViehcleType(ViehcleType viehcleType) {
        this.viehcleType = viehcleType;
    }

}

    public class Roles {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }



    public class ViehcleType {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("icon")
        @Expose
        private String icon;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }

}
