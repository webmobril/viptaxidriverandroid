package com.vip.taxivip_driver.reposotory;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import com.vip.taxivip_driver.models.UpdateProfileResponse;
import com.vip.taxivip_driver.network.NoConnectivityException;
import com.vip.taxivip_driver.network.RestApiService;
import com.vip.taxivip_driver.network.RetrofitInstance;
import com.vip.taxivip_driver.requestclass.SetDestinationRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeRespsotory {

    private static RestApiService apiService;
    private static HomeRespsotory homeRespsotory;

    private HomeRespsotory() {
        apiService = RetrofitInstance.getApiService();

    }

    public static HomeRespsotory getInstance() {
        if (homeRespsotory == null) {
            homeRespsotory = new HomeRespsotory();
        }
        return homeRespsotory;
    }





    public MutableLiveData<UpdateProfileResponse> postSetDetinationApi( SetDestinationRequest setDestinationRequest) {
        MutableLiveData<UpdateProfileResponse> setDestinationLayout = new MutableLiveData<>();
        Call<UpdateProfileResponse> call = apiService.postSetDestination(setDestinationRequest);

        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                if (response.isSuccessful()) {
                    setDestinationLayout.setValue(response.body());

                } else {
                    JSONObject jsonObject = null;
                    try {
                        UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        updateProfileResponse.setMessage(userMessage);
                        setDestinationLayout.setValue(updateProfileResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {

                if (t instanceof NoConnectivityException){
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage(null);
                    setDestinationLayout.setValue(updateProfileResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage("Error");
                    setDestinationLayout.setValue(updateProfileResponse);
                }
            }
        });


        return setDestinationLayout;
    }


}
