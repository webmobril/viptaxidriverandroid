package com.vip.taxivip_driver.reposotory;

import android.util.Log;
import androidx.lifecycle.MutableLiveData;
import com.vip.taxivip_driver.models.DocumentResponse;
import com.vip.taxivip_driver.models.LanguageResponse;
import com.vip.taxivip_driver.models.UpdateProfileResponse;
import com.vip.taxivip_driver.models.VehicleTypeResponse;
import com.vip.taxivip_driver.network.NoConnectivityException;
import com.vip.taxivip_driver.network.RestApiService;
import com.vip.taxivip_driver.network.RetrofitInstance;
import com.vip.taxivip_driver.requestclass.ProfileDescribeRequest;
import com.vip.taxivip_driver.requestclass.VehicleUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileReposotory {
    private final RestApiService apiService;

    private static ProfileReposotory profileReposotory;
    private ProfileReposotory() {
         apiService = RetrofitInstance.getApiService();
    }
     public static ProfileReposotory getInstance(){
        if (profileReposotory == null){
            profileReposotory = new ProfileReposotory();
        }
        return  profileReposotory;
     }


    // Update vehicle type Reposotory
    public MutableLiveData<DocumentResponse> postVehicleModelUpdate(VehicleUploadRequest vehicleUploadRequest) {

        MutableLiveData<DocumentResponse> mutableLiveDataModelCar = new MutableLiveData<>();

        Call<DocumentResponse> call = apiService.postVehicleDetailsUpload(vehicleUploadRequest);
        call.enqueue(new Callback<DocumentResponse>() {
            @Override
            public void onResponse(Call<DocumentResponse> call, Response<DocumentResponse> response) {

                if (response.isSuccessful()){
                mutableLiveDataModelCar.setValue(response.body());

                }   else {
                    JSONObject jsonObject = null;
                    try {
                      DocumentResponse  documentResponse = new DocumentResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        documentResponse.setMessage(userMessage);
                        mutableLiveDataModelCar.setValue(documentResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<DocumentResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    DocumentResponse  documentResponse = new DocumentResponse();
                    documentResponse.setMessage(null);
                    mutableLiveDataModelCar.setValue(documentResponse);
                }
                try {
                    DocumentResponse  documentResponse = new DocumentResponse();
                    documentResponse.setMessage("Error Message");
                    mutableLiveDataModelCar.setValue(documentResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        return mutableLiveDataModelCar;
    }


    public MutableLiveData<UpdateProfileResponse> postUpdateProfileDescribe(ProfileDescribeRequest profileDescribeRequest) {
        MutableLiveData<UpdateProfileResponse> listMutableLiveDataUpdateProfile = new MutableLiveData<>();
        Call<UpdateProfileResponse> call = apiService.postUpateProfileDescribe(profileDescribeRequest);

        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                if (response.isSuccessful()) {
                    listMutableLiveDataUpdateProfile.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        UpdateProfileResponse  updateProfileResponse = new UpdateProfileResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        updateProfileResponse.setMessage(userMessage);
                        listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage(null);
                    listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage("Error");
                    listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                }
            }
        });

        return listMutableLiveDataUpdateProfile;
    }


    public MutableLiveData<UpdateProfileResponse> postUpdateProfileLanguage(int driverId, String language_known) {
        MutableLiveData<UpdateProfileResponse> listMutableLiveDataUpdateProfile = new MutableLiveData<>();
        Call<UpdateProfileResponse> call = apiService.postUpateProfileLanguage(driverId, language_known);

        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                if (response.isSuccessful()) {
                    listMutableLiveDataUpdateProfile.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        UpdateProfileResponse  updateProfileResponse = new UpdateProfileResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        updateProfileResponse.setMessage(userMessage);
                        listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage(null);
                    listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage("Error");
                    listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                }
            }
        });

        return listMutableLiveDataUpdateProfile;
    }



    public MutableLiveData<LanguageResponse> getLanguageResponse() {
        MutableLiveData<LanguageResponse> listMutableLiveDataLanguage = new MutableLiveData<>();
        Call<LanguageResponse> call = apiService.getLanguageApi();

        call.enqueue(new Callback<LanguageResponse>() {
            @Override
            public void onResponse(Call<LanguageResponse> call, Response<LanguageResponse> response) {
                if (response.isSuccessful()) {
                    listMutableLiveDataLanguage.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        LanguageResponse  languageResponse = new LanguageResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        languageResponse.setMessage(userMessage);
                        listMutableLiveDataLanguage.setValue(languageResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LanguageResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    LanguageResponse  languageResponse = new LanguageResponse();
                    languageResponse.setMessage(null);
                    listMutableLiveDataLanguage.setValue(languageResponse);

                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    LanguageResponse  languageResponse = new LanguageResponse();
                    languageResponse.setMessage("Error");
                    listMutableLiveDataLanguage.setValue(languageResponse);
                }

            }
        });

        return listMutableLiveDataLanguage;
    }



    // Get vehicle type Reposotory

    public MutableLiveData<VehicleTypeResponse> getVehicleTypeReposotory(){

        MutableLiveData<VehicleTypeResponse> vehicleTypeResponseMutableLiveData = new MutableLiveData<>();
        Call<VehicleTypeResponse> call = apiService.getVehicleType();
        call.enqueue(new Callback<VehicleTypeResponse>() {
            @Override
            public void onResponse(Call<VehicleTypeResponse> call, Response<VehicleTypeResponse> response) {
                if (response.isSuccessful()){
                    vehicleTypeResponseMutableLiveData.setValue(response.body());

                }else {
                    JSONObject jsonObject = null;
                    try {
                        VehicleTypeResponse  vehicleTypeResponse = new VehicleTypeResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        vehicleTypeResponse.setMessage(userMessage);
                        vehicleTypeResponseMutableLiveData.setValue(vehicleTypeResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<VehicleTypeResponse> call, Throwable t) {

                if (t instanceof NoConnectivityException){
                    VehicleTypeResponse vehicleTypeResponse = new VehicleTypeResponse();
                    vehicleTypeResponse.setMessage(null);
                    vehicleTypeResponseMutableLiveData.setValue(vehicleTypeResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    VehicleTypeResponse vehicleTypeResponse = new VehicleTypeResponse();
                    vehicleTypeResponse.setMessage("Error");
                    vehicleTypeResponseMutableLiveData.setValue(vehicleTypeResponse);
                }
            }
        });

        return vehicleTypeResponseMutableLiveData;
    }




}
