package com.vip.taxivip_driver.reposotory;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.vip.taxivip_driver.models.AcceptBookingResponse;
import com.vip.taxivip_driver.models.BookingDetailsResponse;
import com.vip.taxivip_driver.models.EndTripResponse;
import com.vip.taxivip_driver.models.OfflineResponse;
import com.vip.taxivip_driver.models.ReachLocationResponse;
import com.vip.taxivip_driver.models.SendReviewResponse;
import com.vip.taxivip_driver.models.TripHistoryResponse;
import com.vip.taxivip_driver.network.NoConnectivityException;
import com.vip.taxivip_driver.network.RestApiService;
import com.vip.taxivip_driver.network.RetrofitInstance;
import com.vip.taxivip_driver.requestclass.AcceptBookingRequest;
import com.vip.taxivip_driver.requestclass.EndTripRequest;
import com.vip.taxivip_driver.requestclass.ReviewRequest;
import com.vip.taxivip_driver.requestclass.StartTripRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingReposotory {

    private static RestApiService apiService;
    private static BookingReposotory bookingReposotory;
    private AcceptBookingResponse acceptBookingRespons;

    private BookingReposotory() {
        apiService = RetrofitInstance.getApiService();
    }

    public static BookingReposotory getInstance() {
        if (bookingReposotory == null) {
            bookingReposotory = new BookingReposotory();
        }
        return bookingReposotory;
    }


    // offline online request reposotory

    public MutableLiveData<OfflineResponse> postOfflineDriverApi(int driverId) {
        MutableLiveData<OfflineResponse> listMutableLiveDataOfflineDriver = new MutableLiveData<>();
        Call<OfflineResponse> call = apiService.postOfflineDriver(driverId);
        call.enqueue(new Callback<OfflineResponse>() {
            @Override
            public void onResponse(Call<OfflineResponse> call, Response<OfflineResponse> response) {
                // Log.e("offline_resonse", new Gson().toJson(response));
                if (response.isSuccessful()) {
                    listMutableLiveDataOfflineDriver.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        OfflineResponse offlineError = new OfflineResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        offlineError.setMessage(userMessage);
                        listMutableLiveDataOfflineDriver.setValue(offlineError);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<OfflineResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    Log.e(this.getClass().getName(), "No internet Connected");
                    // show No Connectivity message to user or do whatever you want.
                    OfflineResponse offlineError = new OfflineResponse();
                    offlineError.setMessage(null);
                    listMutableLiveDataOfflineDriver.setValue(offlineError);
                } else {
                    OfflineResponse offlineError = new OfflineResponse();
                    offlineError.setMessage("Error Throw");
                    listMutableLiveDataOfflineDriver.setValue(offlineError);
                }

            }
        });


        return listMutableLiveDataOfflineDriver;

    }


    // Accept Booking Request Reposotory

    public MutableLiveData<AcceptBookingResponse> acceptBookingRequestReposotory(AcceptBookingRequest acceptBookingRequest) {
        MutableLiveData<AcceptBookingResponse> bookingResponseMutableLiveData = new MutableLiveData<>();

        Call<AcceptBookingResponse> call = apiService.postAcceptBookingRequest(acceptBookingRequest);
        call.enqueue(new Callback<AcceptBookingResponse>() {
            @Override
            public void onResponse(Call<AcceptBookingResponse> call, Response<AcceptBookingResponse> response) {
                if (response.isSuccessful()) {
                    bookingResponseMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        acceptBookingRespons = new AcceptBookingResponse();
                        acceptBookingRespons.setMessage(userMessage);
                        bookingResponseMutableLiveData.setValue(acceptBookingRespons);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AcceptBookingResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    acceptBookingRespons = new AcceptBookingResponse();
                    acceptBookingRespons.setMessage(null);
                    bookingResponseMutableLiveData.setValue(acceptBookingRespons);

                } else {
                    acceptBookingRespons = new AcceptBookingResponse();
                    acceptBookingRespons.setMessage("onFailure");
                    bookingResponseMutableLiveData.setValue(acceptBookingRespons);
                }
            }
        });

        return bookingResponseMutableLiveData;
    }


    // Reach Location Trip Booking Reposotory
    public MutableLiveData<ReachLocationResponse> reachLocationReposotory(AcceptBookingRequest acceptBookingRequest){
        MutableLiveData<ReachLocationResponse> reachLocationResponseMutableLiveData = new MutableLiveData<>();
        Call<ReachLocationResponse> call = apiService.reactLocationRequest(acceptBookingRequest);
        call.enqueue(new Callback<ReachLocationResponse>() {
            @Override
            public void onResponse(Call<ReachLocationResponse> call, Response<ReachLocationResponse> response) {
                if (response.isSuccessful()){
                    reachLocationResponseMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        ReachLocationResponse reachLocationResponse = new ReachLocationResponse();
                        reachLocationResponse.setMessage(userMessage);
                        reachLocationResponseMutableLiveData.setValue(reachLocationResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ReachLocationResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    ReachLocationResponse reachLocationResponse = new ReachLocationResponse();
                    reachLocationResponse.setMessage(null);
                    reachLocationResponseMutableLiveData.setValue(reachLocationResponse);
                } else {
                    ReachLocationResponse reachLocationResponse = new ReachLocationResponse();
                    reachLocationResponse.setMessage("onFailure");
                    reachLocationResponseMutableLiveData.setValue(reachLocationResponse);
                }

            }
        });

        return reachLocationResponseMutableLiveData;
    }


    // Start Trip booking Reqeuest Repostory

    public MutableLiveData<AcceptBookingResponse> startTripReposotory(StartTripRequest startTripRequest) {
        MutableLiveData<AcceptBookingResponse> startTripMutableLiveData = new MutableLiveData<>();
        Call<AcceptBookingResponse> call = apiService.postStartTripRequest(startTripRequest);
        call.enqueue(new Callback<AcceptBookingResponse>() {
            @Override
            public void onResponse(Call<AcceptBookingResponse> call, Response<AcceptBookingResponse> response) {
                if (response.isSuccessful()) {
                    startTripMutableLiveData.setValue(response.body());
                } else {

                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        acceptBookingRespons = new AcceptBookingResponse();
                        acceptBookingRespons.setMessage(userMessage);
                        startTripMutableLiveData.setValue(acceptBookingRespons);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<AcceptBookingResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    acceptBookingRespons = new AcceptBookingResponse();
                    acceptBookingRespons.setMessage(null);
                    startTripMutableLiveData.setValue(acceptBookingRespons);

                } else {
                    acceptBookingRespons = new AcceptBookingResponse();
                    acceptBookingRespons.setMessage("onFailure");
                    startTripMutableLiveData.setValue(acceptBookingRespons);
                }
            }
        });
        return startTripMutableLiveData;
    }

    // End Trip Repostoroy Api
    public MutableLiveData<EndTripResponse> endTripResponseReposotory(EndTripRequest endTripRequest) {
        MutableLiveData<EndTripResponse> endTripMutableLiveData = new MutableLiveData<>();
        Call<EndTripResponse> call = apiService.postEndTripApi(endTripRequest);
        call.enqueue(new Callback<EndTripResponse>() {
            @Override
            public void onResponse(Call<EndTripResponse> call, Response<EndTripResponse> response) {

                if (response.isSuccessful()) {
                    endTripMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        EndTripResponse  endTripResponse = new EndTripResponse();
                        endTripResponse.setMessage(userMessage);
                        endTripMutableLiveData.setValue(endTripResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<EndTripResponse> call, Throwable t) {
                EndTripResponse endTripResponse;
                if (t instanceof NoConnectivityException) {
                     endTripResponse = new EndTripResponse();
                    endTripResponse.setMessage(null);
                    endTripMutableLiveData.setValue(endTripResponse);
                } else {
                    endTripResponse = new EndTripResponse();
                    endTripResponse.setMessage("onFailure");
                    endTripMutableLiveData.setValue(endTripResponse);
                }

            }
        });

        return endTripMutableLiveData;
    }


    // Send Review Reposotory
    public MutableLiveData<SendReviewResponse> sendReviewResponseReposotory(ReviewRequest reviewRequest) {
        MutableLiveData<SendReviewResponse> sendReviewResponseMutableLiveData = new MutableLiveData<>();
        Call<SendReviewResponse> call = apiService.postSendReviewApi(reviewRequest);
        call.enqueue(new Callback<SendReviewResponse>() {
            @Override
            public void onResponse(Call<SendReviewResponse> call, Response<SendReviewResponse> response) {

                if (response.isSuccessful()) {
                    sendReviewResponseMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        SendReviewResponse sendReviewResponse = new SendReviewResponse();
                        sendReviewResponse.setMessage(userMessage);
                        sendReviewResponseMutableLiveData.setValue(sendReviewResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<SendReviewResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    SendReviewResponse sendReviewResponse = new SendReviewResponse();
                    sendReviewResponse.setMessage(null);
                    sendReviewResponseMutableLiveData.setValue(sendReviewResponse);
                } else {
                    SendReviewResponse sendReviewResponse = new SendReviewResponse();
                    sendReviewResponse.setMessage("onFailure");
                    sendReviewResponseMutableLiveData.setValue(sendReviewResponse);
                }

            }
        });

        return sendReviewResponseMutableLiveData;
    }


    // Trip Hisotry Reposotory

    public MutableLiveData<TripHistoryResponse> postTripHistoryReposotory(int driverId) {
        MutableLiveData<TripHistoryResponse> tripHistoryResponseMutableLiveData = new MutableLiveData<>();
        Call<TripHistoryResponse> call = apiService.getTripHistory(driverId);
        call.enqueue(new Callback<TripHistoryResponse>() {
            @Override
            public void onResponse(Call<TripHistoryResponse> call, Response<TripHistoryResponse> response) {

                if (response.isSuccessful()) {
                    tripHistoryResponseMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        TripHistoryResponse tripHistoryResponse = new TripHistoryResponse();
                        tripHistoryResponse.setMessage(userMessage);
                        tripHistoryResponseMutableLiveData.setValue(tripHistoryResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<TripHistoryResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    TripHistoryResponse tripHistoryResponse = new TripHistoryResponse();
                    tripHistoryResponse.setMessage(null);
                    tripHistoryResponseMutableLiveData.setValue(tripHistoryResponse);

                } else {
                    TripHistoryResponse tripHistoryResponse = new TripHistoryResponse();
                    tripHistoryResponse.setMessage("onFailure");
                    tripHistoryResponseMutableLiveData.setValue(tripHistoryResponse);
                }
            }
        });

        return tripHistoryResponseMutableLiveData;
    }



    public MutableLiveData<BookingDetailsResponse> getBookingDetailsReposotory(int bookingId){
        MutableLiveData<BookingDetailsResponse> bookingDetailsResponseMutableLiveData = new MutableLiveData<>();
        Call<BookingDetailsResponse> bookingDetailsResponseCall = apiService.getBookingDetailsApi(bookingId);
        bookingDetailsResponseCall.enqueue(new Callback<BookingDetailsResponse>() {
            @Override
            public void onResponse(Call<BookingDetailsResponse> call, Response<BookingDetailsResponse> response) {
                if (response.isSuccessful()){

                } else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        BookingDetailsResponse bookingDetailsResponse = new BookingDetailsResponse();
                        bookingDetailsResponse.setMessage(userMessage);
                        bookingDetailsResponseMutableLiveData.setValue(bookingDetailsResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BookingDetailsResponse> call, Throwable t) {

                if (t instanceof NoConnectivityException){
                    BookingDetailsResponse bookingDetailsResponse = new BookingDetailsResponse();
                    bookingDetailsResponse.setMessage(null);
                } else {
                    BookingDetailsResponse bookingDetailsResponse = new BookingDetailsResponse();
                    bookingDetailsResponse.setMessage("onFailure");
                }
            }
        });


        return bookingDetailsResponseMutableLiveData;
    }

}
