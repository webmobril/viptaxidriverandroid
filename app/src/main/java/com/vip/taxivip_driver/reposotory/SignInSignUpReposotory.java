package com.vip.taxivip_driver.reposotory;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.vip.taxivip_driver.models.CityMessageResponse;
import com.vip.taxivip_driver.models.CityResponse;
import com.vip.taxivip_driver.models.CountryResponse;
import com.vip.taxivip_driver.models.CountryResult;
import com.vip.taxivip_driver.models.OtpVerifiedResponse;
import com.vip.taxivip_driver.models.ProfileImageResponse;
import com.vip.taxivip_driver.models.ResendOTPResponse;
import com.vip.taxivip_driver.models.SignInDataResponse;
import com.vip.taxivip_driver.models.SignupResponse;
import com.vip.taxivip_driver.models.StateMessgeResponse;
import com.vip.taxivip_driver.models.StateResponse;
import com.vip.taxivip_driver.models.UpdateProfileResponse;
import com.vip.taxivip_driver.network.NoConnectivityException;
import com.vip.taxivip_driver.network.RestApiService;
import com.vip.taxivip_driver.network.RetrofitInstance;
import com.vip.taxivip_driver.requestclass.OtpVerifyRequest;
import com.vip.taxivip_driver.requestclass.SingInRequest;
import com.vip.taxivip_driver.requestclass.SingupRequest;
import com.vip.taxivip_driver.requestclass.UpdateProfileRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInSignUpReposotory {

    private static RestApiService apiService;
    MutableLiveData<List<StateMessgeResponse>> mutableLiveDataState;
    MutableLiveData<List<CityMessageResponse>> mutableLiveDataCity;
    MutableLiveData<List<CountryResult>> listMutableLiveDataCountry;
    StateMessgeResponse stateResponse;
    private static SignInSignUpReposotory signupReposotry;

    private SignInSignUpReposotory() {
        apiService = RetrofitInstance.getApiService();
    }

    public static SignInSignUpReposotory getInstance() {
        if (signupReposotry == null) {
            signupReposotry = new SignInSignUpReposotory();
        }
        return signupReposotry;
    }


    public MutableLiveData<SignInDataResponse> getSignInMutableLiveData(SingInRequest singInRequest) {

        MutableLiveData<SignInDataResponse> mutableLiveDataSignUp = new MutableLiveData<>();


        Call<SignInDataResponse> call = apiService.postLoginRequest(singInRequest);

        call.enqueue(new Callback<SignInDataResponse>() {
            @Override
            public void onResponse(Call<SignInDataResponse> call, Response<SignInDataResponse> response) {
                if (response.isSuccessful()) {
                    SignInDataResponse mBlogWrapper = response.body();
                    mutableLiveDataSignUp.setValue(mBlogWrapper);
                } else {
                    JSONObject jsonObject = null;
                    try {
                        SignInDataResponse signinResponse = new SignInDataResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        signinResponse.setMessage(userMessage);
                        mutableLiveDataSignUp.setValue(signinResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SignInDataResponse> call, Throwable t) {
                if(t instanceof NoConnectivityException) {
                    Log.e("Reposotry", "NoInternet" + t.toString());

                    // show No Connectivity message to user or do whatever you want.
                    SignInDataResponse signinResponse = new SignInDataResponse();
                    signinResponse.setMessage("Please connect your internet");
                    mutableLiveDataSignUp.setValue(signinResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    SignInDataResponse signinResponse = new SignInDataResponse();
                    signinResponse.setMessage(t.toString());
                    mutableLiveDataSignUp.setValue(signinResponse);
                }
            }
        });

        return mutableLiveDataSignUp;
    }


    public MutableLiveData<SignupResponse> getMutableLiveData(SingupRequest singupRequest) {

        MutableLiveData<SignupResponse> mutableLiveDataSignUp = new MutableLiveData<>();

        Call<SignupResponse> call = apiService.postSignUp(singupRequest);

        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                if (response.isSuccessful()) {
                    mutableLiveDataSignUp.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        SignupResponse signupResponse = new SignupResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        signupResponse.setMessage(userMessage);
                        mutableLiveDataSignUp.setValue(signupResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    SignupResponse signupResponse = new SignupResponse();
                    signupResponse.setMessage(null);
                    mutableLiveDataSignUp.setValue(signupResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    SignupResponse signupResponse = new SignupResponse();
                    signupResponse.setMessage(t.toString());
                    mutableLiveDataSignUp.setValue(signupResponse);
                }
            }
        });

        return mutableLiveDataSignUp;
    }


    public MutableLiveData<List<StateMessgeResponse>> getMutableListState() {
        if (mutableLiveDataState == null) {
            mutableLiveDataState = new MutableLiveData<>();
            Call<StateResponse> call = apiService.getStateList();
            call.enqueue(new Callback<StateResponse>() {
                @Override
                public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                    Log.e(this.getClass().getName(), "STATE_RESPONSE==" + response);

                    if (response.isSuccessful()) {
                        List<StateMessgeResponse> stateResponse = response.body().getMessage();
                        mutableLiveDataState.setValue(stateResponse);
                    } else {
                        JSONObject jsonObject = null;
                        try {
                            stateResponse = new StateMessgeResponse();
                            jsonObject = new JSONObject(response.errorBody().string());
                            JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                            String userMessage = jsonObject1.getString("message");
                            stateResponse.setName(userMessage);
                            mutableLiveDataState.setValue((List<StateMessgeResponse>) stateResponse);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<StateResponse> call, Throwable t) {

                    try {
                        Log.e(this.getClass().getName(), "ERROR==" + t.toString());
                        stateResponse = new StateMessgeResponse();

                        stateResponse.setName("Error");
                        mutableLiveDataState.setValue((List<StateMessgeResponse>) stateResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        return mutableLiveDataState;
    }


    public MutableLiveData<List<CityMessageResponse>> getMutableListCity(int state_id) {
        if (mutableLiveDataCity == null) {
            mutableLiveDataCity = new MutableLiveData<>();
            Call<CityResponse> call = apiService.getCityList(state_id);
            call.enqueue(new Callback<CityResponse>() {
                @Override
                public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                    Log.e(this.getClass().getName(), "STATE_RESPONSE==" + response);

                    if (response.isSuccessful()) {
                        List<CityMessageResponse> stateResponse = response.body().getMessage();
                        mutableLiveDataCity.setValue(stateResponse);
                    } else {
                        JSONObject jsonObject = null;
                        try {
                            stateResponse = new StateMessgeResponse();
                            jsonObject = new JSONObject(response.errorBody().string());
                            JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                            String userMessage = jsonObject1.getString("message");
                            stateResponse.setName(userMessage);
                            mutableLiveDataCity.setValue((List<CityMessageResponse>) stateResponse);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<CityResponse> call, Throwable t) {
                    if (t instanceof NoConnectivityException){
                        stateResponse = new StateMessgeResponse();
                        stateResponse.setName(null);
                        mutableLiveDataCity.setValue((List<CityMessageResponse>) stateResponse);
                    } else
                    try {
                        Log.e(this.getClass().getName(), "ERROR==" + t.toString());
                        stateResponse = new StateMessgeResponse();
                        stateResponse.setName("Error");
                        mutableLiveDataCity.setValue((List<CityMessageResponse>) stateResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        return mutableLiveDataCity;
    }


    public MutableLiveData<List<CountryResult>> getMutableListCountry() {
        if (listMutableLiveDataCountry == null) {
            listMutableLiveDataCountry = new MutableLiveData<>();
            Call<CountryResponse> call = apiService.getCountryList();
            call.enqueue(new Callback<CountryResponse>() {
                @Override
                public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                    Log.e(this.getClass().getName(), "STATE_RESPONSE==" + response);

                    if (response.isSuccessful()) {
                        List<CountryResult> stateResponse = response.body().getResult();
                        listMutableLiveDataCountry.setValue(stateResponse);
                    } else {
                        JSONObject jsonObject = null;
                        try {
                            stateResponse = new StateMessgeResponse();
                            jsonObject = new JSONObject(response.errorBody().string());
                            JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                            String userMessage = jsonObject1.getString("message");
                            stateResponse.setName(userMessage);
                            listMutableLiveDataCountry.setValue((List<CountryResult>) stateResponse);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<CountryResponse> call, Throwable t) {

                    if (t instanceof NoConnectivityException){
                        Log.e(this.getClass().getName(), "ERROR==" + t.toString());
                        CountryResponse  stateResponse = new CountryResponse();
                        stateResponse.setMessage(null);
                        listMutableLiveDataCountry.setValue((List<CountryResult>)stateResponse);
                    } else {
                        Log.e(this.getClass().getName(), "ERROR==" + t.toString());
                        CountryResponse  stateResponse = new CountryResponse();
                        stateResponse.setMessage("Error");
                        listMutableLiveDataCountry.setValue((List<CountryResult>)stateResponse);
                    }



                }
            });
        }

        return listMutableLiveDataCountry;
    }


    public MutableLiveData<UpdateProfileResponse> postUpdateProfile(UpdateProfileRequest updateProfileRequest) {
        MutableLiveData<UpdateProfileResponse> listMutableLiveDataUpdateProfile = new MutableLiveData<>();
        Call<UpdateProfileResponse> call = apiService.postUpateProfile(updateProfileRequest);

        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                if (response.isSuccessful()) {
                    listMutableLiveDataUpdateProfile.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        updateProfileResponse.setMessage(userMessage);
                        listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage(null);
                    listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage("Error");
                    listMutableLiveDataUpdateProfile.setValue(updateProfileResponse);
                }
            }
        });

        return listMutableLiveDataUpdateProfile;
    }


    public MutableLiveData<UpdateProfileResponse> postgetProfile(int driverId) {
        MutableLiveData<UpdateProfileResponse> listMutableLiveDataGetProfile = new MutableLiveData<>();
        Call<UpdateProfileResponse> call = apiService.postGetProfile(driverId);

        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                if (response.isSuccessful()) {
                    listMutableLiveDataGetProfile.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        updateProfileResponse.setMessage(userMessage);
                        listMutableLiveDataGetProfile.setValue(updateProfileResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage(null);
                    listMutableLiveDataGetProfile.setValue(updateProfileResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    UpdateProfileResponse updateProfileResponse = new UpdateProfileResponse();
                    updateProfileResponse.setMessage("Error");
                    listMutableLiveDataGetProfile.setValue(updateProfileResponse);
                }
            }
        });

        return listMutableLiveDataGetProfile;
    }


    public MutableLiveData<ProfileImageResponse> postUpdateProfileImage(RequestBody profileImage, MultipartBody.Part image_body) {
        MutableLiveData<ProfileImageResponse> listMutableLiveDataImageProfileUpdate = new MutableLiveData<>();
        Call<ProfileImageResponse> call = apiService.postUploadImage(profileImage, image_body);

        call.enqueue(new Callback<ProfileImageResponse>() {
            @Override
            public void onResponse(Call<ProfileImageResponse> call, Response<ProfileImageResponse> response) {
                if (response.isSuccessful()) {
                    listMutableLiveDataImageProfileUpdate.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        ProfileImageResponse updateImgProfileResponse = new ProfileImageResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        updateImgProfileResponse.setMessage(userMessage);
                        listMutableLiveDataImageProfileUpdate.setValue(updateImgProfileResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileImageResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    ProfileImageResponse updateImgProfileResponse = new ProfileImageResponse();
                    updateImgProfileResponse.setMessage(null);
                    listMutableLiveDataImageProfileUpdate.setValue(updateImgProfileResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    ProfileImageResponse updateImgProfileResponse = new ProfileImageResponse();
                    updateImgProfileResponse.setMessage("Error");
                    listMutableLiveDataImageProfileUpdate.setValue(updateImgProfileResponse);
                }
            }
        });

        return listMutableLiveDataImageProfileUpdate;
    }

    public LiveData<OtpVerifiedResponse> getOtpVerifyMutableLiveData(OtpVerifyRequest otpVerifyRequest) {
        MutableLiveData<OtpVerifiedResponse> mutableLiveData = new MutableLiveData<>();

        Call<OtpVerifiedResponse> call = apiService.postVerifyOtp(otpVerifyRequest);

        call.enqueue(new Callback<OtpVerifiedResponse>() {
            @Override
            public void onResponse(Call<OtpVerifiedResponse> call, Response<OtpVerifiedResponse> response) {
                Log.e("otp_resonse", new Gson().toJson(response.message()));
                if (response.isSuccessful()){mutableLiveData.setValue(response.body());}
                else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        OtpVerifiedResponse logoutResponse = new OtpVerifiedResponse();
                        logoutResponse.setMessage(userMessage);
                        mutableLiveData.setValue(logoutResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<OtpVerifiedResponse> call, Throwable t) {
                if ( t instanceof NoConnectivityException){
                    OtpVerifiedResponse logoutResponse = new OtpVerifiedResponse();
                    logoutResponse.setMessage(null);
                    mutableLiveData.setValue(logoutResponse);
                } else {
                    Log.e("error", t.getMessage());
                    OtpVerifiedResponse logoutResponse = new OtpVerifiedResponse();
                    logoutResponse.setMessage(t.getMessage());
                    mutableLiveData.setValue(logoutResponse);
                }

            }
        });

        return mutableLiveData;
    }

    public LiveData<ResendOTPResponse> getResendOTP(int userid) {
        MutableLiveData<ResendOTPResponse> resendMutableLiveData = new MutableLiveData<>();
        Call<ResendOTPResponse> call = apiService.postresendOTp(userid);

        call.enqueue(new Callback<ResendOTPResponse>() {
            @Override
            public void onResponse(Call<ResendOTPResponse> call, Response<ResendOTPResponse> response) {
                Log.e("ResendOTPResponse", new Gson().toJson(response.body()));

                if (response.isSuccessful()){resendMutableLiveData.setValue(response.body());}
                else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        ResendOTPResponse resendOTPResponse = new ResendOTPResponse();
                        resendOTPResponse.setMessage(userMessage);
                        resendMutableLiveData.setValue(resendOTPResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResendOTPResponse> call, Throwable t) {
                if ( t instanceof NoConnectivityException){
                    ResendOTPResponse logoutResponse = new ResendOTPResponse();
                    logoutResponse.setMessage(null);
                    resendMutableLiveData.setValue(logoutResponse);
                } else {
                    Log.e("error", t.getMessage());
                    ResendOTPResponse logoutResponse = new ResendOTPResponse();
                    logoutResponse.setMessage(t.getMessage());
                    resendMutableLiveData.setValue(logoutResponse);
                }


            }
        });
        return resendMutableLiveData;
    }

}
