package com.vip.taxivip_driver.reposotory;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.vip.taxivip_driver.models.AboutUsResponse;
import com.vip.taxivip_driver.models.ContactUsResponse;
import com.vip.taxivip_driver.models.GetHelpResponse;
import com.vip.taxivip_driver.models.LogoutResponse;
import com.vip.taxivip_driver.models.OfflineResponse;
import com.vip.taxivip_driver.models.TermsConditionResponse;
import com.vip.taxivip_driver.network.NoConnectivityException;
import com.vip.taxivip_driver.network.RestApiService;
import com.vip.taxivip_driver.network.RetrofitInstance;
import com.vip.taxivip_driver.requestclass.ContactAdminRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountReposotory {

    private static AccountReposotory accountReposotory;
    private static RestApiService apiService;

    private AccountReposotory() {
        apiService = RetrofitInstance.getApiService();

    }

    public static AccountReposotory getInstance() {
        if (accountReposotory == null) {
            accountReposotory = new AccountReposotory();
        }
        return accountReposotory;
    }


    MutableLiveData<LogoutResponse> logoutResponseMutableLiveData;

    public MutableLiveData<LogoutResponse> postLogoutResponse(int userId) {
        logoutResponseMutableLiveData = new MutableLiveData<>();
        Call<LogoutResponse> call = apiService.postLogOut(userId);
        call.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                if (response.isSuccessful()) {
                    logoutResponseMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        LogoutResponse logoutResponse = new LogoutResponse();
                        logoutResponse.setMessage(userMessage);
                        logoutResponseMutableLiveData.setValue(logoutResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {

                if (t instanceof NoConnectivityException) {
                    LogoutResponse logoutResponse = new LogoutResponse();
                    logoutResponse.setMessage(null);
                    logoutResponseMutableLiveData.setValue(logoutResponse);
                } else {
                    LogoutResponse logoutResponse = new LogoutResponse();
                    logoutResponse.setMessage(t.getMessage());
                    logoutResponseMutableLiveData.setValue(logoutResponse);
                }


            }
        });


        return logoutResponseMutableLiveData;
    }


    public MutableLiveData<ContactUsResponse> getContactUs() {
        MutableLiveData<ContactUsResponse> contactUsResponseMutableLiveData = new MutableLiveData<>();
        Call<ContactUsResponse> call = apiService.getContactUsInfo();
        call.enqueue(new Callback<ContactUsResponse>() {
            @Override
            public void onResponse(Call<ContactUsResponse> call, Response<ContactUsResponse> response) {

                if (response.isSuccessful()) {
                    contactUsResponseMutableLiveData.setValue(response.body());
                } else {

                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        ContactUsResponse contactUsResponse = new ContactUsResponse();
                        contactUsResponse.setMessage(userMessage);
                        contactUsResponseMutableLiveData.setValue(contactUsResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    ContactUsResponse contactUsResponse = new ContactUsResponse();
                    contactUsResponse.setMessage(null);
                    contactUsResponseMutableLiveData.setValue(contactUsResponse);
                } else {
                    ContactUsResponse contactUsResponse = new ContactUsResponse();
                    contactUsResponse.setMessage(t.getMessage());
                    contactUsResponseMutableLiveData.setValue(contactUsResponse);
                }
            }
        });


        return contactUsResponseMutableLiveData;
    }


    public MutableLiveData<GetHelpResponse> getHelpResponseMutableLive(ContactAdminRequest contactAdminRequest) {

        MutableLiveData<GetHelpResponse> getHelpResponseMutableLiveData = new MutableLiveData<>();
        Call<GetHelpResponse> call = apiService.postGetHelp(contactAdminRequest);
        call.enqueue(new Callback<GetHelpResponse>() {
            @Override
            public void onResponse(Call<GetHelpResponse> call, Response<GetHelpResponse> response) {
                if (response.isSuccessful()) {
                    getHelpResponseMutableLiveData.setValue(response.body());

                } else {

                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        int errorCode = jsonObject1.optInt("status_code");
                        Log.e(this.getClass().getName(), "ERror_code" + userMessage + " " + errorCode);
                        GetHelpResponse getHelpResponse = new GetHelpResponse();
                        getHelpResponse.setMessage(userMessage);
                        getHelpResponseMutableLiveData.setValue(getHelpResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetHelpResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    GetHelpResponse getHelpResponse = new GetHelpResponse();
                    getHelpResponse.setMessage(null);
                    getHelpResponseMutableLiveData.setValue(getHelpResponse);
                } else {
                    GetHelpResponse getHelpResponse = new GetHelpResponse();
                    getHelpResponse.setMessage(t.getMessage());
                    getHelpResponseMutableLiveData.setValue(getHelpResponse);
                }

            }
        });


        return getHelpResponseMutableLiveData;
    }


    public MutableLiveData<AboutUsResponse> getAboutUsApi(int language) {
        MutableLiveData<AboutUsResponse> aboutUsMutableLiveData = new MutableLiveData<>();
        Call<AboutUsResponse> call = apiService.postAboutUsDetails(language);

        call.enqueue(new Callback<AboutUsResponse>() {
            @Override
            public void onResponse(Call<AboutUsResponse> call, Response<AboutUsResponse> response) {
                if (response.isSuccessful()) {
                    aboutUsMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        AboutUsResponse offlineError = new AboutUsResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        offlineError.setMessage(userMessage);
                        aboutUsMutableLiveData.setValue(offlineError);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AboutUsResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    AboutUsResponse offlineError = new AboutUsResponse();
                    offlineError.setMessage(null);
                    aboutUsMutableLiveData.setValue(offlineError);
                }

               /* OfflineResponse offlineError = new OfflineResponse();
                offlineError.setMessage("Error Throw");
                aboutUsMutableLiveData.setValue(offlineError);*/

            }
        });
        return aboutUsMutableLiveData;
    }


    public MutableLiveData<TermsConditionResponse> getTermsConditionReposotory(int language) {
        MutableLiveData<TermsConditionResponse> termsConditionMutableLiveData = new MutableLiveData<>();
        Call<TermsConditionResponse> call = apiService.postTermsConditione(language);

        call.enqueue(new Callback<TermsConditionResponse>() {
            @Override
            public void onResponse(Call<TermsConditionResponse> call, Response<TermsConditionResponse> response) {
                if (response.isSuccessful()) {
                    termsConditionMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        TermsConditionResponse termsConditionResponse = new TermsConditionResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        termsConditionResponse.setMessage(userMessage);
                        //aboutUsMutableLiveData.setValue(offlineError);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<TermsConditionResponse> call, Throwable t) {

                if (t instanceof NoConnectivityException) {
                    TermsConditionResponse termsConditionResponse = new TermsConditionResponse();
                    termsConditionResponse.setMessage(null);
                    termsConditionMutableLiveData.setValue(termsConditionResponse);
                } else {
                    TermsConditionResponse termsConditionResponse = new TermsConditionResponse();
                    termsConditionResponse.setMessage("Error Throw");
                    termsConditionMutableLiveData.setValue(termsConditionResponse);
                }

            }
        });
        return termsConditionMutableLiveData;
    }


    public MutableLiveData<TermsConditionResponse> getPrivacyPolicyReposotory(int language) {
        MutableLiveData<TermsConditionResponse> termsConditionMutableLiveData = new MutableLiveData<>();
        Call<TermsConditionResponse> call = apiService.postPrivacyPolicy(language);

        call.enqueue(new Callback<TermsConditionResponse>() {
            @Override
            public void onResponse(Call<TermsConditionResponse> call, Response<TermsConditionResponse> response) {
                if (response.isSuccessful()) {
                    termsConditionMutableLiveData.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        TermsConditionResponse termsConditionResponse = new TermsConditionResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        termsConditionResponse.setMessage(userMessage);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<TermsConditionResponse> call, Throwable t) {

                if (t instanceof NoConnectivityException) {
                    TermsConditionResponse termsConditionResponse = new TermsConditionResponse();
                    termsConditionResponse.setMessage(null);
                    termsConditionMutableLiveData.setValue(termsConditionResponse);
                } else {
                    TermsConditionResponse termsConditionResponse = new TermsConditionResponse();
                    termsConditionResponse.setMessage("Error Throw");
                    termsConditionMutableLiveData.setValue(termsConditionResponse);
                }

            }
        });
        return termsConditionMutableLiveData;
    }
}
