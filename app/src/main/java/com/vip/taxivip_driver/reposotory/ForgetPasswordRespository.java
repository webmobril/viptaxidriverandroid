package com.vip.taxivip_driver.reposotory;

import android.util.Log;
import androidx.lifecycle.MutableLiveData;
import com.vip.taxivip_driver.models.ForgetPasswordResponse;
import com.vip.taxivip_driver.models.ResetPasswordResponse;
import com.vip.taxivip_driver.network.NoConnectivityException;
import com.vip.taxivip_driver.network.RestApiService;
import com.vip.taxivip_driver.network.RetrofitInstance;
import com.vip.taxivip_driver.requestclass.ForgetPasswordRequest;
import com.vip.taxivip_driver.requestclass.ResetPasswordRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordRespository {

    private final RestApiService apiService;
    private ForgetPasswordResponse forgetPasswordResponse;
    private ResetPasswordResponse resetPasswordResponse;
    private static ForgetPasswordRespository forgetPasswordRespository;

    private ForgetPasswordRespository() {
        apiService = RetrofitInstance.getApiService();
    }

    public static ForgetPasswordRespository getInstance() {
        if (forgetPasswordRespository == null) {
            forgetPasswordRespository = new ForgetPasswordRespository();
        }
        return forgetPasswordRespository;
    }

    public MutableLiveData<ForgetPasswordResponse> getMutableLiveData(ForgetPasswordRequest forgetPasswordRequest) {
        MutableLiveData<ForgetPasswordResponse> mutableLiveDataSignUp = new MutableLiveData<>();
        Call<ForgetPasswordResponse> call = apiService.postForgetPassword(forgetPasswordRequest);

        call.enqueue(new Callback<ForgetPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                if (response.isSuccessful()) {
                    ForgetPasswordResponse mBlogWrapper = response.body();
                    forgetPasswordResponse = mBlogWrapper;
                    mutableLiveDataSignUp.setValue(forgetPasswordResponse);
                } else {
                    JSONObject jsonObject = null;
                    try {
                        forgetPasswordResponse = new ForgetPasswordResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        forgetPasswordResponse.setMessage(userMessage);
                        mutableLiveDataSignUp.setValue(forgetPasswordResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    forgetPasswordResponse = new ForgetPasswordResponse();
                    forgetPasswordResponse.setMessage(null);
                    mutableLiveDataSignUp.setValue(forgetPasswordResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    forgetPasswordResponse = new ForgetPasswordResponse();
                    forgetPasswordResponse.setMessage(t.toString());
                    mutableLiveDataSignUp.setValue(forgetPasswordResponse);
                }
            }
        });

        return mutableLiveDataSignUp;
    }


    public MutableLiveData<ResetPasswordResponse> getMutableLiveDataResetPass(ResetPasswordRequest resetPasswordRequest) {
        MutableLiveData<ResetPasswordResponse> mutableLiveDataResetPassword = new MutableLiveData<>();
        Call<ResetPasswordResponse> call = apiService.postResetPassword(resetPasswordRequest);

        call.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                if (response.isSuccessful()) {
                    mutableLiveDataResetPassword.setValue(response.body());
                } else {
                    JSONObject jsonObject = null;
                    try {
                        resetPasswordResponse = new ResetPasswordResponse();
                        jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                        String userMessage = jsonObject1.getString("message");
                        forgetPasswordResponse.setMessage(userMessage);
                        mutableLiveDataResetPassword.setValue(resetPasswordResponse);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                if (t instanceof NoConnectivityException){
                    resetPasswordResponse = new ResetPasswordResponse();
                    resetPasswordResponse.setMessage(null);
                    mutableLiveDataResetPassword.setValue(resetPasswordResponse);
                } else {
                    Log.e("Reposotry", "Error==" + t.toString());
                    resetPasswordResponse = new ResetPasswordResponse();
                    resetPasswordResponse.setMessage(t.toString());
                    mutableLiveDataResetPassword.setValue(resetPasswordResponse);
                }
            }
        });

        return mutableLiveDataResetPassword;
    }


}
