package com.vip.taxivip_driver.firebase;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.models.NotificationResponse;
import com.vip.taxivip_driver.utils.AppData;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.LoginPreference;
import com.vip.taxivip_driver.views.activities.ContainerActivity;

import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private NotificationManager notifManager;
    private NotificationChannel mChannel;
    private Context mContext;
    private LoginPreference loginPreference;


    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("NEW_FIREBASE_TOKEN", s);
        loginPreference = new LoginPreference(getApplicationContext());
        loginPreference.saveDeviceToken(s);

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("FirebaseMessagingServic", Objects.requireNonNull(remoteMessage.getFrom()));
        String body = remoteMessage.getData().toString();
        Log.e("Notification Response--", body);
        Gson gson = new Gson();
        NotificationResponse response = gson.fromJson(body, NotificationResponse.class);
        if (!AppData.getInstance().getIsOngoing()) {
            displayNotification(response);
        }
    }




    private void displayNotification(NotificationResponse result) {
        AppData.getInstance().setIsOngoing(true);

        Intent intent = new Intent();
        String Title = result.result.title;
        int bookingId = result.result.id;
        Log.e(this.getClass().getName(),"Booking_ID_firebase" + bookingId);
       // loginPreference = new LoginPreference(getApplicationContext());
        //loginPreference.setBookingId(bookingId);

        switch (Title) {
            case Constants.NEW_BOOKING:
                AppData.getInstance().setFROM(Constants.NEW_BOOKING);
                AppData.getInstance().setIsfromNotification(true);
                AppData.getInstance().setNotificationData(result.result);
                intent = new Intent(this, ContainerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // startService(new Intent(this, NotificationChecker.class));

                if (CommonMethod.isAppRunning(getApplicationContext(), "com.vip.taxivip_driver")) {
                    startActivity(intent);
                }

                break;

            case Constants.CANCEl_BOOKING:
                AppData.getInstance().setIsOngoing(false);
                loginPreference.setBookingType("");
                AppData.getInstance().setIsfromNotification(false);
                intent = new Intent(this, ContainerActivity.class);
                intent.putExtra(Constants.TYPE, Constants.HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                if (CommonMethod.isAppRunning(getApplicationContext(), "com.vip.taxivip_driver")) {
                    startActivity(intent);
                }
                break;


            case Constants.DESTINATION_CHANGED:
                AppData.getInstance().setFROM(Constants.DESTINATION_CHANGED);
                AppData.getInstance().setIsfromNotification(true);
                /*AppData.getInstance().setCancelNotification(result);*/
                AppData.getInstance().setNotificationData(result.result);
                intent = new Intent(this, ContainerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                if (CommonMethod.isAppRunning(getApplicationContext(), "com.vip.taxivip_driver")) {
                    startActivity(intent);
                }
                break;

        }

        if (notifManager == null) {
            notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        Uri sound = Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.notification_ring);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        @SuppressLint("WrongConstant") NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "CH_ID")
                .setContentTitle("Hey ! " + result.result.user_name)
                .setContentText(Title)
                .setSmallIcon(getNotificationIcon())
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setBadgeIconType(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setSound(sound)
                /*.setTimeoutAfter(Constants.NOTIFICATION_CANCEL_TIME)*/
                .setContentIntent(pendingIntent);


        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            if (sound != null) {
                Log.e("TAG", "sound != null");
                // Changing Default mode of notification
                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
                // Creating an Audio Attribute
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();
                // Creating Channel
                NotificationChannel notificationChannel = new NotificationChannel("CH_ID", "VIBES Taxi", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setSound(sound, audioAttributes);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
        }
        mNotificationManager.notify(0, notificationBuilder.build());
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }
}
