package com.vip.taxivip_driver.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.vip.taxivip_driver.R;
import com.vip.taxivip_driver.views.activities.ContainerActivity;
import com.vip.taxivip_driver.utils.Constants;
import com.vip.taxivip_driver.utils.CommonMethod;
import com.vip.taxivip_driver.utils.LoginPreference;

import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MyLocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Context context;
    private boolean stopService = false;

    /* For Google Fused API */
    protected GoogleApiClient mGoogleApiClient;
    protected LocationSettingsRequest mLocationSettingsRequest;
    private double latitude = 0.0f, longitude = 0.0f;
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    //private PubNub pubnub;
    private boolean isFirstTime = true;
    private LoginPreference loginPreference;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(this.getClass().getName(), "onCreate");
        context = this;
       // loginPreference = new LoginPreferencegetApplicationContext());
        loginPreference =new LoginPreference(getApplicationContext());

        Log.e(this.getClass().getName(),"PREF_OBJ" + loginPreference);
        if (TextUtils.isEmpty(loginPreference.getDeviceToken())) {
            Intent serviceIntent = new Intent(getApplicationContext(), MyLocationService.class);
            stopService(serviceIntent);
            stopSelf();
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(this.getClass().getName(), "onStartCommand");
        //initPubNub();

        buildGoogleApiClient();

        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                try {
                    if (!stopService) {
                        if (latitude != 0.0f) {
                            storeLocation();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (!stopService) {
                        handler.postDelayed(this, TimeUnit.SECONDS.toMillis(10));
                    }
                }
            }
        };

        handler.postDelayed(runnable, 2000);


        return START_STICKY;
    }

    private void storeLocation() {
       /* AppData.getInstance().setLatLng(new LatLng(latitude, longitude));
        try {
            Log.e("try--", "called");
            LinkedHashMap<String, String> message = getNewLocationMessage(latitude, longitude);
            pubnub.publish()
                    .message(message)
                    .channel(Constants.PUBNUB_CHANNEL_NAME)
                    .async(new PNCallback<PNPublishResult>() {
                        @Override
                        public void onResponse(PNPublishResult result, PNStatus status) {
                            // handle publish result, status always present, result if successful
                            // status.isError() to see if error happened
                                        if (!status.isError()) {
                                            Log.e("pub timetoken: ", String.valueOf(result.getTimetoken()));
                                        }
                                        Log.e("pub status code: ", String.valueOf(status.getStatusCode()));
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {*/
            Log.e("finally--", "called");
            getDistance();
       // }
    }

    private void getDistance() {
        Intent i = new Intent(Constants.LOCATION_RECEIVER);
        i.putExtra("lat", latitude);
        i.putExtra("lng", longitude);
        sendBroadcast(i);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        String TAG = "MyLocationService";
        Log.e(TAG, "Service Stopped");
        stopService = true;
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            Log.e(Constants.TAG_LOCATION, "Location Update Callback Removed");
        }
        super.onDestroy();
    }


    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        Log.e(Constants.TAG_LOCATION, "Latitude : " + latitude + "\tLongitude : " + longitude);


        if (isFirstTime) {
            loginPreference.setLatitude(String.valueOf(latitude));
            loginPreference.setLongitude(String.valueOf(longitude));
            isFirstTime = false;
        }

        if (latitude == 0.0f) {
            requestLocationUpdate();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5 * 1000);
        mLocationRequest.setFastestInterval(4 * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();

        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(locationSettingsResponse -> {
                    Log.e(Constants.TAG_LOCATION, "GPS Success");
                    requestLocationUpdate();
                }).addOnFailureListener(e -> {
                  /*  int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                int REQUEST_CHECK_SETTINGS = 214;
                                ResolvableApiException rae = (ResolvableApiException) e;
                                rae.startResolutionForResult((Activity) getApplicationContext(), REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException sie) {
                                Log.e(TAG_LOCATION, "Unable to execute request.");
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.e(TAG_LOCATION, "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                    }*/
                });
    }

    @Override
    public void onConnectionSuspended(int i) {
        connectGoogleClient();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        mSettingsClient = LocationServices.getSettingsClient(context);

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        connectGoogleClient();

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Log.e(Constants.TAG_LOCATION, "Location Received" + locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                onLocationChanged(mCurrentLocation);
            }
        };
    }

    private void connectGoogleClient() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(context);
        if (resultCode == ConnectionResult.SUCCESS) {
            mGoogleApiClient.connect();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdate() {
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }


    private void showNotification() {
        Intent intent = new Intent(context, ContainerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String CHANNEL_ID = "channel_location";
        String CHANNEL_NAME = "channel_location";

        NotificationCompat.Builder builder = null;
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
            builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID);
            builder.setChannelId(CHANNEL_ID);
            builder.setBadgeIconType(NotificationCompat.BADGE_ICON_NONE);
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID);
        }

        builder.setContentTitle(getString(R.string.app_name));
        //builder.setContentText(String.valueOf(CommonMethod.roundTwoDecimals(LoginPreferences.getActiveInstance(getApplicationContext()).getCurrentKm())).concat("Km"));
        Uri notificationSound = RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(notificationSound);
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentIntent(pendingIntent);
        Notification notification = builder.build();
        startForeground(101, notification);
    }

    /*private void initPubNub() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(Constants.PUBNUB_SUBSCRIBE_KEY);
        pnConfiguration.setPublishKey(Constants.PUBNUB_PUBLISH_KEY);
        pnConfiguration.setSecure(true);
        pubnub = new PubNub(pnConfiguration);
    }*/

    private LinkedHashMap<String, String> getNewLocationMessage(double lat, double lng) {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put("lat", String.valueOf(lat));
        map.put("lng", String.valueOf(lng));
        map.put("user_id", loginPreference.getRegister_id()+"");
        map.put("user_name", loginPreference.getName());
        return map;
    }



    private void saveDistance() {

        Location new_location = new Location("latest_new_location");
        new_location.setLatitude(latitude);
        new_location.setLongitude(longitude);

        double oldLat = Double.parseDouble(loginPreference.getLatitude());
        double oldlng = Double.parseDouble(loginPreference.getLongitude());

        Location old_location = new Location("latest_old_location");
        old_location.setLatitude(oldLat);
        old_location.setLongitude(oldlng);

        if (!TextUtils.isEmpty(loginPreference.getLatitude())) {

            final double new_distance = old_location.distanceTo(new_location);
            final float speed = new_location.getSpeed();

            Log.e("Service speed", "#" + speed);
            Log.e("Service Distance", "#" + new_distance);

            Log.e("Current LATLNG", "#" + latitude + ":" + longitude);
            Log.e("OLD LATLNG", "#" + oldLat + ":" + oldlng);

            if (new_distance > 5) {
                loginPreference.setCurrentKm((float) ((float)
                                        loginPreference.getCurrentKm() + CommonMethod.roundTwoDecimals(new_distance / 1000)));
                // showNotification();
            }
        }
        loginPreference.setLatitude(String.valueOf(latitude));
        loginPreference.setLongitude(String.valueOf(longitude));

    }
}
